<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','HomeController@index')->name('home');
Route::get('/dashboard','HomeController@index')->name('home');

Auth::routes();

//customers
Route::get('/customer/add','CustomerController@addCustomer')->name('add_customer');
Route::post('/customer/save','CustomerController@saveCustomer')->name('save_customer');
Route::get('/customer/list','CustomerController@listCustomer')->name('list_customers');
Route::get('/customer/business/select/{id}','CustomerController@selectBusiness')->name('select_business');

//business
Route::get('/business/list','CustomerController@listBusiness')->name('listBusiness');
Route::get('/business/completed/list','CustomerController@listComplatedBusiness')->name('listComplatedBusiness');
Route::get('/customer/business/motor/{id}','BusinessController@motorBusiness')->name('motor-business');
Route::get('/customer/business/motor/new-vehicle/add/{id}','BusinessController@addNewVehicle')->name('addNewVehicle');
Route::post('/customer/get/info','BusinessController@getCustomerInfo')->name('getCustomerInfo');
Route::post('/customer/business/motor/save','BusinessController@saveMotorBusinessData')->name('saveMotorBusinessData');
Route::post('/customer/business/motor/update','BusinessController@updateMotorBusinessData')->name('updateMotorBusinessData');
Route::get('/customer/business/{lead_id}','BusinessController@viewCustomerBusiness')->name('viewCustomerBusiness');
Route::post('/customer/business/vehicle-number/{save}','BusinessController@saveVehicleNumber');
Route::get('/customer/business/quotation/generate/{id}','BusinessController@quotationGenerate')->name('quotationGenerate');
Route::post('/customer/business/quotation/generated/save','BusinessController@quotationGeneratedSave')->name('quotationGeneratedSave');
Route::get('/customer/business/quotation/generated/view/{id}','BusinessController@quotationGenerated')->name('quotationGenerated');
Route::post('/customer/business/quotation/generated/update','BusinessController@quotationGeneratedUpdate')->name('quotationGeneratedUpdate');
Route::get('/customer/business/quotation/insurence/view/{id}','BusinessController@quotationInsurenceSingle')->name('quotationInsurenceSingle');
Route::get('/customer/business/payment/{quote_no}/{lead_id}','BusinessController@customerPayments')->name('customerPayments');
Route::get('/customer/business/documents/{quote_no}/{lead_id}','BusinessController@customerDocuments')->name('customerDocuments');
Route::post('/customer/business/documents/save','DocumentController@customerDocumentsSave')->name('customerDocumentsSave');
Route::post('/customer/business/documents/file/upload','DocumentController@customerDocumentsUpload')->name('customerDocumentsUpload');
Route::post('/email/send','EmailController@sendEmail')->name('sendEmail');
Route::post('/customer/business/payment/save','BusinessController@savePaymentDetails')->name('savePaymentDetails');
Route::post('/invoice/print/','PrintController@printInvoice')->name('printInvoice');
Route::post('/invoice/print/renew','PrintController@printInvoiceRenew')->name('printInvoiceRenew');
Route::post('/calc/remain/dates','BusinessController@calcRemainDate')->name('calcRemainDate');
Route::post('/customer-status/update','BusinessController@customerStatusUpdate')->name('customerStatusUpdate');

//Renewals
Route::get('/business/renew-soon','RenewalController@renewSoon')->name('renewSoon');
Route::get('/business/renew/{lead_id}','RenewalController@RenewBusiness')->name('RenewBusiness');
Route::post('/business/renew/save','RenewalController@saveRenewalQuote')->name('saveRenewalQuote');
Route::get('/business/renew-soon/{userID}','RenewalController@renewSoonByUser')->name('renewSoonByUser');

Route::get('/business/renew-soon','RenewalController@renewSoon')->name('renewSoon');
Route::post('/business/renew/quote','RenewalController@RenewBusinessQuote')->name('RenewBusinessQuote');
Route::post('/business/renew/quote/proceed','RenewalController@RenewBusinessQuoteProceed')->name('RenewBusinessQuoteProceed');
Route::post('/business/renew/quote/get','RenewalController@RenewBusinessQuoteGet')->name('RenewBusinessQuoteGet');
Route::get('/business/renew/completed/done','RenewalController@renewalsComplated')->name('renewalsComplated');

//
//
////payments
//Route::post('/payments/type/save','PaymentController@savePaymentType')->name('savePaymentType');
//
//
//Route::get('/home', 'HomeController@index')->name('home');
//
////html layouts
//Route::get('/add-vehical',function (){
//    return view('html.add-vehical');
//});
////Route::get('/dashboard',function (){
////    return view('html.dashboard');
////});
//Route::get('/customers',function (){
//    return view('html.customers');
//});
//Route::get('/insurance-company-1',function (){
//    return view('html.insurance-company-1');
//});
//Route::get('/insurance-company-2',function (){
//    return view('html.insurance-company-2');
//});
//Route::get('/loa',function (){
//    return view('html.loa');
//});
//Route::get('/loa-to-company',function (){
//    return view('html.loa-to-company');
//});
//Route::get('/new-business',function (){
//    return view('html.new-business');
//});
//Route::get('/quotations',function (){
//    return view('html.quotations');
//});
//Route::get('/register-new-customer',function (){
//    return view('html.register-new-customer');
//});
//Route::get('/renewal',function (){
//    return view('html.renewal');
//});
//Route::get('/selected-business',function (){
//    return view('html.selected-business');
//});
