@extends('layouts.dashboard-layout')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="text-center visible-xs">
            Agent Dashboard
        </h1>
        <h1 class="hidden-xs">
            Agent Dashboard
        </h1>
        <ol class="breadcrumb hidden-xs">
            <li><a href="{{asset('')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Agent Dashboard</li>
            <!-- <li class="active">Blank page</li> -->
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-lg-3 col-xs-6">
                <a href="{{route('add_customer')}}">
                    <div class="small-box bg-aqua">
                        <div class="inner">
                            <h3 class="mb-0">Add</h3>
                            <h3 class="mb-0">Customer</h3>
                        </div>
                        <div class="icon">
                            <i class="ion ion-plus"></i>
                        </div>
                        <span class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></span>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-xs-6">
                <a href="{{route('list_customers')}}">
                    <div class="small-box bg-aqua">
                        <div class="inner">
                            <h3 class="mb-0">List</h3>
                            <h3 class="mb-0">Customers</h3>
                        </div>
                        <div class="icon">
                            <i class="ion ion-ios-list-outline"></i>
                        </div>
                        <span class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></span>
                    </div>
                </a>
            </div>
            <!-- ./col -->

            <div class="col-lg-3 col-xs-6">
                <a href="{{route('listBusiness')}}">
                    <div class="small-box bg-yellow">
                        <div class="inner">
                            <h3 class="mb-0">Ongoing</h3>
                            <h3 class="mb-0">Businesses</h3>
                        </div>
                        <div class="icon">
                            <i class="fa fa-refresh"></i>
                        </div>
                        <span class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></span>
                    </div>
                </a>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <a href="{{route('listComplatedBusiness')}}">
                    <div class="small-box bg-yellow">
                        <div class="inner">
                            <h3 class="mb-0">Completed</h3>
                            <h3 class="mb-0">Businesses</h3>
                        </div>
                        <div class="icon">
                            <i class="fa fa-check"></i>
                        </div>
                        <span class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></span>
                    </div>
                </a>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3>65</h3>

                        <p>Unique Visitors</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-pie-graph"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3>65</h3>

                        <p>Unique Visitors</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-pie-graph"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3>65</h3>

                        <p>Unique Visitors</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-pie-graph"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
        </div>
        {{--<div class="connectedSortable ">--}}
        {{--<div class="nav-tabs-custom">--}}
        {{--<!-- Tabs within a box -->--}}
        {{--<ul class="nav nav-tabs pull-right">--}}
        {{--<li class="active"><a href="#revenue-chart" data-toggle="tab">Area</a></li>--}}
        {{--<li class="pull-left header"><i class="fa fa-inbox"></i> Sales</li>--}}
        {{--</ul>--}}
        {{--<div class="tab-content no-padding">--}}
        {{--<!-- Morris chart - Sales -->--}}
        {{--<div class="chart tab-pane active" id="revenue-chart"--}}
        {{--style="position: relative; height: 300px;"></div>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}


    </section>
    <!-- /.content -->
    <div class="footer-tabs visible-xs">
        <div class="navbar">
            <a href="#news" class="customer_list bg-navy-active">Customer List</a>
            <a href="#contact" class="hot-leads">Hot Leads</a>
            <a href="#contact" class="add-business"><i class="ion-plus-round"></i></a>
            <a href="#contact" class="add-user"><i class="ion-person-add"></i></a>
        </div>
    </div>
@endsection

@section('extra-css')
    <style>

        .footer-tabs .navbar {
            overflow: hidden;
            background-color: rgb(255, 255, 255);
            position: fixed;
            bottom: -1px;
            width: 100%;
            margin: 0px !important;
            /*border-radius: 6px 6px 0px 0px;*/

        }

        .footer-tabs .add-business {
            padding: 9px 15px !important;
            border-radius: 100% !important;
            margin-top: 7px;
            margin-right: 2px;
            margin-left: 2px;
            border: 2px solid #09708a !important;
            color: #fff !important;
            background: #09708a !important;
        }

        .footer-tabs .add-user {
            padding: 9px 14px !important;
            border-radius: 100% !important;
            margin-top: 7px;
            margin-right: 2px;
            margin-left: 2px;
            border: 2px solid #09708a !important;
            color: #09708a !important;
            background: #fff !important;
        }

        .footer-tabs .hot-leads {

        }

        .footer-tabs .navbar a {
            float: left;
            display: block;
            color: #fff;
            text-align: center;
            padding: 10px 8px;
            font-weight: 600;
            text-decoration: none;
            font-size: 16px;
            border-radius: 5;
            background: #09708a;
            border: 1px solid #09708a;
            margin-top: 3px;
            margin-bottom: 3px;
        }

        .footer-tabs .navbar a:hover {
            background: #f1f1f1;
            color: black;
        }

        .footer-tabs .navbar a.active {
            background-color: #4CAF50;
            color: white;
        }

        .footer-tabs .navbar .customer_list {
            margin-right: 2px;
            margin-left: 1px;
        }


    </style>
@endsection


@section('extra-js')

@endsection