<div class="footer-tabs visible-xs">
    <div class="navbar">
        <div class="" style="margin-top: 5px;margin-bottom: 5px">
            <div class="col-xs-5" style="margin-top: 5px">
                <label for="">
                    <small>Payment Link
                    @if($lead_qoute->payment_link)
                        <span class="text-muted"> ({{ucfirst(strtolower($lead_qoute->tr_type))}})</span>
                    @endif
                    </small>
                </label>
            </div>
            <div class="col-xs-7">
                @if($lead_qoute->payment_link)
                    <form action="{{route('sendEmail')}}" method="post">
                        <div class="input-group input-group-sm">
                            {{csrf_field()}}
                            <input type="text" class="form-control" placeholder="Payment Link" name="content" required
                                   value="{{$lead_qoute->payment_link}}" readonly>
                            <input type="hidden" name="email" value="{{$lead_details->user_email}}">
                            <input type="hidden" name="type" value="payment_link">
                            <input type="hidden" name="user_name" value="{{$lead_details->user_fname}}">
                            <span class="input-group-btn">
                            <button type="submit" class="btn btn-warning">Send</button>
                        </span>
                        </div>
                    </form>
                @else
                    <button class="btn btn-link btn-sm"
                            onclick="javascript:window.location.href = '{{route('customerPayments',['quote_no'=>$quote_no,'lead_no'=>$lead_no])}}';return false;">
                        Select payment Method
                    </button>
                @endif
                <div class="clearfix"></div>
                @if(session()->has('email_sent'))
                    <div class="footer_success">
                        {{ session()->get('email_sent') }}
                    </div>
                @endif
            </div>

            <div class="clearfix"></div>
        </div>
        <div class="row">
            <div class="col-xs-4 pr-2">
                <button class="btn btn-block btn-primary"
                        onclick="javascript:window.location.href = '{{route('customerPayments',['quote_no'=>$quote_no,'lead_no'=>$lead_no])}}';return false;">
                    Payments
                </button>
            </div>
            <div class="col-xs-5 padding-2">
                <button class="btn btn-block btn-primary"
                        onclick="javascript:window.location.href = '{{route('customerDocuments',['quote_no'=>$quote_no,'lead_no'=>$lead_no])}}';return false;">
                    Documentation
                </button>
            </div>
            <div class="col-xs-3 pl-2 ">
                <button class="btn btn-block btn-danger"
                        onclick="javascript:window.location.href = '{{route('quotationInsurenceSingle',['quote_no'=>$quote_no])}}';return false;">
                    <i class="ion-android-arrow-back"></i> Back
                </button>
            </div>
        </div>
    </div>
</div>