<div class="footer-tabs visible-xs">
    <div class="navbar">
        <div class="" style="margin-top: 5px;margin-bottom: 5px">
            <div class="col-xs-4" style="margin-top: 5px">
                <label for="">
                    <small>Payment Link</small>
                </label>
            </div>
            <div class="col-xs-8">
                <form action="{{route('sendEmail')}}" method="post">
                    <div class="input-group input-group-sm">
                        {{csrf_field()}}
                        <input type="text" class="form-control" placeholder="Payment Link" name="content" required>
                        <input type="hidden" name="email" value="{{$lead_details->user_email}}">
                        <input type="hidden" name="type" value="payment_link">
                        <input type="hidden" name="user_name" value="{{$lead_details->user_fname}}">
                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-warning">Send</button>
                        </span>
                    </div>
                </form>
                <div class="clearfix"></div>
                @if(session()->has('email_sent'))
                    <div class="footer_success">
                        {{ session()->get('email_sent') }}
                    </div>
                @endif
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="row">
            <div class="col-xs-4 pr-2 ">
                <form action="{{route('sendEmail')}}" method="post">
                    {{csrf_field()}}
                    <input type="hidden" name="email" value="{{$lead_details->user_email}}">
                    <input type="hidden" name="content" class="content">
                    <input type="hidden" name="type" value="quotes">
                    <input type="hidden" name="user_name" value="{{$lead_details->user_fname}}">
                    <button class="btn btn-block btn-primary" type="submit">Send Quote</button>
                </form>
            </div>
            <div class="col-xs-5 padding-2">
                <button class="btn btn-block btn-primary" data-toggle="modal" data-target="#sendto">Send Quote To...
                </button>
            </div>
            <div class="col-xs-3 pl-2 ">
                <button class="btn btn-block btn-danger" onclick="javascript:window.location.href = '{{route('listBusiness')}}';return false;"><i
                            class="ion-android-arrow-back"></i> Back
                </button>
            </div>
        </div>
    </div>
</div>
<div id="sendto" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Send Quote To...</h4>
            </div>
            <div class="modal-body">
                <form action="{{route('sendEmail')}}" method="post" class="form-inline">
                    {{csrf_field()}}
                    <div class="form-group">
                        <input type="email" name="email" class="form-control" placeholder="Email Address" required>
                    </div>
                    <div class="form-group">
                        <input type="text" name="cc_email" class="form-control" placeholder="CC Email Addresses">
                    </div>
                    <input type="hidden" name="content" class="content">
                    <input type="hidden" name="type" value="quotes_to">
                    <input type="hidden" name="user_name" value="">
                    <div class="form-group">
                        <button class="btn btn-sm btn-primary" type="submit">Send Quote</button>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>
