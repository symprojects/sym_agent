@extends('layouts.dashboard-layout')

@section('content')
    <section class="content-header">
        <!--<h1 class="text-center visible-xs">-->
        <!--Customers-->
        <!--</h1>-->
        <h1 class="hidden-xs">
            Insurance Company
        </h1>
        <ol class="breadcrumb hidden-xs">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Insurance Company</a></li>
            <!-- <li class="active">Blank page</li> -->
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-4 col-md-4 col-lg-4 col-xs-12 text-center">
                        <h4 class="text-center">Documentation</h4>
                        <hr>
                        @if(session()->has('success'))
                            <div class="alert alert-success">
                                {{ session()->get('success') }}
                            </div>
                        @endif
                        @if(session()->has('error'))
                            <div class="alert alert-danger">
                                {{ session()->get('error') }}
                            </div>
                        @endif
                        <div class="alert alert-danger" id="js-errors">
                            <ul>
                                <li></li>
                            </ul>
                        </div>
                        <div action="#" class="form-horizontal">

                            <div class="col-sm-8 col-md-8 col-lg-8 col-xs-12">
                                <div class="form-group">
                                    <label for="inputEmail3"
                                           class="col-xs-6 control-label text-right">Vehicle Reg No:</label>
                                    <div class="col-xs-6">
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="vehicle_reg_no"
                                                   placeholder="Vehicle Reg"
                                                   value="{{($docment_details != null AND $docment_details->vehicle_reg_no)?$docment_details->vehicle_reg_no : $lead_details->vehicle_reg_no}}">
                                        </div>
                                    </div>
                                </div>
                                {{--{{$docment_details}}--}}

                                <div class="form-group">
                                    <label for="inputEmail3"
                                           class="col-xs-6 control-label text-right">Chassis No:</label>
                                    <div class="col-xs-6">
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="chassis_no"
                                                   value="{{($docment_details != null AND $docment_details->chassis_no)?$docment_details->chassis_no : ''}}"
                                                   placeholder="Chassis No">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputEmail3" class="col-xs-6 control-label text-right">Engine
                                        No:</label>
                                    <div class="col-xs-6">
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="engine_no"
                                                   value="{{($docment_details != null AND $docment_details->engine_no)?$docment_details->engine_no : ''}}"
                                                   placeholder="Engine No">
                                        </div>
                                    </div>
                                </div>

                                <form action="#" enctype="multipart/form-data" id="vrc_form">
                                    <input type="hidden" name="lead_id" value="{{$lead_no}}">
                                    <input type="hidden" name="field" value="vrc">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-xs-6 control-label text-right">VRC</label>

                                        <div class="col-xs-3">
                                            <div class="fileUpload">
                                                <span>upload </span>
                                                <input type="file" class="upload" name="vrc" id="vrc_file">
                                            </div>
                                        </div>
                                        <div class="col-xs-3">
                                            @if($docment_details != null AND $docment_details->vrc)
                                                <button class="btn btn-info btn-block btn-sm btn-flat"
                                                        data-toggle="modal" data-target="#vrc_modal" type="button">View
                                                </button>
                                                <div id="vrc_modal" class="modal fade" role="dialog">
                                                    <div class="modal-dialog">

                                                        <!-- Modal content-->
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close"
                                                                        data-dismiss="modal">&times;
                                                                </button>
                                                                <h4 class="modal-title">VRC</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <img src="{{asset('')}}{{ltrim($docment_details->vrc, './')}}"
                                                                     alt="" class="img-responsive">
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </form>

                                <form action="#" enctype="multipart/form-data" id="nic_form">
                                    <input type="hidden" name="lead_id" value="{{$lead_no}}">
                                    <input type="hidden" name="field" value="nic">
                                    <div class="form-group">
                                        <label for="inputEmail3"
                                               class="col-xs-6 control-label text-right">NIC/License</label>
                                        <div class="col-xs-3">
                                            <div class="fileUpload">
                                                <span>upload </span>
                                                <input type="file" class="upload" name="nic" id="nic_file">
                                            </div>
                                        </div>
                                        <div class="col-xs-3">
                                            @if($docment_details != null AND $docment_details->nic)
                                                <button class="btn btn-info btn-block btn-sm btn-flat"
                                                        data-toggle="modal" data-target="#nic_modal" type="button">View
                                                </button>
                                                <div id="nic_modal" class="modal fade" role="dialog">
                                                    <div class="modal-dialog">

                                                        <!-- Modal content-->
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close"
                                                                        data-dismiss="modal">&times;
                                                                </button>
                                                                <h4 class="modal-title">NIC/License</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <img src="{{asset('')}}{{ltrim($docment_details->nic, './')}}"
                                                                     alt="" class="img-responsive">
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </form>

                                <form action="#" enctype="multipart/form-data" id="proposal_form">
                                    <input type="hidden" name="lead_id" value="{{$lead_no}}">
                                    <input type="hidden" name="field" value="proposal">
                                    <div class="form-group">
                                        <label for="inputEmail3"
                                               class="col-xs-6 control-label text-right">Proposal Form</label>
                                        <div class="col-xs-3">
                                            <div class="fileUpload">
                                                <span>upload </span>
                                                <input type="file" class="upload" name="proposal" id="proposal_file">
                                            </div>
                                        </div>
                                        <div class="col-xs-3">
                                            @if($docment_details != null AND $docment_details->proposal)
                                                <button class="btn btn-info btn-block btn-sm btn-flat"
                                                        data-toggle="modal" data-target="#proposal_modal" type="button">View
                                                </button>
                                                <div id="proposal_modal" class="modal fade" role="dialog">
                                                    <div class="modal-dialog">

                                                        <!-- Modal content-->
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close"
                                                                        data-dismiss="modal">&times;
                                                                </button>
                                                                <h4 class="modal-title">Proposal Form</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <img src="{{asset('')}}{{ltrim($docment_details->proposal, './')}}"
                                                                     alt="" class="img-responsive">
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </form>

                                <form action="#" enctype="multipart/form-data" id="inspection_form">
                                    <input type="hidden" name="lead_id" value="{{$lead_no}}">
                                    <input type="hidden" name="field" value="inspection">
                                    <div class="form-group">
                                        <label for="inputEmail3"
                                               class="col-xs-6 control-label text-right">Inspection Form</label>
                                        <div class="col-xs-3">
                                            <div class="fileUpload">
                                                <span>upload </span>
                                                <input type="file" class="upload" name="inspection" id="inspection_file">
                                            </div>
                                        </div>
                                        <div class="col-xs-3">
                                            @if($docment_details != null AND $docment_details->inspection)
                                                <button class="btn btn-info btn-block btn-sm btn-flat"
                                                        data-toggle="modal" data-target="#inspection_modal" type="button">View
                                                </button>
                                                <div id="inspection_modal" class="modal fade" role="dialog">
                                                    <div class="modal-dialog">

                                                        <!-- Modal content-->
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close"
                                                                        data-dismiss="modal">&times;
                                                                </button>
                                                                <h4 class="modal-title">Inspection Form</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <img src="{{asset('')}}{{ltrim($docment_details->inspection, './')}}"
                                                                     alt="" class="img-responsive">
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </form>

                                <form action="#" enctype="multipart/form-data" id="loa_form">
                                    <input type="hidden" name="lead_id" value="{{$lead_no}}">
                                    <input type="hidden" name="field" value="loa">
                                    <div class="form-group">
                                        <label for="inputEmail3"
                                               class="col-xs-6 control-label text-right">LOA</label>
                                        <div class="col-xs-3">
                                            <div class="fileUpload">
                                                <span>upload </span>
                                                <input type="file" class="upload" name="loa" id="loa_file">
                                            </div>
                                        </div>
                                        <div class="col-xs-3">
                                            @if($docment_details != null AND $docment_details->loa)
                                                <button class="btn btn-info btn-block btn-sm btn-flat"
                                                        data-toggle="modal" data-target="#loa_modal" type="button">View
                                                </button>
                                                <div id="loa_modal" class="modal fade" role="dialog">
                                                    <div class="modal-dialog">

                                                        <!-- Modal content-->
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close"
                                                                        data-dismiss="modal">&times;
                                                                </button>
                                                                <h4 class="modal-title">LOA</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <img src="{{asset('')}}{{ltrim($docment_details->loa, './')}}"
                                                                     alt="" class="img-responsive">
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </form>

                                <form action="#" enctype="multipart/form-data" id="temp_cover_note_form">
                                    <input type="hidden" name="lead_id" value="{{$lead_no}}">
                                    <input type="hidden" name="field" value="temp_cover_note">
                                    <div class="form-group">
                                        <label for="inputEmail3"
                                               class="col-xs-6 control-label text-right">Temporary Cover
                                            Note</label>
                                        <div class="col-xs-3">
                                            <div class="fileUpload">
                                                <span>upload </span>
                                                <input type="file" class="upload" name="temp_cover_note" id="temp_cover_note_file">
                                            </div>
                                        </div>
                                        <div class="col-xs-3">
                                            @if($docment_details != null AND $docment_details->temp_cover_note)
                                                <button class="btn btn-info btn-block btn-sm btn-flat"
                                                        data-toggle="modal" data-target="#temp_cover_note_modal" type="button">View
                                                </button>
                                                <div id="temp_cover_note_modal" class="modal fade" role="dialog">
                                                    <div class="modal-dialog">

                                                        <!-- Modal content-->
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close"
                                                                        data-dismiss="modal">&times;
                                                                </button>
                                                                <h4 class="modal-title">Temporary Cover Note</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <img src="{{asset('')}}{{ltrim($docment_details->temp_cover_note, './')}}"
                                                                     alt="" class="img-responsive">
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </form>

                            </div>
                            {{--<div class="row">--}}
                            {{--<div class="col-xs-6 col-md-6 col-lg-6 pr-2">--}}
                            {{--<button type="button" class="btn btn-warning btn-block" onclick="window.print();">Print Invoice</button>--}}

                            {{--</div>--}}
                            {{--<!-- /.col -->--}}
                            {{--<div class="col-xs-6 col-md-6 col-lg-6  pl-2">--}}
                            {{--<button type="submit" class="btn btn-warning btn-block">Send Invoice</button>--}}
                            {{--</div>--}}
                            {{--<!-- /.col -->--}}
                            {{--</div>--}}
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.box -->

    </section>

    @include('partials.payment-doc-back')

@endsection

@section('extra-css')
    <style>

        .footer-tabs .navbar {
            overflow: hidden;
            background-color: rgba(255, 255, 255, 1);
            position: fixed;
            bottom: -1px;
            width: 100%;
            margin: 0px !important;
            border-radius: 6px 6px 0px 0px;
            min-height: 76px !important;

        }

        .footer-tabs .add-business {
            padding: 9px 15px !important;
            border-radius: 100% !important;
            margin-top: 7px;
            margin-right: 2px;
            margin-left: 2px;
            border: 2px solid #09708a !important;
            color: #fff !important;
            background: #09708a !important;
        }

        .footer-tabs .add-user {
            padding: 9px 14px !important;
            border-radius: 100% !important;
            margin-top: 7px;
            margin-right: 2px;
            margin-left: 2px;
            border: 2px solid #09708a !important;
            color: #09708a !important;
            background: #fff !important;
        }

        .footer-tabs .hot-leads {

        }

        .footer-tabs .navbar a {
            float: left;
            display: block;
            color: #fff;
            text-align: center;
            padding: 10px 8px;
            font-weight: 600;
            text-decoration: none;
            font-size: 17px;
            border-radius: 10px 10px 0px 0px;
            background: #09708a;
            border: 1px solid #09708a;
            margin-top: 3px;
        }

        .footer-tabs .navbar a:hover {
            background: #f1f1f1;
            color: black;
        }

        .footer-tabs .navbar a.active {
            background-color: #4CAF50;
            color: white;
        }

        .footer-tabs .navbar .customer_list {
            margin-right: 2px;
            margin-left: 1px;
        }

        /*-----------------------------------------------------------*/

        .toggle {
            position: relative;
            display: block;
            width: 40px;
            height: 20px;
            cursor: pointer;
            -webkit-tap-highlight-color: transparent;
            transform: translate3d(0, 0, 0);
        }

        .toggle:before {
            content: "";
            position: relative;
            top: 3px;
            left: 3px;
            width: 34px;
            height: 14px;
            display: block;
            background: #9A9999;
            border-radius: 8px;
            transition: background 0.2s ease;
        }

        .toggle span {
            position: absolute;
            top: 0;
            left: 0;
            width: 20px;
            height: 20px;
            display: block;
            background: white;
            border-radius: 10px;
            box-shadow: 0 3px 8px rgba(154, 153, 153, 0.5);
            transition: all 0.2s ease;
        }

        .toggle span:before {
            content: "";
            position: absolute;
            display: block;
            margin: -18px;
            width: 56px;
            height: 56px;
            background: rgba(79, 46, 220, 0.5);
            border-radius: 50%;
            transform: scale(0);
            opacity: 1;
            pointer-events: none;
        }

        .toggle_check:checked + .toggle:before {
            background: #947ADA;
        }

        .toggle_check:checked + .toggle span {
            background: #4F2EDC;
            transform: translateX(20px);
            transition: all 0.2s cubic-bezier(0.8, 0.4, 0.3, 1.25), background 0.15s ease;
            box-shadow: 0 3px 8px rgba(79, 46, 220, 0.2);
        }

        .toggle_check:checked + .toggle span:before {
            transform: scale(1);
            opacity: 0;
            transition: all 0.4s ease;
        }

        /*--------------------------*/
        .fileUpload {
            position: relative;
            overflow: hidden;
            border: 1px solid #ccc;
            display: inline-block;
            padding: 4px 10px;
            margin-left: 0px;
            cursor: pointer;
            margin-bottom: -5px;
        }

        .fileUpload input.upload {
            position: absolute;
            top: 0;
            right: 0;
            margin: 0;
            padding: 0;
            font-size: 20px;
            cursor: pointer;
            opacity: 0;
            filter: alpha(opacity=0);
        }
        #js-errors {
            padding: 3px !important;
            display: none;
        }

        #js-errors ul {
            padding: 0px;
            margin: 0px;
            list-style-type: none;
            font-size: 12px;
        }
    </style>
@endsection


@section('extra-js')
    <script>
        var quote_no = {{$quote_no}};
        var lead_no = {{$lead_no}};
        var remote_url = "{{env('remote_url')}}";
        var token = $('meta[name="csrf-token"]').attr('content');

        // -------------------------------------------
        $('#vehicle_reg_no').blur(function (e) {
            e.preventDefault();
            var vehicle_reg_no = $('#vehicle_reg_no').val();
            if (vehicle_reg_no == '' || vehicle_reg_no.length < 5) {
                $('#js-errors').show();
                $('#js-errors ul').html('Enter valid vehical reg no');
                $('#vehicle_reg_no').css('border', '1px solid #d01c1c');
            } else {
                $('#js-errors').hide();
                $('#vehicle_reg_no').css('border', '1px solid #17c32a');
                $.ajax({
                    type: "post",
                    url: '/customer/business/documents/save',
                    data: {'_token': token, quote_no: quote_no, lead_no: lead_no, vehicle_reg_no: vehicle_reg_no},
                    success: function (res) {

                    }
                });
            }
        });

        $('#chassis_no').blur(function (e) {
            e.preventDefault();
            var chassis_no = $('#chassis_no').val();
            if (chassis_no == '' || chassis_no.length < 5) {
                $('#js-errors').show();
                $('#js-errors ul').html('Enter valid chassis no');
                $('#chassis_no').css('border', '1px solid #d01c1c');
            } else {
                $('#js-errors').hide();
                $('#chassis_no').css('border', '1px solid #17c32a');
                $.ajax({
                    type: "post",
                    url: '/customer/business/documents/save',
                    data: {'_token': token, quote_no: quote_no, lead_no: lead_no, chassis_no: chassis_no},
                    success: function (res) {

                    }
                });
            }
        });

        $('#engine_no').blur(function (e) {
            e.preventDefault();
            var engine_no = $('#engine_no').val();
            if (engine_no == '' || engine_no.length < 5) {
                $('#js-errors').show();
                $('#js-errors ul').html('Enter valid engine no');
                $('#engine_no').css('border', '1px solid #d01c1c');
            } else {
                $('#js-errors').hide();
                $('#engine_no').css('border', '1px solid #17c32a');
                $.ajax({
                    type: "post",
                    url: '/customer/business/documents/save',
                    data: {'_token': token, quote_no: quote_no, lead_no: lead_no, engine_no: engine_no},
                    success: function (res) {

                    }
                });
            }
        });

        // ---------------------------upload---------------

        $(document).ready(function (e) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            // ------------------------vrc--------------------
            $('#vrc_form').on('submit', (function (e) {
                e.preventDefault();
                var formData = new FormData(this);

                $.ajax({
                    type: 'POST',
                    url: '/customer/business/documents/file/upload',
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        if (data) {
                            location.reload();
                        }
                    },
                    error: function (data) {
                        console.log("error");
                        console.log(data);
                    }
                });
            }));

            $("#vrc_file").on("change", function () {
                // alert('asdasdsadsa');
                $("#vrc_form").submit();
            });

            // ------------------------nic--------------------
            $('#nic_form').on('submit', (function (e) {
                e.preventDefault();
                var formData = new FormData(this);

                $.ajax({
                    type: 'POST',
                    url: '/customer/business/documents/file/upload',
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        if (data) {
                            location.reload();
                        }
                    },
                    error: function (data) {
                        console.log("error");
                        console.log(data);
                    }
                });
            }));

            $("#nic_file").on("change", function () {
                // alert('asdasdsadsa');
                $("#nic_form").submit();
            });

            // ------------------------proposal--------------------
            $('#proposal_form').on('submit', (function (e) {
                e.preventDefault();
                var formData = new FormData(this);

                $.ajax({
                    type: 'POST',
                    url: '/customer/business/documents/file/upload',
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        if (data) {
                            location.reload();
                        }
                    },
                    error: function (data) {
                        console.log("error");
                        console.log(data);
                    }
                });
            }));

            $("#proposal_file").on("change", function () {
                // alert('asdasdsadsa');
                $("#proposal_form").submit();
            });

            // ------------------------inspection--------------------
            $('#inspection_form').on('submit', (function (e) {
                e.preventDefault();
                var formData = new FormData(this);

                $.ajax({
                    type: 'POST',
                    url: '/customer/business/documents/file/upload',
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        if (data) {
                            location.reload();
                        }
                    },
                    error: function (data) {
                        console.log("error");
                        console.log(data);
                    }
                });
            }));

            $("#inspection_file").on("change", function () {
                // alert('asdasdsadsa');
                $("#inspection_form").submit();
            });

            // ------------------------LOA--------------------
            $('#loa_form').on('submit', (function (e) {
                e.preventDefault();
                var formData = new FormData(this);

                $.ajax({
                    type: 'POST',
                    url: '/customer/business/documents/file/upload',
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        if (data) {
                            location.reload();
                        }
                    },
                    error: function (data) {
                        console.log("error");
                        console.log(data);
                    }
                });
            }));

            $("#loa_file").on("change", function () {
                // alert('asdasdsadsa');
                $("#loa_form").submit();
            });

            // ------------------------LOA--------------------
            $('#temp_cover_note_form').on('submit', (function (e) {
                e.preventDefault();
                var formData = new FormData(this);

                $.ajax({
                    type: 'POST',
                    url: '/customer/business/documents/file/upload',
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        if (data) {
                            location.reload();
                        }
                    },
                    error: function (data) {
                        console.log("error");
                        console.log(data);
                    }
                });
            }));

            $("#temp_cover_note_file").on("change", function () {
                // alert('asdasdsadsa');
                $("#temp_cover_note_form").submit();
            });
        });
    </script>
@endsection