@extends('layouts.dashboard-layout')

@section('content')
    <section class="content-header">
        <!--<h1 class="text-center visible-xs">-->
        <!--Customers-->
        <!--</h1>-->
        <h1 class="hidden-xs">
            Insurance Company
        </h1>
        <ol class="breadcrumb hidden-xs">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Insurance Company</a></li>
            <!-- <li class="active">Blank page</li> -->
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-4 col-md-4 col-lg-4 col-xs-12 text-center">
                        <img id="ins_com_logo"
                             style="width:20%;height: auto;object-fit: cover;display: inline;float: left">
                        <h4 style="width: 80%;display: inline-block" class="text-left">payments</h4>
                        <hr>
                        <div class="form-horizontal">
                            <div class="col-sm-8 col-md-8 col-lg-8 col-xs-12">
                                <div class="form-group">
                                    <label for="inputEmail3"
                                           class="col-xs-6 control-label text-right">Payment Method</label>
                                    <div class="col-xs-6">
                                        <div class="input-group">
                                            <select name="payment_method" id="payment_method" class="form-control">
                                                <option value="">Select Payment Method</option>
                                                <option value="SAMPATH" {{($lead_qoute AND $lead_qoute->tr_type == 'SAMPATH')?'selected':''}}>Sampath Visa/Master</option>
                                                <option value="COMBANK" {{($lead_qoute AND $lead_qoute->tr_type == 'COMBANK')?'selected':''}}>Commercial Visa/Master</option>
                                                <option value="AMEX" {{($lead_qoute AND $lead_qoute->tr_type == 'AMEX')?'selected':''}}>Nations Trust Amex</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" id="paymnet_link_div">
                                    <label for="inputEmail3"
                                           class="col-xs-6 control-label text-right">Payment Link</label>
                                    <div class="col-xs-6">
                                        <form action="{{route('sendEmail')}}" method="post">
                                            <div class="input-group input-group-sm">
                                                {{csrf_field()}}
                                                <input type="text" class="form-control" placeholder="Payment Link" value="{{$lead_qoute->payment_link}}"
                                                       name="content" required="required" readonly="readonly" id="payment_link_input">
                                                <div class="footer_success"></div>
                                                <input type="hidden" name="email" value="{{$lead_details->user_email}}">
                                                <input type="hidden" name="type" value="payment_link">
                                                <input type="hidden" name="user_name"
                                                       value="{{$lead_details->user_fname}}">
                                                <span class="input-group-btn">
                                                    <button type="submit" class="btn btn-info btn-flat">Send</button>
                                                </span>
                                            </div>
                                        </form>
                                        <div class="clearfix"></div>
                                        @if(session()->has('email_sent'))
                                            <div class="footer_success">
                                                {{ session()->get('email_sent') }}
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-xs-6 control-label text-right">Advance
                                        Paid</label>
                                    <div class="col-xs-6">
                                        <div class="input-group">
                                            <span class="input-group-addon">LKR</span>
                                            <input type="number" class="form-control" id="advance_paid"
                                                   placeholder="Advance Paid" value="{{$lead_qoute->advance_payment}}">
                                        </div>
                                    </div>
                                </div>

                                <form action="#" enctype="multipart/form-data" id="debit_note_form">
                                    <input type="hidden" name="lead_id" value="{{$lead_no}}">
                                    <input type="hidden" name="quote_no" value="{{$quote_no}}">
                                    <input type="hidden" name="field" value="debit_note">
                                    <div class="form-group">
                                        <label for="inputEmail3"
                                               class="col-xs-6 control-label text-right">Debit Note</label>
                                        <div class="col-xs-3">
                                            <div class="fileUpload">
                                                <span>upload </span>
                                                <input type="file" class="upload" name="debit_note" id="debit_note_file">
                                            </div>
                                        </div>
                                        <div class="col-xs-3">
                                            @if($lead_qoute->debit_note)
                                                <button class="btn btn-info btn-block btn-sm btn-flat"
                                                        data-toggle="modal" data-target="#debit_note_modal" type="button">
                                                    View
                                                </button>
                                                <div id="debit_note_modal" class="modal fade" role="dialog">
                                                    <div class="modal-dialog">

                                                        <!-- Modal content-->
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close"
                                                                        data-dismiss="modal">&times;
                                                                </button>
                                                                <h4 class="modal-title">Debit Note</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <img src="{{asset('')}}{{ltrim($lead_qoute->debit_note, './')}}"
                                                                     alt="" class="img-responsive">
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </form>

                                <div class="form-group">
                                    <label for="inputEmail3" class="col-xs-6 control-label text-right">Policy Start
                                        Date</label>
                                    <div class="col-xs-6">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control pull-right" id="datepicker" value="{{$lead_qoute->policy_start_date}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-xs-6 control-label text-right">Balance
                                        Payment</label>
                                    <div class="col-xs-6">
                                        <div class="input-group">
                                            <span class="input-group-addon">LKR</span>
                                            <input type="number" class="form-control" id="balance_payment"
                                                   value="{{$lead_qoute->balance_payment}}"
                                                   placeholder="Balance Payment" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-xs-6 control-label text-right">Credit
                                        Period</label>
                                    <div class="col-xs-6">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control pull-right" id="reservation" value="{{$lead_qoute->credit_period_start_date}} - {{$lead_qoute->credit_period_end_date}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-xs-6 control-label text-right">Days
                                        Remaining <small class="text-muted">From today</small></label>
                                    <div class="col-xs-6">
                                        <div class="input-group">
                                            <input type="number" class="form-control" id="days_remaining"
                                                   placeholder="Days Remaining" value="{{$lead_qoute->days_remaining}}" readonly>
                                            <span class="input-group-addon">Days</span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            {{--<div class="row">--}}
                            {{--<div class="col-xs-6 col-md-6 col-lg-6 pr-2">--}}
                            {{--<button type="button" class="btn btn-warning btn-block" onclick="window.print();">Print Invoice</button>--}}

                            {{--</div>--}}
                            {{--<!-- /.col -->--}}
                            {{--<div class="col-xs-6 col-md-6 col-lg-6  pl-2">--}}
                            {{--<button type="submit" class="btn btn-warning btn-block">Send Invoice</button>--}}
                            {{--</div>--}}
                            {{--<!-- /.col -->--}}
                            {{--</div>--}}
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.box -->

    </section>

    @include('partials.payment-doc-back')

@endsection

@section('extra-css')
    <style>

        .footer-tabs .navbar {
            overflow: hidden;
            background-color: rgba(255, 255, 255, 1);
            position: fixed;
            bottom: -1px;
            width: 100%;
            margin: 0px !important;
            border-radius: 6px 6px 0px 0px;
            min-height: 76px !important;

        }

        .footer-tabs .add-business {
            padding: 9px 15px !important;
            border-radius: 100% !important;
            margin-top: 7px;
            margin-right: 2px;
            margin-left: 2px;
            border: 2px solid #09708a !important;
            color: #fff !important;
            background: #09708a !important;
        }

        .footer-tabs .add-user {
            padding: 9px 14px !important;
            border-radius: 100% !important;
            margin-top: 7px;
            margin-right: 2px;
            margin-left: 2px;
            border: 2px solid #09708a !important;
            color: #09708a !important;
            background: #fff !important;
        }

        .footer-tabs .hot-leads {

        }

        .footer-tabs .navbar a {
            float: left;
            display: block;
            color: #fff;
            text-align: center;
            padding: 10px 8px;
            font-weight: 600;
            text-decoration: none;
            font-size: 17px;
            border-radius: 10px 10px 0px 0px;
            background: #09708a;
            border: 1px solid #09708a;
            margin-top: 3px;
        }

        .footer-tabs .navbar a:hover {
            background: #f1f1f1;
            color: black;
        }

        .footer-tabs .navbar a.active {
            background-color: #4CAF50;
            color: white;
        }

        .footer-tabs .navbar .customer_list {
            margin-right: 2px;
            margin-left: 1px;
        }


    </style>
@endsection


@section('extra-js')
    <script>
        $('#datepicker').datepicker({
            autoclose: true,
            format:"yyyy-mm-dd",
            startDate:"{{\Carbon\Carbon::parse($lead_qoute->credit_period_start_date)->format('Y/m/d')}}",
        });
        $('#reservation').daterangepicker({
            autoApply: true,
            format:"yyyy-dd-mm",
            'startDate' : {{\Carbon\Carbon::parse($lead_qoute->credit_period_start_date)->format('Y-m-d')}},
            'endDate' : {{\Carbon\Carbon::parse($lead_qoute->credit_period_end_date)->format('Y-m-d')}},
            {{--}, function(start, end, label) {--}}
            {{--console.log('New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')');--}}
        });
    </script>

    <script>
        var quote_no = {{$quote_no}};
        var lead_no = {{$lead_no}};
        var remote_url = "{{env('remote_url')}}";
        var token = $('meta[name="csrf-token"]').attr('content');
        var front_url = $('meta[name="siteURL"]').attr('content');
        var amount = 0;
        var discounted_value = 0;

        $(document).ready(function () {
            @if( $lead_qoute->payment_link == null)
            $('#paymnet_link_div button').attr("disabled", true);
            @endif

            $.ajax({
                type: "post",
                url: remote_url + '/api/quote/insurence/single/' + quote_no,
                data: {'_token': token, quote_no: quote_no},
                success: function (res) {
                    console.log(res);
                    console.log(res[0].quote_no);
                    $('#amount').val(res[0].quote_value).attr('readonly', 'readonly');
                    $('#discount').val(calcDiscount(res[0].quote_value, res[0].discounted_value)).attr('readonly', 'readonly');
                    $('#final_amount').val(res[0].discounted_value).attr('readonly', 'readonly');
                    $('#discount_rate').val(calcDiscountRate(calcDiscount(res[0].quote_value, res[0].discounted_value), res[0].quote_value)).attr('readonly', 'readonly');
                    $('#ins_com_logo').attr('src', res[0].picture);
                    amount=res[0].quote_value;
                    discounted_value=res[0].discounted_value;
                }
            });
//            calc remaing date
            $.ajax({
                type: "post",
                url: '/calc/remain/dates',
                data: {'_token': token, quote_no: quote_no},
                success: function (res) {
                    $('#days_remaining').val(res);
                }
            });
        });

        function calcDiscountRate(discount, amount) {
            return (discount / amount) * 100;
        }

        function calcDiscount(quote_value, discounted_value) {
            return parseFloat(quote_value) - parseFloat(discounted_value);
        }

        $('#payment_method').on('change', function (e) {
            var payment = $('#payment_method').val();
            if (payment != '') {
                $.ajax({
                    type: "post",
                    url: remote_url + '/api/quote/payment/type/save/' + lead_no,
                    data: {'_token': token, quote_no: quote_no, lead_no: lead_no, payment: payment},
                    success: function (res) {
                        console.log(res);
                        if (res.status) {
                            $.ajax({
                                type: "post",
                                url: '/payments/type/save',
                                data: {'_token': token, quote_no: quote_no, lead_no: lead_no, payment: payment,lead_token:res.lead.lead_token, front_url:front_url},
                                success: function (res2) {
                                    $('#payment_method').css('border', '1px solid #19be1f');

                                    $('#paymnet_link_div button').attr("disabled", false);
                                    $('#payment_link_input').val(res2.payment_link);
                                    console.log(res2);
                                    setTimeout(function () {
                                        location.reload();
                                    },2000);
                                }
                            });
                        }
                    }
                });
            }
        });

        // ------------------------Debit Note--------------------
        $('#debit_note_form').on('submit', (function (e) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            e.preventDefault();
            var formData = new FormData(this);

            $.ajax({
                type: 'POST',
                url: '/customer/business/documents/file/upload',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data) {
                        location.reload();
                    }
                },
                error: function (data) {
                    console.log("error");
                    console.log(data);
                }
            });
        }));

        $("#debit_note_file").on("change", function () {
            // alert('asdasdsadsa');
            $("#debit_note_form").submit();
        });

        $('#datepicker').on("change",function () {
            var policy_start_date =$('#datepicker').val();
            $.ajax({
                type: "post",
                url: '/customer/business/payment/save',
                data: {'_token': token, quote_no: quote_no, type:'policy_start_date',value:policy_start_date},
                success: function (res) {
                    if (res.status){
                        if (res.type == 'policy_start_date') {
                            $('#datepicker').css('border','1px solid #38b01a');
                            $('#days_remaining').val(res.days);
                        }
                    }
                }
            });
        });

        $('#advance_paid').on("blur",function () {
            var advance_paid =$('#advance_paid').val();
            var balance_payment =discounted_value -parseFloat(advance_paid);

            $('#balance_payment').val((balance_payment>0)?balance_payment:0);
            $.ajax({
                type: "post",
                url: '/customer/business/payment/save',
                data: {'_token': token, quote_no: quote_no, type:'advance_paid',value:advance_paid},
                success: function (res) {
                    if (res.status){
                        if (res.type == 'advance_paid') {
                            $('#advance_paid').css('border','1px solid #38b01a');
                        }
                    }
                }
            });
            $.ajax({
                type: "post",
                url: '/customer/business/payment/save',
                data: {'_token': token, quote_no: quote_no, type:'balance_payment',value:balance_payment},
                success: function (res) {
                    if (res.status){
                        if (res.type == 'balance_payment') {
                            $('#balance_payment').css('border','1px solid #38b01a');
                        }
                    }
                }
            });
        });

        $('#reservation').on("change",function () {
            var credit_period =$('#reservation').val();
            $.ajax({
                type: "post",
                url: '/customer/business/payment/save',
                data: {'_token': token, quote_no: quote_no, type:'credit_period',value:credit_period},
                success: function (res) {
                    if (res.status){
                        if (res.type == 'credit_period') {
                            $('#reservation').css('border','1px solid #38b01a');
                        }
                    }
                }
            });
        });

//        $('#days_remaining').on("blur",function () {
//            var days_remaining =$('#days_remaining').val();
//            $.ajax({
//                type: "post",
//                url: '/customer/business/payment/save',
//                data: {'_token': token, quote_no: quote_no, type:'days_remaining',value:days_remaining},
//                success: function (res) {
//                    if (res.status){
//                        if (res.type == 'days_remaining') {
//                            $('#days_remaining').css('border','1px solid #38b01a');
//                        }
//                    }
//                }
//            });
//        });
    </script>
@endsection