@extends('layouts.dashboard-layout')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="text-center visible-xs">
            Quotations
        </h1>
        <h1 class="hidden-xs">
            Quotations
        </h1>
        <ol class="breadcrumb hidden-xs">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Quotations</a></li>
            <!-- <li class="active">Blank page</li> -->
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box">
            <div class="box-body">
                @if(session()->has('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                @endif
                @if(session()->has('error'))
                    <div class="alert alert-danger">
                        {{ session()->get('error') }}
                    </div>
                @endif
                <div class="alert alert-danger" id="js-errors">
                    <ul>
                        <li></li>
                    </ul>
                </div>
                <form action="">
                    <div class="form-group">
                        <label for="" class="col-xs-12 col-md-4 no-padding">Price Negotiation</label>
                        <label for="required" class="col-xs-6 col-md-4">
                            <input type="radio" name="negotiation" value="required" checked="" id="required">
                            Required
                        </label>
                        <label for="notrequired" class="col-xs-6 col-md-4">
                            <input type="radio" name="negotiation" value="notrequired" checked="" id="notrequired">
                            Not Required
                        </label>
                        <div class="clearfix"></div>
                    </div>
                    {{--<div class="form-group has-feedback is_required">--}}
                    {{--<div class="row">--}}
                    {{--<div class="col-xs-6">--}}
                    {{--<input type="text" class="form-control" placeholder="Change Value">--}}
                    {{--</div>--}}
                    {{--<div class="col-xs-6">--}}
                    {{--<button class="btn btn-warning btn-block">Regenerate</button>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--<hr>--}}
                    {{--</div>--}}
                </form>

                <div id="ins_companies"></div>
            </div>

        </div>
        <!-- /.box -->

    </section>

    @include('partials.send-sendto-back')


@endsection

@section('extra-css')
    <style>

        .footer-tabs .navbar {
            overflow: hidden;
            background-color: rgba(255, 255, 255, 1);
            position: fixed;
            bottom: -1px;
            width: 100%;
            margin: 0px !important;
            border-radius: 6px 6px 0px 0px;
            min-height: 76px !important;

        }

        .footer-tabs .add-business {
            padding: 9px 15px !important;
            border-radius: 100% !important;
            margin-top: 7px;
            margin-right: 2px;
            margin-left: 2px;
            border: 2px solid #09708a !important;
            color: #fff !important;
            background: #09708a !important;
        }

        .footer-tabs .add-user {
            padding: 9px 14px !important;
            border-radius: 100% !important;
            margin-top: 7px;
            margin-right: 2px;
            margin-left: 2px;
            border: 2px solid #09708a !important;
            color: #09708a !important;
            background: #fff !important;
        }

        .footer-tabs .hot-leads {

        }

        .footer-tabs .navbar a {
            float: left;
            display: block;
            color: #fff;
            text-align: center;
            padding: 10px 8px;
            font-weight: 600;
            text-decoration: none;
            font-size: 17px;
            border-radius: 10px 10px 0px 0px;
            background: #09708a;
            border: 1px solid #09708a;
            margin-top: 3px;
        }

        .footer-tabs .navbar a:hover {
            background: #f1f1f1;
            color: black;
        }

        .footer-tabs .navbar a.active {
            background-color: #4CAF50;
            color: white;
        }

        .footer-tabs .navbar .customer_list {
            margin-right: 2px;
            margin-left: 1px;
        }

        .is_required {
            display: none;
        }

        .hide {
            display: none !important;
        }

        .show {
            display: block !important;
        }

        #js-errors {
            padding: 3px !important;
            display: none;
        }

        #js-errors ul {
            padding: 0px;
            margin: 0px;
            list-style-type: none;
            font-size: 12px;
        }

    </style>
@endsection


@section('extra-js')
    <script>
        var lead_id = {{$lead_id}};
        var remote_url = "{{env('remote_url')}}";
        var token = $('meta[name="csrf-token"]').attr('content');

        $(document).ready(function () {
            $.ajax({
                type: "post",
                url: remote_url + '/api/quote/insurence/details/get',
                data: {'_token': token, lead_id: lead_id},
                success: function (res) {
                    var content = '';
                    $.each(res[0], function (index, value) {
                        console.log(value);
                        var selected = '';
                        var selected_a = '';
                        var selected_class = 'primary';
                        var select_txt = 'Select This';
                        if (value.selected == 1) {
                            selected = 'border:4px solid #2bae22';
                            selected_a = 'background:#2bae22;color:#fff;';
                            selected_class = 'success';
                            select_txt = 'Selected'
                        }

                        var company = '<section id="section-' + value.quote_no + '" class="panel" style="border:1px solid #ccc;' + selected + '">' +
                            '                            <div class="panel-body">' +
                            '                            <div class="row">' +
                            '                            <div class="col-sm-3 col-md-3 col-lg-3">' +
                            '                            <img src="' + value.picture + '"' +
                            '                        style="width:110px;height: auto;object-fit: cover;display: inline;">' +
                            '                            </div>' +
                            '                            <div class="col-sm-5 col-md-5 col-lg-5">' +
                            '                            <div class="col-sm-12 col-md-12 col-lg-12 text-center"' +
                            '                        style="border-bottom:   1px solid #e1e1e1;padding-bottom: 2px;">' +
                            '                        <span style="width: auto;display: inline;font-weight: bold;font-size: 17px;color: #0c517d;">' + value.class_name + ' - #' + value.quote_no + '</span>' +
                            '                        </div>' +
                            '                        </div>' +
                            '                        <div class="col-sm-4 col-md-4 col-lg-4 text-center" style="position:relative;top:-10px;">' +
                            '                            <h5><strong>' +
                            '                            <small>Amount :</small>' +
                            '                        <br><span style="position:relative;top:3px;">Rs. ' + value.quote_value + '<span></span></span></strong>' +
                            '                        </h5>' +
                            '                        <h5><strong>' +
                            '                        <small>Discounted Amount :</small>' +
                            '                        <br><span style="position:relative;top:3px;">Rs. ' + value.discounted_value + '</span></strong></h5>' +
                            '                        </div>' +
                            '    <div style="text-align: center;margin-bottom: 10px;"><a href="#" onclick="navigate(' + value.quote_no + ')" ' +
                            '                        class="btn btn-' + selected_class + '">' + select_txt + '</a></div>' +
                            '                        </div>' +
                            '' +
                            '                        <div class="row is_required">' +
                            '                            <form action="#" class="form-horizontal">' +
                            '                            <input type="hidden" name="qu_amount" value="' + value.quote_value + '" disabled="">' +
                            '                            <input type="hidden" name="qu_disc" value="' + value.discounted_value + '" disabled="">' +
                            '                            <input type="hidden" name="quote_no" value="' + value.quote_no + '" disabled="">' +
                            '                            <input type="hidden" name="set_crm_id" value="12226" disabled="">' +
                            '                            <div class="col-sm-8 col-md-8 col-lg-8 col-xs-12">' +
                            '                            <div class="form-group">' +
                            '                            <label for="inputEmail3" class="col-xs-6 control-label">Amount</label>' +
                            '                            <div class="col-xs-6">' +
                            '                            <input type="email" class="form-control" id="amount_' + value.quote_no + '" placeholder="Amount">' +
                            '                            </div>' +
                            '                            </div>' +
                            '                            <div class="form-group">' +
                            '                            <label for="inputEmail3" class="col-xs-6 control-label">Discounted Amount</label>' +
                            '                        <div class="col-xs-6">' +
                            '                            <input type="email" class="form-control" id="discount_amount_' + value.quote_no + '"' +
                            '                        placeholder="Discounted Amount">' +
                            '                            </div>' +
                            '                            </div>' +
                            '                            </div>' +
                            '                            <div class="col-sm-2 col-md-2 col-lg-2 col-xs-12">' +
                            '                            <button type="button" class="btn-warning btn btn-block" onclick="update_amounts(' + value.quote_no + ')">Update</button>' +
                            '                            </div>' +
                            '                            </form>' +
                            '                            </div>' +
                            '                            </div>' +
                            '                            </section>';

                        $('#ins_companies').append(company);
                        content = content + company;
                    });
                    $('.content').val(content);
                }
            });
        });
    </script>
    <script>
        $("input[name='negotiation']").click(function () {
            var neg = $("input[name='negotiation']:checked").val();
            if (neg == 'required') {
                $('.is_required').addClass('show').removeClass('hide');
            } else if (neg == 'notrequired') {
                $('.is_required').addClass('hide').removeClass('show');

            }
        });

        function update_amounts(q) {
            var x = confirm('Are you sure to change amount?');
            if (x) {
                var amount = $('#amount_' + q).val();
                var discount_amount = $('#discount_amount_' + q).val();
                console.log(amount)
                console.log(discount_amount);
                var errors = "";
                if (amount == '') {
                    errors = errors + '<li>Amount can not empty.</li>';
                    $('#amount_' + q).css('border', '1px solid #dd4b39')
                } else {
                    $('#amount_' + q).css('border', '1px solid #d2d6de')
                }
                if (discount_amount == '') {
                    errors = errors + '<li>Discounted amount can not empty.</li>'
                    $('#discount_amount_' + q).css('border', '1px solid #dd4b39')
                } else {
                    $('#discount_amount_' + q).css('border', '1px solid #d2d6de')
                }
                if (discount_amount > amount) {
                    errors = errors + '<li>Discounted amount can not lager than amount.</li>'
                    $('#discount_amount_' + q).css('border', '1px solid #dd4b39')
                } else {
                    $('#discount_amount_' + q).css('border', '1px solid #d2d6de')
                }

                if (errors == '') {
                    $('#js-errors').hide();
                    $('#js-errors ul').html('');
                    $.ajax({
                        type: "post",
                        url: remote_url + '/api/quote/insurence/details/update',
                        data: {
                            '_token': token,
                            lead_id: lead_id,
                            quote_id: q,
                            amount: amount,
                            discount_amount: discount_amount
                        },
                        success: function (res) {
                            if (res) {
                                $.ajax({
                                    type: "post",
                                    url: '/customer/business/quotation/generated/update',
                                    data: {
                                        '_token': token,
                                        lead_id: lead_id,
                                        quote_id: q,
                                        amount: amount,
                                        discount_amount: discount_amount
                                    },
                                    success: function (res2) {
                                        if (res2) {
                                            notyalert('success', 'Quote value updated successfully!', 3000);
                                            setTimeout(function () {
                                                location.reload();
                                            }, 3000);
                                        } else {
                                            notyalert('error', 'Something went wrong!', 3000);
                                        }
                                    }
                                });
                            } else {
                                notyalert('error', 'Something went wrong!', 3000);
                            }
                        }
                    });
                } else {
                    $('#js-errors').show();
                    $('#js-errors ul').html(errors);
                }

            }

        }

        function navigate(id) {
//            alert(id);
            $('#section-' + id).css('border', '4px solid #2bae22');
            $('#section-' + id + ' a').css('background', '#2bae22').css('color', '#fff');
            setTimeout(function () {
                window.location = '/customer/business/quotation/insurence/view/' + id;
            }, 1000);
        }
    </script>
@endsection