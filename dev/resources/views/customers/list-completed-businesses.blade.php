@extends('layouts.dashboard-layout')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="text-center visible-xs">
            Completed Businesses
        </h1>
        <h1 class="hidden-xs">
            Completed Businesses
        </h1>
        <ol class="breadcrumb hidden-xs">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Completed Businesses</a></li>
            <!-- <li class="active">Blank page</li> -->
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="box-footer box-comments">
                    @if(count($customers))
                        @foreach($customers as $customer)
                            <div class="box-comment">
                                <!-- User image -->
                                <img class="img-circle img-sm" src="{{asset('')}}images/icons/car.png"
                                     alt="User Image">

                                <div class="comment-text">
                                    @if($customer->step == 1)
                                        <span class="label label-success pull-right">Select Business</span>
                                    @elseif($customer->step == 2)
                                        <span class="label label-success pull-right">Select Business Type</span>
                                    @elseif($customer->step == 3)
                                        <span class="label label-success pull-right">Enter Details</span>
                                    @elseif($customer->step == 4)
                                        <span class="label label-success pull-right">View Saved Business</span>
                                    @elseif($customer->step == 5)
                                        <span class="label label-success pull-right">Quote Generated</span>
                                    @endif
                                    <span class="username">{!!ucfirst($customer->user_fname).' '.ucfirst($customer->user_lname)!!}</span>
                                    <span class="username text-muted">{{ucfirst($customer->lead_make_text).' - '.ucfirst($customer->lead_model_text).' ('.$customer->lead_year.')'}}</span>
                                    <small class="text-muted pull-left">{{\Carbon\Carbon::parse($customer->created_at)->toFormattedDateString()}}</small>
                                    &nbsp;&nbsp;
                                </div>
                                <div class="comment-text">
                                    <a href="{{asset('')}}customer/business/{{$customer->id}}">
                                        <button class="btn btn-link btn-sm pull-right" style="margin: 0px 2px;">View <i
                                                    class="fa fa-arrow-circle-right"></i></button>
                                    </a>
                                    &nbsp;&nbsp;
                                    <a href="#coll_{{$customer->id}}" data-toggle="collapse">
                                        <button class="btn btn-sm pull-right btn-link" style="margin: 0px 2px;">
                                            Documents
                                            <i class="fa fa-plus-circle"></i></button>
                                    </a>
                                    <a href="#status_{{$customer->id}}" data-toggle="collapse">
                                        <button class="btn btn-link btn-sm pull-right" style="margin: 0px 2px;">Status
                                            <i
                                                    class="fa fa-arrow-circle-right"></i></button>
                                    </a>
                                </div>
                                <!-- /.comment-text -->
                            </div>
                            <div class="collapse" id="coll_{{$customer->id}}">
                                <div class="box box-success box-solid " style="margin-bottom:10px;">
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                        <div class="box-footer no-padding">
                                            <ul class="nav nav-stacked">
                                                <li>
                                                    <a href="#">VRC {!! ($customer->document AND $customer->document->vrc)?'<span class="pull-right badge bg-blue" data-toggle="modal" data-target="#vcr_modal_'.$customer->id.'">View</span>':'<span class="pull-right"><i class="fa fa-exclamation-circle text-danger" title="Not Uploaded" style="    font-size: 21px;"></i></span>' !!}</a>
                                                </li>
                                                <li>
                                                    <a href="#">NIC/License {!! ($customer->document AND $customer->document->nic)?'<span class="pull-right badge bg-blue" data-toggle="modal" data-target="#nic_modal_'.$customer->id.'">View</span>':'<span class="pull-right"><i class="fa fa-exclamation-circle text-danger" title="Not Uploaded" style="    font-size: 21px;"></i></span>' !!}</a>
                                                </li>
                                                <li><a href="#">Proposal
                                                        Form {!! ($customer->document AND $customer->document->proposal)?'<span class="pull-right badge bg-blue" data-toggle="modal" data-target="#proposal_modal_'.$customer->id.'">View</span>':'<span class="pull-right"><i class="fa fa-exclamation-circle text-danger" title="Not Uploaded" style="    font-size: 21px;"></i></span>' !!}</a>
                                                </li>
                                                <li><a href="#">Inspection
                                                        Form {!! ($customer->document AND $customer->document->inspection)?'<span class="pull-right badge bg-blue" data-toggle="modal" data-target="#inspection_modal_'.$customer->id.'">View</span>':'<span class="pull-right"><i class="fa fa-exclamation-circle text-danger" title="Not Uploaded" style="    font-size: 21px;"></i></span>' !!}</a>
                                                </li>
                                                <li>
                                                    <a href="#">LOA {!! ($customer->document AND $customer->document->loa)?'<span class="pull-right badge bg-blue" data-toggle="modal" data-target="#loa_modal_'.$customer->id.'">View</span>':'<span class="pull-right"><i class="fa fa-exclamation-circle text-danger" title="Not Uploaded" style="    font-size: 21px;"></i></span>' !!}</a>
                                                </li>
                                                <li><a href="#">Temporary Cover
                                                        Note {!! ($customer->document AND $customer->document->temp_cover_note)?'<span class="pull-right badge bg-blue" data-toggle="modal" data-target="#temp_cover_note_modal_'.$customer->id.'">View</span>':'<span class="pull-right"><i class="fa fa-exclamation-circle text-danger" title="Not Uploaded" style="    font-size: 21px;"></i></span>' !!}</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- /.box-body -->
                                    @if($customer->document)
                                        {{--models--}}
                                        @if($customer->document->temp_cover_note)
                                            <div id="vcr_modal_{{$customer->id}}" class="modal fade" role="dialog">
                                                <div class="modal-dialog">
                                                    <!-- Modal content-->
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close"
                                                                    data-dismiss="modal">&times;
                                                            </button>
                                                            <h4 class="modal-title">VRC
                                                                <small>{{ucfirst($customer->lead_make_text).' - '.ucfirst($customer->lead_model_text).' ('.$customer->lead_year.')'}}</small>
                                                            </h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <img src="{{asset('')}}{{ltrim($customer->document->temp_cover_note, './')}}"
                                                                 alt="" class="img-responsive">
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        @endif
                                        @if($customer->document->nic)
                                            <div id="nic_modal_{{$customer->id}}" class="modal fade" role="dialog">
                                                <div class="modal-dialog">
                                                    <!-- Modal content-->
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close"
                                                                    data-dismiss="modal">&times;
                                                            </button>
                                                            <h4 class="modal-title">NIC/License
                                                                <small>{{ucfirst($customer->lead_make_text).' - '.ucfirst($customer->lead_model_text).' ('.$customer->lead_year.')'}}</small>
                                                            </h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <img src="{{asset('')}}{{ltrim($customer->document->nic, './')}}"
                                                                 alt="" class="img-responsive">
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        @endif
                                        @if($customer->document->proposal)
                                            <div id="proposal_modal_{{$customer->id}}" class="modal fade" role="dialog">
                                                <div class="modal-dialog">
                                                    <!-- Modal content-->
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close"
                                                                    data-dismiss="modal">&times;
                                                            </button>
                                                            <h4 class="modal-title">Proposal Form
                                                                <small>{{ucfirst($customer->lead_make_text).' - '.ucfirst($customer->lead_model_text).' ('.$customer->lead_year.')'}}</small>
                                                            </h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <img src="{{asset('')}}{{ltrim($customer->document->proposal, './')}}"
                                                                 alt="" class="img-responsive">
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        @endif
                                        @if($customer->document->inspection)
                                            <div id="inspection_modal_{{$customer->id}}" class="modal fade"
                                                 role="dialog">
                                                <div class="modal-dialog">
                                                    <!-- Modal content-->
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close"
                                                                    data-dismiss="modal">&times;
                                                            </button>
                                                            <h4 class="modal-title">Inspection Form
                                                                <small>{{ucfirst($customer->lead_make_text).' - '.ucfirst($customer->lead_model_text).' ('.$customer->lead_year.')'}}</small>
                                                            </h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <img src="{{asset('')}}{{ltrim($customer->document->inspection, './')}}"
                                                                 alt="" class="img-responsive">
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        @endif
                                        @if($customer->document->loa)
                                            <div id="loa_modal_{{$customer->id}}" class="modal fade" role="dialog">
                                                <div class="modal-dialog">
                                                    <!-- Modal content-->
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close"
                                                                    data-dismiss="modal">&times;
                                                            </button>
                                                            <h4 class="modal-title">LOA
                                                                <small>{{ucfirst($customer->lead_make_text).' - '.ucfirst($customer->lead_model_text).' ('.$customer->lead_year.')'}}</small>
                                                            </h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <img src="{{asset('')}}{{ltrim($customer->document->loa, './')}}"
                                                                 alt="" class="img-responsive">
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        @endif
                                        @if($customer->document->temp_cover_note)
                                            <div id="temp_cover_note_modal_{{$customer->id}}" class="modal fade"
                                                 role="dialog">
                                                <div class="modal-dialog">
                                                    <!-- Modal content-->
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close"
                                                                    data-dismiss="modal">&times;
                                                            </button>
                                                            <h4 class="modal-title">Temporary Cover Note
                                                                <small>{{ucfirst($customer->lead_make_text).' - '.ucfirst($customer->lead_model_text).' ('.$customer->lead_year.')'}}</small>
                                                            </h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <img src="{{asset('')}}{{ltrim($customer->document->temp_cover_note, './')}}"
                                                                 alt="" class="img-responsive">
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        @endif
                                        {{--models end--}}
                                    @endif
                                </div>
                                <hr style="margin: 2px 0px">
                            </div>
                            <div class="collapse" id="status_{{$customer->id}}">
                                <div class="box box-warning box-solid " style="margin-bottom:10px;">
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                        <h5 class="text-center">Customer Status</h5>
                                        <div class="box-footer no-padding">
                                            <ul class="nav nav-stacked">
                                                <li>
                                                    <a href="#">Potential Customer
                                                        <div class="pull-right">
                                                            <button type="button"
                                                                    class="btn btn-sm btn-toggle {{$customer->is_potential ? 'active': ''}}"
                                                                    data-toggle="button" aria-pressed="true"
                                                                    onclick="chageStatus({{$customer->id}},'is_potential')"
                                                                    autocomplete="off">
                                                                <div class="handle"></div>
                                                            </button>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">Payment Done
                                                        <div class="pull-right">
                                                            <button type="button"
                                                                    class="btn btn-sm btn-toggle {{$customer->is_paid ? 'active': ''}}"
                                                                    data-toggle="button" aria-pressed="true"
                                                                    onclick="chageStatus({{$customer->id}},'is_paid')"
                                                                    autocomplete="off">
                                                                <div class="handle"></div>
                                                            </button>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">Documents
                                                        <div class="pull-right">
                                                            <button type="button"
                                                                    class="btn btn-sm btn-toggle {{$customer->documents ? 'active': ''}}"
                                                                    data-toggle="button" aria-pressed="true"
                                                                    onclick="chageStatus({{$customer->id}},'documents')"
                                                                    autocomplete="off">
                                                                <div class="handle"></div>
                                                            </button>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">Business Completed
                                                        <div class="pull-right">
                                                            <button type="button"
                                                                    class="btn btn-sm btn-toggle {{$customer->status == 2 ? 'active': ''}}"
                                                                    data-toggle="button" aria-pressed="true"
                                                                    onclick="chageStatus({{$customer->id}},'status')"
                                                                    autocomplete="off">
                                                                <div class="handle"></div>
                                                            </button>
                                                        </div>
                                                    </a>
                                                </li>

                                            </ul>
                                        </div>
                                    </div>
                                    <!-- /.box-body -->
                                    @if($customer->document)
                                        {{--models--}}
                                        @if($customer->document->temp_cover_note)
                                            <div id="vcr_modal_{{$customer->id}}" class="modal fade" role="dialog">
                                                <div class="modal-dialog">
                                                    <!-- Modal content-->
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close"
                                                                    data-dismiss="modal">&times;
                                                            </button>
                                                            <h4 class="modal-title">VRC
                                                                <small>{{ucfirst($customer->lead_make_text).' - '.ucfirst($customer->lead_model_text).' ('.$customer->lead_year.')'}}</small>
                                                            </h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <img src="{{asset('')}}{{ltrim($customer->document->temp_cover_note, './')}}"
                                                                 alt="" class="img-responsive">
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        @endif
                                        @if($customer->document->nic)
                                            <div id="nic_modal_{{$customer->id}}" class="modal fade" role="dialog">
                                                <div class="modal-dialog">
                                                    <!-- Modal content-->
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close"
                                                                    data-dismiss="modal">&times;
                                                            </button>
                                                            <h4 class="modal-title">NIC/License
                                                                <small>{{ucfirst($customer->lead_make_text).' - '.ucfirst($customer->lead_model_text).' ('.$customer->lead_year.')'}}</small>
                                                            </h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <img src="{{asset('')}}{{ltrim($customer->document->nic, './')}}"
                                                                 alt="" class="img-responsive">
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        @endif
                                        @if($customer->document->proposal)
                                            <div id="proposal_modal_{{$customer->id}}" class="modal fade" role="dialog">
                                                <div class="modal-dialog">
                                                    <!-- Modal content-->
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close"
                                                                    data-dismiss="modal">&times;
                                                            </button>
                                                            <h4 class="modal-title">Proposal Form
                                                                <small>{{ucfirst($customer->lead_make_text).' - '.ucfirst($customer->lead_model_text).' ('.$customer->lead_year.')'}}</small>
                                                            </h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <img src="{{asset('')}}{{ltrim($customer->document->proposal, './')}}"
                                                                 alt="" class="img-responsive">
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        @endif
                                        @if($customer->document->inspection)
                                            <div id="inspection_modal_{{$customer->id}}" class="modal fade"
                                                 role="dialog">
                                                <div class="modal-dialog">
                                                    <!-- Modal content-->
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close"
                                                                    data-dismiss="modal">&times;
                                                            </button>
                                                            <h4 class="modal-title">Inspection Form
                                                                <small>{{ucfirst($customer->lead_make_text).' - '.ucfirst($customer->lead_model_text).' ('.$customer->lead_year.')'}}</small>
                                                            </h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <img src="{{asset('')}}{{ltrim($customer->document->inspection, './')}}"
                                                                 alt="" class="img-responsive">
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        @endif
                                        @if($customer->document->loa)
                                            <div id="loa_modal_{{$customer->id}}" class="modal fade" role="dialog">
                                                <div class="modal-dialog">
                                                    <!-- Modal content-->
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close"
                                                                    data-dismiss="modal">&times;
                                                            </button>
                                                            <h4 class="modal-title">LOA
                                                                <small>{{ucfirst($customer->lead_make_text).' - '.ucfirst($customer->lead_model_text).' ('.$customer->lead_year.')'}}</small>
                                                            </h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <img src="{{asset('')}}{{ltrim($customer->document->loa, './')}}"
                                                                 alt="" class="img-responsive">
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        @endif
                                        @if($customer->document->temp_cover_note)
                                            <div id="temp_cover_note_modal_{{$customer->id}}" class="modal fade"
                                                 role="dialog">
                                                <div class="modal-dialog">
                                                    <!-- Modal content-->
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close"
                                                                    data-dismiss="modal">&times;
                                                            </button>
                                                            <h4 class="modal-title">Temporary Cover Note
                                                                <small>{{ucfirst($customer->lead_make_text).' - '.ucfirst($customer->lead_model_text).' ('.$customer->lead_year.')'}}</small>
                                                            </h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <img src="{{asset('')}}{{ltrim($customer->document->temp_cover_note, './')}}"
                                                                 alt="" class="img-responsive">
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        @endif
                                        {{--models end--}}
                                    @endif
                                </div>
                                <hr style="margin: 2px 0px">
                            </div>
                        @endforeach
                    @else
                        <div class="box-comment">
                            <!-- User image -->
                            <p class="text-center">No completed businesses.</p>
                        </div>
                @endif
                <!-- /.box-comment -->

                    <!-- /.box-comment -->
                </div>
            </div>

        </div>
        <!-- /.box -->

    </section>
@endsection

@section('extra-css')

@endsection


@section('extra-js')
    <script>
        $(document).ready(function (e) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        });
        function chageStatus(id, element) {
            $.ajax({
                type: 'POST',
                url: '{{route('customerStatusUpdate')}}',
                data: {id:id, element:element},
                success: function (data) {
                    if (data) {
                        notyalert('success', 'Status Updated Successfully', 3000);
                    }else {
                        notyalert('error', 'Status Not Updated', 3000);
                    }
                },
                error: function (data) {
                    console.log("error");
                    console.log(data);
                }
            });

        }
    </script>
@endsection