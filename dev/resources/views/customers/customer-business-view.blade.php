@extends('layouts.dashboard-layout')

@section('extra-css')
    <style>
        .select2-container--default .select2-selection--single {
            padding: 6px;
            height: 37px;
            width: 100%;
            font-size: 1.2em;
            position: relative;
        }

        .select2-container {
            width: 100% !important;
        }

        .select2-container--default .select2-selection--single {
            background-color: #fff;
            border: 1px solid #d2d6de !important;
            border-radius: 0px !important;
            height: 35px !important;
        }

        .deatails-div hr {
            margin-top: 8px !important;
            margin-bottom: 8px !important;
        }

        .vehi-no-txt {
            border-top: 0px;
            border-left: 0px;
            border-right: 0px;
            border-bottom: 2px solid #333;
        }

    </style>
@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="text-center visible-xs">
            Business Details View
        </h1>
        <h1 class="hidden-xs">
            Business Details View
        </h1>
        <ol class="breadcrumb hidden-xs">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Business Details View</a></li>
            <!-- <li class="active">Blank page</li> -->
        </ol>
    </section>

    <section class="content">

        <div class="row deatails-div">

            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="post text-right">
                            <button class="btn btn-sm btn-success" id="quote_gen_btn_2">
                                <i class="ion-ios-refresh-empty"></i>&nbsp;&nbsp;&nbsp;Generate Quotation
                            </button>
                            <a class="btn btn-sm btn-danger" href="#">Delete</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">User Details</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="post ">
                            <div class="user-block" style="    margin-bottom: 5px !important;">
                                <img class="img-circle img-bordered-sm"
                                     src="{{asset('')}}images/icons/user.png"
                                     alt="user image">
                                <span class="username" style="font-size: 18px !important; margin-top: 6px;">
                                  <a href="#">{{$business->user_fname}} {{$business->user_lname}}</a>
                                </span>
                            </div>

                        </div>
                    </div>
                    <div class="box-footer no-padding">
                        <ul class="nav nav-stacked">
                            <li><a href="#"><i class="fa fa-envelope  margin-r-5"></i> Email
                                    <span class="pull-right">{{$business->user_email}}</span>
                                </a></li>
                            <li><a href="#"><i class="fa fa-id-card margin-r-5"></i> NIC Number <span
                                            class="pull-right">{{$business->user_nic}}</span></a></li>
                            <li><a href="#"><i class="fa fa-mobile margin-r-5"></i> Mobile Number <span
                                            class="pull-right">{{$business->user_mobile}}</span></a></li>
                        </ul>
                    </div>
                    <!-- /.box-body -->
                </div>

            </div>
            <div class="col-md-12">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#vehicale_info" data-toggle="tab" aria-expanded="true">
                                <i class="fa fa-car margin-r-5"></i>Vehicle
                                Info</a></li>
                        <li class=""><a href="#edit_info" data-toggle="tab" aria-expanded="false">
                                <i class="fa fa-edit margin-r-5"></i>Edit Info</a>
                        </li>
                        <li class=""><a href="#renewal_date" data-toggle="tab" aria-expanded="false">
                                <i class="fa fa-calendar margin-r-5"></i>Renewal Date</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="vehicale_info">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 col-xs-12 control-label">Vehicle Number</label>
                                @if(is_null($business->vehicle_reg_no))
                                    <div class="col-sm-5 col-xs-7">
                                        <input type="email" class="form-control vehi-no-txt" id="vehi_no_text"
                                               placeholder="WPCBB1234" oninput="this.value = this.value.toUpperCase()">
                                    </div>
                                    <div class="col-sm-4 col-xs-5">

                                        <button type="button" class="btn btn-default " id="vehi_no_save_btn"><i
                                                    class="fa fa-check  margin-r-5"></i> Save
                                        </button>

                                    </div>
                                @else
                                    <div class="col-sm-5 col-xs-7">
                                        <span class="badge bg-green-gradient"
                                              style="border-radius: 2px;font-size: 28px;">{{$business->vehicle_reg_no}}</span>
                                    </div>
                                    <div class="col-sm-4 col-xs-5">

                                        <button type="button" class="btn btn-default " id="vehi_no_edit_btn"><i
                                                    class="fa fa-pencil  margin-r-5"></i> edit
                                        </button>

                                    </div>
                                @endif
                            </div>
                            <div class="clearfix"></div>
                            {{--<hr>--}}
                            <div class="box-footer no-padding" style="margin-top: 15px;">
                                <ul class="nav nav-stacked">
                                    <li><a href="#"><i class="fa fa-circle margin-r-5"></i> Make
                                            <span class="pull-right">{{$business->lead_make_text}}</span></a></li>
                                    <li><a href="#"><i class="fa fa-circle margin-r-5"></i> Model
                                            <span class="pull-right">{{$business->lead_model_text}}</span></a></li>
                                    <li><a href="#"><i class="fa fa-circle margin-r-5"></i> Vehicle Value
                                            <span class="pull-right">LKR {{$business->lead_value}}</span></a></li>
                                    <li><a href="#"><i class="fa fa-circle margin-r-5"></i> Purpose
                                            <span class="pull-right">{{$business->purpose_text}}</span></a></li>
                                    <li><a href="#"><i class="fa fa-circle margin-r-5"></i> Lease/Hire Purchase
                                            <span class="pull-right">
                                                {!!($business->leased)? '<small class="label pull-right bg-green">Yes</small>':'<small class="label pull-right bg-yellow">No</small>'!!}
                                            </span></a></li>
                                    <li><a href="#"><i class="fa fa-circle margin-r-5"></i> Agent Repair
                                            <span class="pull-right">
                                                {!!($business->agent_repair)? '<small class="label pull-right bg-green">Yes</small>':'<small class="label pull-right bg-yellow">No</small>'!!}
                                            </span></a></li>
                                    <li><a href="#"><i class="fa fa-circle margin-r-5"></i> Year
                                            <span class="pull-right">{{$business->lead_year}}</span></a></li>
                                    <li><a href="#"><i class="fa fa-circle margin-r-5"></i> Fuel Type
                                            <span class="pull-right">{{$business->lead_fuel_text}}</span></a></li>
                                    <li><a href="#"><i class="fa fa-circle margin-r-5"></i> No Claim Bonus
                                            <span class="pull-right">{{is_null($business->lead_no_claim_bonus)?0:$business->lead_no_claim_bonus}}
                                                %</span></a></li>

                                </ul>
                            </div>
                        </div>

                        <div class="tab-pane" id="edit_info">
                            <div class="register-box">
                                <div class="register-box-body">
                                    <form id="generate_quote_form">
                                        {{csrf_field()}}
                                        <input type="hidden" name="lead_id" id="lead_id"
                                               value="{{$business->main_lead_id}}">
                                        <div class="form-group has-feedback">
                                            <select class="form-control" id="purpose" name="purpose">
                                                <option value="" selected="selected">Select Purpose</option>
                                            </select>
                                        </div>
                                        <div class="form-group has-feedback">
                                            <select class="form-control" id="lead_make" name="lead_make">
                                                <option value="">Make</option>
                                            </select>
                                        </div>
                                        <div class="form-group has-feedback">
                                            <select class="form-control" id="lead_model" name="lead_model">
                                                <option value="">Model</option>
                                            </select>
                                        </div>
                                        <div class="form-group has-feedback">
                                            <select class="form-control" id="lead_year" name="lead_year">
                                                <option value="">Model Year</option>
                                                @php $x = (int)\Carbon\Carbon::now()->format('Y'); @endphp
                                                @for($x;$x>=1990;$x--)
                                                    <option value="{{$x}}">{{$x}}</option>
                                                @endfor
                                            </select>
                                        </div>
                                        <div class="form-group has-feedback">
                                            <select class="form-control" id="lead_fueltype" name="lead_fueltype">
                                                <option value="">Fuel Type</option>
                                            </select>
                                        </div>
                                        <div class="input-group form-group">
                                            <span class="input-group-addon">LKR</span>
                                            <input type="text" class="form-control" placeholder="Value" name="vl_value"
                                                   id="vl_value" value="{{$business->lead_value}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="" class="col-xs-4 no-padding">Agent Repair</label>
                                            <label for="" class="col-xs-4">
                                                <input type="radio" name="agent_repair"
                                                       value="1" {{($business->agent_repair)?'checked="checked"':''}}>
                                                Yes
                                            </label>
                                            <label for="" class="col-xs-4">
                                                <input type="radio" name="agent_repair"
                                                       value="0" {{!($business->agent_repair)?'checked="checked"':''}}>
                                                No
                                            </label>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="input-group form-group">
                                            <input type="text" class="form-control" placeholder="No Claim Bonus %"
                                                   name="lead_no_claim_bonus" id="lead_no_claim_bonus"
                                                   value="{{$business->lead_no_claim_bonus}}">
                                            <span class="input-group-addon">%</span>
                                        </div>
                                        <div class="form-group">
                                            <label for="" class="col-xs-4 no-padding">Lease</label>
                                            <label for="" class="col-xs-4">
                                                <input type="radio" name="leased" id=""
                                                       value="1" {{($business->leased)?'checked="checked"':''}}>
                                                Yes
                                            </label>
                                            <label for="" class="col-xs-4">
                                                <input type="radio" name="leased" id=""
                                                       value="0" {{!($business->leased)?'checked="checked"':''}}>
                                                No
                                            </label>
                                            <div class="clearfix"></div>
                                        </div>

                                        <div class="row">
                                            <div class="col-xs-6 col-md-6 col-lg-6">

                                            </div>
                                            <!-- /.col -->
                                            <div class="col-xs-12 col-md-6 col-lg-6">
                                                <button type="button" class="btn btn-warning btn-block"
                                                        id="generate_quote_submit">Update
                                                </button>
                                            </div>
                                            <!-- /.col -->
                                        </div>
                                        <!-- <div class="row">
                                          <div class="col-xs-12">
                                            <p class="mt-2 text-center">By click register, I agree to the <a href="#">Tearms and Conditions</a></p>
                                          </div>
                                        </div> -->
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="renewal_date">
                            <div class="register-box">
                                <div class="register-box-body">
                                    <form id="generate_quote_form">
                                        <div class="form-group">
                                            <label>Renewal Date :</label>

                                            <div class="input-group date">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" class="form-control pull-right" id="datepicker">
                                            </div>
                                            <!-- /.input group -->
                                        </div>

                                        <div class="row">
                                            <div class="col-xs-6 col-md-6 col-lg-6">

                                            </div>
                                            <!-- /.col -->
                                            <div class="col-xs-12 col-md-6 col-lg-6">
                                                <button type="button" class="btn btn-warning btn-block"
                                                        id="generate_quote_submit">Update
                                                </button>
                                            </div>
                                            <!-- /.col -->
                                        </div>
                                        <!-- <div class="row">
                                          <div class="col-xs-12">
                                            <p class="mt-2 text-center">By click register, I agree to the <a href="#">Tearms and Conditions</a></p>
                                          </div>
                                        </div> -->
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
                <!-- /.nav-tabs-custom -->
            </div>

            <div class="col-md-12">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Action</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <a href="#" class="btn btn-success btn-block" style="margin:5px;" id="quote_gen_btn">
                            <i class="ion-refresh"></i>&nbsp;&nbsp;&nbsp;<b>Generate Quotation</b>
                        </a>
                        {{--<a href="/lead/screen/status?id=4581&amp;status=4" class="btn btn-default" style="margin:5px;">--}}
                        {{--<img src="{{asset('')}}images/icons/trash.png" width="50"> Dead--}}
                        {{--Lead--}}
                        {{--</a>--}}
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

    </section>
    <br>

@endsection

@section('extra-js')
    <script>
        var remote_url = "{{env('remote_url')}}";
        var token = $('meta[name="csrf-token"]').attr('content');
        var lead_id = {{$business->main_lead_id}};
    </script>
    <script>
        function setDefaultValueSelect(elementID, optionText, optionID) {
            var option = new Option(optionText, optionID, true, true);
            $(elementID).append(option).trigger('change');

            // manually trigger the `select2:select` event
            $(elementID).trigger({
                type: 'select2:select',
            });
        }

        setDefaultValueSelect('#purpose', '{{$business->purpose_text}}', '{{$business->purpose}}');
        setDefaultValueSelect('#lead_make', '{{$business->lead_make_text}}', '{{$business->lead_make_id}}');
        setDefaultValueSelect('#lead_model', '{{$business->lead_model_text}}', '{{$business->lead_model_id}}');
        setDefaultValueSelect('#lead_fueltype', '{{$business->lead_fuel_text}}', '{{$business->lead_fuel_id}}');
        setDefaultValueSelect('#lead_year', '{{$business->lead_year}}', '{{$business->lead_year}}');

        $('#purpose').select2({
            ajax: {
                url: remote_url + "/api/purposes/all",
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                cache: true
            },
            minimumResultsForSearch: -1
        });
        $('#lead_make').select2({
            ajax: {
                url: remote_url + "/api/vehicle/make/all",
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                cache: true
            }
        });
        $('#lead_make').on('change', function () {
            var data = $("#lead_make option:selected").val();
            var url = remote_url + "/api/vehicle/model/" + data;
            setDefaultValueSelect('#lead_model', 'Select Model', '');
            $('#lead_model').select2({
                ajax: {
                    url: url,
                    type: "get",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            searchTerm: params.term // search term
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: response
                        };
                    },
                    cache: true
                }
            });
        });
        $('#lead_model').select2();
        $('#lead_fueltype').select2({
            ajax: {
                url: remote_url + "/api/fuel/all",
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                cache: true
            },
            minimumResultsForSearch: -1
        });
        $('#lead_year').select2();
    </script>

    <script>

        $('#vehi_no_save_btn').click(function (e) {
            blockUi('processing..');
            var vehi_no = $('#vehi_no_text').val();
            if (vehi_no != '') {
                $.ajax({
                    type: "post",
                    url: remote_url + '/api/crm/vehicle/number/save',
                    data: {'_token': token, lead_id: lead_id, vehi_no: vehi_no},
                    success: function (res) {
                        if (res.status) {
                            $.ajax({
                                type: "post",
                                url: '/customer/business/vehicle-number/save',
                                data: {'_token': token, lead_id: lead_id, vehi_no: vehi_no},
                                success: function (res2) {
                                    $.unblockUI();
                                    if (res) {
                                        notyalert('success', 'Vehicle Number Saved Successfully!', 3000);
                                        setTimeout(function () {
                                            location.reload();
                                        }, 3000);
                                    } else {
                                        notyalert('error', 'Something went wrong. Please try again!', 2000);
                                    }
                                }
                            });
                        }
                    }
                });
            } else {
                notyalert('error', 'Enter Vehicle Number!', 2000);

            }
        });

        $('#vehi_no_edit_btn').click(function (e) {
            blockUi('processing..');
            $.ajax({
                type: "post",
                url: remote_url + '/api/crm/vehicle/number/delete',
                data: {'_token': token, lead_id: lead_id},
                success: function (res) {
                    if (res.status) {
                        $.ajax({
                            type: "post",
                            url: '/customer/business/vehicle-number/delete',
                            data: {'_token': token, lead_id: lead_id},
                            success: function (res2) {
                                $.unblockUI();
                                if (res) {
                                    notyalert('info', 'Re-enter Vehicle Number!', 2000);
                                    setTimeout(function () {
                                        location.reload();
                                    }, 2000);
                                } else {
                                    notyalert('error', 'Something went wrong. Please try again!', 2000);
                                }
                            }
                        });
                    }
                }
            });
        });
    </script>

    <script>
        $('#generate_quote_submit').click(function (e) {
            blockUi('processing..');
            var token = $('meta[name="csrf-token"]').attr('content');
            var purpose = $('#purpose').val();
            var purpose_text = $('#purpose option:selected').text();
            var lead_make = $('#lead_make').val();
            var lead_make_text = $('#lead_make option:selected').text();
            var lead_model = $('#lead_model').val();
            var lead_model_text = $('#lead_model option:selected').text();
            var lead_year = $('#lead_year').val();
            var lead_fueltype = $('#lead_fueltype').val();
            var lead_fueltype_text = $('#lead_fueltype option:selected').text();
            var vl_value = $('#vl_value').val();
            var lead_no_claim_bonus = $('#lead_no_claim_bonus').val();
            var agent_repair = $("input[name='agent_repair']:checked").val();
            var leased = $("input[name='leased']:checked").val();
            var lead_id = $("#lead_id").val();

            var form_data = {
                'purpose': purpose,
                'purpose_text': purpose_text,
                'lead_make': lead_make,
                'lead_make_text': lead_make_text,
                'lead_model': lead_model,
                'lead_model_text': lead_model_text,
                'lead_year': lead_year,
                'lead_fueltype': lead_fueltype,
                'lead_fueltype_text': lead_fueltype_text,
                'vl_value': vl_value,
                'lead_no_claim_bonus': lead_no_claim_bonus,
                'agent_repair': agent_repair,
                'leased': leased
            };
            $.ajax({
                type: "post",
                url: remote_url + '/api/crm/vehicle/update',
                data: {'_token': token, form_data: form_data, lead_id: lead_id},
                success: function (res1) {
                    console.log('remote db add')
                    console.log(res1)
                    if (res1.status) {
                        $.ajax({
                            type: "post",
                            url: '/customer/business/motor/update',
                            data: {'_token': token, form_data: form_data, lead_id: lead_id},
                            success: function (res2) {
                                $.unblockUI();
                                if (res2.status) {
                                    notyalert('success', 'Vehicle details updated successfully!', 3000);
                                    setTimeout(function () {
                                        location.reload();
                                    }, 3000);
                                }
                            }
                        });
                    }
                }
            });
        });
    </script>

    <script>
        $('#datepicker').datepicker({
            autoclose: true
        });
        $('#quote_gen_btn, #quote_gen_btn_2').click(function (e) {
            $.ajax({
                type: "post",
                url: remote_url + '/api/quote/generate/' + lead_id,
                data: {'_token': token, lead_id: lead_id},
                success: function (res) {
                    if (res.status) {
                        console.log(res.qoutes);
                        $.ajax({
                            type:"post",
                            url: '/customer/business/quotation/generated/save',
                            data: {'_token': token, qoutes: res.qoutes, lead_id: lead_id},
                            success: function (res) {

                            }
                        });
                        notyalert('success', 'Quote Generated successfully!', 2000);
                        setTimeout(function () {
                            window.location = '/customer/business/quotation/generated/view/'+lead_id;
                        },2000);
                    } else {
                        notyalert('error', res.msg, 2000);
                    }
                }
            });
        });

    </script>
@endsection