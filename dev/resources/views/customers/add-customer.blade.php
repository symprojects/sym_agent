@extends('layouts.dashboard-layout')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="text-center visible-xs">
            Register New Customer
        </h1>
        <h1 class="hidden-xs">
            Register New Customer
        </h1>
        <ol class="breadcrumb hidden-xs">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Register New Customer</a></li>
            <!-- <li class="active">Blank page</li> -->
        </ol>
    </section>

    <!-- Main content -->
    <section class="">
        <div class="register-box">
            @if(session()->has('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
            @endif
            @if(session()->has('error'))
                <div class="alert alert-danger">
                    {{ session()->get('error') }}
                </div>
            @endif
            <div class="register-box-body">
                <form action="{{route('save_customer')}}" method="post" id="add_customer_form">
                    {{csrf_field()  }}
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" placeholder="First Name" name="first_name"
                               id="first_name" required>
                        <span class="ion-android-person form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" placeholder="Last Name" name="last_name" id="last_name"
                               required>
                        <span class="ion-android-person form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" placeholder="NIC Number" name="nic" id="nic" required>
                        <span class="ion-card form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" placeholder="Mobile Number" name="mobile" id="mobile"
                               required>
                        <span class="ion-iphone form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="email" class="form-control" placeholder="Email" name="email" id="email" required>
                        <span class="ion-email form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" placeholder="Address" name="address" id="address"
                               required>
                        <span class="ion-navicon form-control-feedback"></span>
                    </div>
                    <div class="row">
                        <div class="col-xs-6 col-md-6 col-lg-6">

                        </div>
                        <!-- /.col -->
                        <div class="col-xs-6 col-md-6 col-lg-6">
                            <button type="submit" class="btn btn-warning btn-block">Register</button>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- <div class="row">
                      <div class="col-xs-12">
                        <p class="mt-2 text-center">By click register, I agree to the <a href="#">Tearms and Conditions</a></p>
                      </div>
                    </div> -->
                </form>
            </div>
        </div>
    </section>
    <br>
    <!-- /.content -->
@endsection

@section('extra-css')

@endsection


@section('extra-js')
    <script>
        validator = $('#add_customer_form').validate({ // initialize plugin
            // your rules & options,
            ignore: [],
            rules: {
                'first_name': {
                    required: true,
                },
                'last_name': {
                    required: true,
                },
                'nic': {
                    required: true,
                    maxlength: 12,
                    minlength: 10
                },
                'mobile': {
                    required: true,
                    number: true,
                    maxlength: 10,
                    minlength: 10

                },
                'email': {
                    required: true,
                    email: true,
                    minlength: 3,
                },
                'address': {
                    required: true,
                    maxlength: 100,
                    minlength: 10
                }

            },
            messages: {
                first_name: "Please enter first name",
                last_name: "Please enter last name",
                nic: {
                    required: "Please enter NIC number"
                },
                mobile: {
                    required: "Please enter mobile number"
                },
                address: {
                    required: "Please enter address"
                },
                email: {
                    required: "Please enter email",
                }
            },
            highlight: function (label) {
                $(label).closest('.form-group').addClass('has-error');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents(".form-group").removeClass("has-error");
            },
            success: function (label) {
                $(label).closest('.form-group').removeClass('has-error');
                label.remove();
            },
            errorPlacement: function (error, element) {
                var placement = element.closest('.form-group .form-control-feedback');
                if (!placement.get(0)) {
                    placement = element;
                }
                if (error.text() !== '') {
                    placement.after(error);
                }
            },
            submitHandler: function (form) {
                $.blockUI({message: '<h1><img src="busy.gif" /> Just a moment...</h1>'});
                form.submit();
                setTimeout(function () {


                }, 1000);
            }
        });
    </script>
@endsection