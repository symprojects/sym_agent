@extends('layouts.dashboard-layout')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="text-center visible-xs">
            Quotations
        </h1>
        <h1 class="hidden-xs">
            Quotations
        </h1>
        <ol class="breadcrumb hidden-xs">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Quotations</a></li>
            <!-- <li class="active">Blank page</li> -->
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <form action="">
                    <div class="form-group">
                        <label for="" class="col-xs-12 col-md-4 no-padding">Price Negotiation</label>
                        <label for="" class="col-xs-6 col-md-4">
                            <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked="">
                            Required
                        </label>
                        <label for="" class="col-xs-6 col-md-4">
                            <input type="radio" name="optionsRadios" id="optionsRadios2" value="option1" checked="">
                            Not Required
                        </label>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group has-feedback">
                        <div class="row">
                            <div class="col-xs-6">
                                <input type="text" class="form-control" placeholder="Change Value">
                            </div>
                            <div class="col-xs-6">
                                <button class="btn btn-warning btn-block">Regenerate</button>
                            </div>
                        </div>
                    </div>
                </form>
                <hr>
                <section class="panel" style="border:1px solid #ccc;">
                    <a href="/lead/set/selected?quote_no=56341"
                       style="padding: 6px 18px;position: absolute;border: 1px solid #ccc;right: 11px;background: none;border-radius: 0px !important;color: darkgreen;border-top-right-radius: 2px !important;border-bottom-left-radius: 30px !important;z-index: 1000;border-top: 0px;border-right: 0px;"
                       class="btn btn-primary"><i class="fa fa-check" aria-hidden="true"></i></a>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-3 col-md-3 col-lg-3">
                                <img src="https://bckend.saveyourmonkey.com/uploads/ins/hj6Ca9Efzm6DSgbJ_16.png"
                                     style="width:110px;height: auto;object-fit: cover;display: inline;">
                            </div>
                            <div class="col-sm-5 col-md-5 col-lg-5">
                                <div class="col-sm-12 col-md-12 col-lg-12 text-center"
                                     style="border-bottom:   1px solid #e1e1e1;padding-bottom: 2px;">
                                    <a href="#" class="btn btn-primary"
                                       style="position: absolute;left: -9px;top: -5px;background: none;color: darkgreen;border: 0;"
                                       onclick="openSendSingleQuote(56341)"><i class="fa fa-paper-plane"
                                                                               aria-hidden="true"></i></a>
                                    <div id="quote_resend_modal_single_56341" class="modal fade quote_container"
                                         role="dialog" style="margin-left: 10px; margin-right: 10px;">
                                        <div class="modal-dialog modal-dialog-pmt">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Send the Orient Quotation</h4>
                                                </div>
                                                <div class="modal-body row" align="center"
                                                     style="margin-top: 10px;">
                                                    <form action="/lead/send/single/quote" method="post">
                                                        <div class="col-sm-12 col-md-12 col-lg-12">
                                                            <div class="form-group login-group">
                                                                <input type="text" class="form-control login-field"
                                                                       style="text-align:center;" name="lead_email"
                                                                       placeholder="Email Address"
                                                                       value="mohan@mohanperera.com" disabled="">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12 col-md-12 col-lg-12">
                                                            <div class="form-group login-group">
                                                                <br>
                                                                <input type="hidden" name="lead_id" value="77128"
                                                                       disabled="">
                                                                <input type="hidden" name="quote_no" value="56341"
                                                                       disabled="">
                                                                <button class="btn btn-primary" type="submit"
                                                                        disabled="">Send Quotation
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <span style="width: auto;display: inline;font-weight: bold;font-size: 17px;color: #0c517d;">Orient - #56341</span>
                                </div>
                                <div class="col-sm-12 col-md-12 col-lg-12" style="padding: 7px 0px;">
                                    <center>
                                        &nbsp;&nbsp;<a target="_blank"
                                                       href="/formula/log?ins_id=16&amp;lead_id=77128">View Formula
                                            Log</a>
                                    </center>
                                </div>

                            </div>
                            <div class="col-sm-4 col-md-4 col-lg-4 text-center" style="position:relative;top:-10px;">
                                <h5><strong>
                                        <small>Amount :</small>
                                        <br><span style="position:relative;top:3px;">Rs. 100,247.16<span></span></span></strong>
                                </h5>
                                <h5><strong>
                                        <small>Discounted Amount :</small>
                                        <br><span style="position:relative;top:3px;">Rs. 98,297.16</span></strong></h5>
                            </div>
                        </div>

                        <div class="row">
                            <form action="#" class="form-horizontal">
                                <div class="col-sm-8 col-md-8 col-lg-8 col-xs-12">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-xs-6 control-label">Amount</label>
                                        <div class="col-xs-6">
                                            <input type="email" class="form-control" id="" placeholder="Amount">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-xs-6 control-label">Discounted
                                            Amount</label>
                                        <div class="col-xs-6">
                                            <input type="email" class="form-control" id="inputEmail3"
                                                   placeholder="Discounted Amount">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-2 col-md-2 col-lg-2 col-xs-12">
                                    <button type="submit" class="btn-warning btn btn-block">Update</button>
                                </div>
                            </form>
                        </div>
                        <div class="row price_neg" style=" display:none;">

                            <form class="form-horizontal" method="post"
                                  action="/crm/price/negotiation?lead_id=77128">
                                <input type="hidden" name="qu_amount" value="100247.16" disabled="">
                                <input type="hidden" name="qu_disc" value="98297.16" disabled="">
                                <div class="col-sm-8 col-md-8 col-lg-8">
                                    <div class="form-group" style="margin-bottom: 4px;">
                                        <label class="col-sm-5 col-md-5 col-lg-5 control-label"
                                               for="quote_value_56341">Amount</label>
                                        <div class="col-sm-7 col-md-7 col-lg-7">
                                            <input name="quote_value" id="quote_value_56341" type="text"
                                                   class="form-control input-sm" value="100247.16"
                                                   required="required" disabled="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-5 col-md-5 col-lg-5 control-label"
                                               for="discounted_value_56341">Discounted Amount</label>
                                        <div class="col-sm-7 col-md-7 col-lg-7">
                                            <input name="discounted_value" id="discounted_value_56341" type="text"
                                                   class="form-control input-sm" value="98297.16"
                                                   required="required" disabled="">
                                        </div>
                                    </div>
                                    <input type="hidden" name="quote_no" value="56341" disabled="">
                                    <input type="hidden" name="set_crm_id" value="12226" disabled="">
                                </div>
                                <div class="col-sm-4 col-md-4 col-lg-4">
                                    <button type="submit" class="btn btn-danger" disabled=""><i
                                                class="fa fa-pencil-square-o" aria-hidden="true"></i> Update
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>

                <section class="panel" style="border:1px solid #ccc;">
                    <a href="/lead/set/selected?quote_no=56341"
                       style="padding: 6px 18px;position: absolute;border: 1px solid #ccc;right: 11px;background: none;border-radius: 0px !important;color: darkgreen;border-top-right-radius: 2px !important;border-bottom-left-radius: 30px !important;z-index: 1000;border-top: 0px;border-right: 0px;"
                       class="btn btn-primary"><i class="fa fa-check" aria-hidden="true"></i></a>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-3 col-md-3 col-lg-3">
                                <img src="https://bckend.saveyourmonkey.com/uploads/ins/hj6Ca9Efzm6DSgbJ_16.png"
                                     style="width:110px;height: auto;object-fit: cover;display: inline;">
                            </div>
                            <div class="col-sm-5 col-md-5 col-lg-5">
                                <div class="col-sm-12 col-md-12 col-lg-12 text-center"
                                     style="border-bottom:   1px solid #e1e1e1;padding-bottom: 2px;">
                                    <a href="#" class="btn btn-primary"
                                       style="position: absolute;left: -9px;top: -5px;background: none;color: darkgreen;border: 0;"
                                       onclick="openSendSingleQuote(56341)"><i class="fa fa-paper-plane"
                                                                               aria-hidden="true"></i></a>
                                    <div id="quote_resend_modal_single_5634" class="modal fade quote_container"
                                         role="dialog" style="margin-left: 10px; margin-right: 10px;">
                                        <div class="modal-dialog modal-dialog-pmt">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Send the Orient Quotation</h4>
                                                </div>
                                                <div class="modal-body row" align="center"
                                                     style="margin-top: 10px;">
                                                    <form action="/lead/send/single/quote" method="post">
                                                        <div class="col-sm-12 col-md-12 col-lg-12">
                                                            <div class="form-group login-group">
                                                                <input type="text" class="form-control login-field"
                                                                       style="text-align:center;" name="lead_email"
                                                                       placeholder="Email Address"
                                                                       value="mohan@mohanperera.com" disabled="">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12 col-md-12 col-lg-12">
                                                            <div class="form-group login-group">
                                                                <br>
                                                                <input type="hidden" name="lead_id" value="77128"
                                                                       disabled="">
                                                                <input type="hidden" name="quote_no" value="56341"
                                                                       disabled="">
                                                                <button class="btn btn-primary" type="submit"
                                                                        disabled="">Send Quotation
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <span style="width: auto;display: inline;font-weight: bold;font-size: 17px;color: #0c517d;">Orient - #56341</span>
                                </div>
                                <div class="col-sm-12 col-md-12 col-lg-12" style="padding: 7px 0px;">
                                    <center>
                                        &nbsp;&nbsp;<a target="_blank"
                                                       href="/formula/log?ins_id=16&amp;lead_id=77128">View Formula
                                            Log</a>
                                    </center>
                                </div>
                            </div>
                            <div class="col-sm-4 col-md-4 col-lg-4" style="position:relative;top:-10px;">
                                <h5><strong>
                                        <small>Amount :</small>
                                        <br><span style="position:relative;top:3px;">Rs. 100,247.16<span></span></span></strong>
                                </h5>
                                <h5><strong>
                                        <small>Discounted Amount :</small>
                                        <br><span style="position:relative;top:3px;">Rs. 98,297.16</span></strong></h5>
                            </div>
                        </div>
                        <div class="row price_neg" style=" display:none;">

                            <form class="form-horizontal" method="post"
                                  action="/crm/price/negotiation?lead_id=77128">
                                <input type="hidden" name="qu_amount" value="100247.16" disabled="">
                                <input type="hidden" name="qu_disc" value="98297.16" disabled="">
                                <div class="col-sm-8 col-md-8 col-lg-8">
                                    <div class="form-group" style="margin-bottom: 4px;">
                                        <label class="col-sm-5 col-md-5 col-lg-5 control-label"
                                               for="quote_value_56341">Amount</label>
                                        <div class="col-sm-7 col-md-7 col-lg-7">
                                            <input name="quote_value" id="quote_value_563" type="text"
                                                   class="form-control input-sm" value="100247.16"
                                                   required="required" disabled="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-5 col-md-5 col-lg-5 control-label"
                                               for="discounted_value_56341">Discounted Amount</label>
                                        <div class="col-sm-7 col-md-7 col-lg-7">
                                            <input name="discounted_value" id="discounted_value_563zz" type="text"
                                                   class="form-control input-sm" value="98297.16"
                                                   required="required" disabled="">
                                        </div>
                                    </div>
                                    <input type="hidden" name="quote_no" value="56341" disabled="">
                                    <input type="hidden" name="set_crm_id" value="12226" disabled="">
                                </div>
                                <div class="col-sm-4 col-md-4 col-lg-4">
                                    <button type="submit" class="btn btn-danger" disabled=""><i
                                                class="fa fa-pencil-square-o" aria-hidden="true"></i> Update
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
            </div>

        </div>
        <!-- /.box -->

    </section>

    <div class="footer-tabs visible-xs">
        <div class="navbar">
            <div class="" style="margin-top: 5px;margin-bottom: 5px">
                <div class="col-xs-4" style="margin-top: 5px">
                    <label for=""><small>Payment Link</small></label>
                </div>
                <div class="col-xs-8">
                    <div class="input-group input-group-sm">
                        <input type="text" class="form-control" placeholder="Payment Link">
                        <span class="input-group-btn">
                      <button type="button" class="btn btn-warning">Send</button>
                    </span>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="row">
                <div class="col-xs-4 pr-2 ">
                    <button class="btn btn-block btn-primary">Send Quote</button>
                </div>
                <div class="col-xs-5 padding-2">
                    <button class="btn btn-block btn-primary">Send Quote To...</button>
                </div>
                <div class="col-xs-3 pl-2 ">
                    <button class="btn btn-block btn-danger"><i class="ion-android-arrow-back"></i> Back</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('extra-css')
    <style>

        .footer-tabs .navbar {
            overflow: hidden;
            background-color: rgba(255, 255, 255, 1);
            position: fixed;
            bottom: -1px;
            width: 100%;
            margin: 0px !important;
            border-radius: 6px 6px 0px 0px;
            min-height: 76px !important;


        }

        .footer-tabs .add-business {
            padding: 9px 15px !important;
            border-radius: 100% !important;
            margin-top: 7px;
            margin-right: 2px;
            margin-left: 2px;
            border: 2px solid #09708a !important;
            color: #fff !important;
            background: #09708a !important;
        }

        .footer-tabs .add-user {
            padding: 9px 14px !important;
            border-radius: 100% !important;
            margin-top: 7px;
            margin-right: 2px;
            margin-left: 2px;
            border: 2px solid #09708a !important;
            color: #09708a !important;
            background: #fff !important;
        }

        .footer-tabs .hot-leads {

        }

        .footer-tabs .navbar a {
            float: left;
            display: block;
            color: #fff;
            text-align: center;
            padding: 10px 8px;
            font-weight: 600;
            text-decoration: none;
            font-size: 17px;
            border-radius: 10px 10px 0px 0px;
            background: #09708a;
            border: 1px solid #09708a;
            margin-top: 3px;
        }

        .footer-tabs .navbar a:hover {
            background: #f1f1f1;
            color: black;
        }

        .footer-tabs .navbar a.active {
            background-color: #4CAF50;
            color: white;
        }

        .footer-tabs .navbar .customer_list {
            margin-right: 2px;
            margin-left: 1px;
        }


    </style>
@endsection


@section('extra-js')

@endsection