@extends('layouts.dashboard-layout')

@section('extra-css')
    <style>
        .select2-container--default .select2-selection--single {
            padding: 6px;
            height: 37px;
            width: 100%;
            font-size: 1.2em;
            position: relative;
        }

        .select2-container {
            width: 100% !important;
        }

        .select2-container--default .select2-selection--single {
            background-color: #fff;
            border: 1px solid #d2d6de !important;
            border-radius: 0px !important;
            height: 35px !important;
        }

    </style>
@endsection

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="text-center visible-xs">
            Add Vehicle Details
        </h1>
        <h1 class="hidden-xs">
            Add Vehicle Details
        </h1>
        <ol class="breadcrumb hidden-xs">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Add Vehicle Details</a></li>
            <!-- <li class="active">Blank page</li> -->
        </ol>
    </section>

    <!-- Main content -->
    <section class="">
        <div class="register-box">
            <div class="register-box-body">
                <form id="generate_quote_form">
                    {{csrf_field()}}
                    <div class="form-group has-feedback">
                        <select class="form-control" id="purpose" name="purpose">
                            <option value="">Purpose</option>
                        </select>
                    </div>
                    <div class="form-group has-feedback">
                        <select class="form-control" id="lead_make" name="lead_make">
                            <option value="">Make</option>
                        </select>
                    </div>
                    <div class="form-group has-feedback">
                        <select class="form-control" id="lead_model" name="lead_model">
                            <option value="">Model</option>
                        </select>
                    </div>
                    <div class="form-group has-feedback">
                        <select class="form-control" id="lead_year" name="lead_year">
                            <option value="">Model Year</option>
                            @php $x = (int)\Carbon\Carbon::now()->format('Y'); @endphp
                            @for($x;$x>=1990;$x--)
                                <option value="{{$x}}">{{$x}}</option>
                            @endfor
                        </select>
                    </div>
                    <div class="form-group has-feedback">
                        <select class="form-control" id="lead_fueltype" name="lead_fueltype">
                            <option value="">Fuel Type</option>
                        </select>
                    </div>
                    <div class="input-group form-group">
                        <span class="input-group-addon">LKR</span>
                        <input type="text" class="form-control" placeholder="Value" name="vl_value" id="vl_value">
                    </div>
                    <div class="form-group">
                        <label for="" class="col-xs-4 no-padding">Agent Repair</label>
                        <label for="" class="col-xs-4">
                            <input type="radio" name="agent_repair" value="1" checked="checked">
                            Yes
                        </label>
                        <label for="" class="col-xs-4">
                            <input type="radio" name="agent_repair" value="0">
                            No
                        </label>
                        <div class="clearfix"></div>
                    </div>
                    <div class="input-group form-group">
                        <input type="text" class="form-control" placeholder="No Claim Bonus %"
                               name="lead_no_claim_bonus" id="lead_no_claim_bonus">
                        <span class="input-group-addon">%</span>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-xs-4 no-padding">Lease</label>
                        <label for="" class="col-xs-4">
                            <input type="radio" name="leased" id="" value="1" checked="">
                            Yes
                        </label>
                        <label for="" class="col-xs-4">
                            <input type="radio" name="leased" id="" value="0" checked="">
                            No
                        </label>
                        <div class="clearfix"></div>
                    </div>

                    <div class="row">
                        <div class="col-xs-6 col-md-6 col-lg-6">

                        </div>
                        <!-- /.col -->
                        <div class="col-xs-12 col-md-6 col-lg-6">
                            <button type="button" class="btn btn-warning btn-block" id="generate_quote_submit">Generate
                                Quotes
                            </button>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- <div class="row">
                      <div class="col-xs-12">
                        <p class="mt-2 text-center">By click register, I agree to the <a href="#">Tearms and Conditions</a></p>
                      </div>
                    </div> -->
                </form>
            </div>
        </div>
    </section>
    <br>

@endsection

@section('extra-js')
    <script>
        var remote_url = "{{env('remote_url')}}";
    </script>
    <script>
        $('#purpose').select2({
            ajax: {
                url: remote_url + "/api/purposes/all",
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                cache: true
            },
            minimumResultsForSearch: -1
        });
        $('#lead_make').select2({
            ajax: {
                url: remote_url + "/api/vehicle/make/all",
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                cache: true
            }
        });

        $('#lead_make').on('change', function () {
            var data = $("#lead_make option:selected").val();
            var url = remote_url + "/api/vehicle/model/" + data;
            $('#lead_model').select2({
                ajax: {
                    url: url,
                    type: "get",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            searchTerm: params.term // search term
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: response
                        };
                    },
                    cache: true
                }
            });
        });

        $('#lead_model').select2();
        $('#lead_fueltype').select2({
            ajax: {
                url: remote_url + "/api/fuel/all",
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                cache: true
            },
            minimumResultsForSearch: -1
        });
        $('#lead_year').select2();
    </script>
    <script>
        $('#generate_quote_submit').click(function (e) {

            var token = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                type: "post",
                url: '/customer/get/info',
                data: {'_token': token, user_id: {{$id}}},
                success: function (userdata) {
                    var purpose = $('#purpose').val();
                    var purpose_text = $('#purpose option:selected').text();
                    var lead_make = $('#lead_make').val();
                    var lead_make_text = $('#lead_make option:selected').text();
                    var lead_model = $('#lead_model').val();
                    var lead_model_text = $('#lead_model option:selected').text();
                    var lead_year = $('#lead_year').val();
                    var lead_fueltype = $('#lead_fueltype').val();
                    var lead_fueltype_text = $('#lead_fueltype option:selected').text();
                    var vl_value = $('#vl_value').val();
                    var lead_no_claim_bonus = $('#lead_no_claim_bonus').val();
                    var agent_repair = $("input[name='agent_repair']:checked").val();
                    var leased = $("input[name='leased']:checked").val();

                    var form_data = {
                        'purpose': purpose,
                        'purpose_text': purpose_text,
                        'lead_make': lead_make,
                        'lead_make_text': lead_make_text,
                        'lead_model': lead_model,
                        'lead_model_text': lead_model_text,
                        'lead_year': lead_year,
                        'lead_fueltype': lead_fueltype,
                        'lead_fueltype_text': lead_fueltype_text,
                        'vl_value': vl_value,
                        'lead_no_claim_bonus': lead_no_claim_bonus,
                        'agent_repair': agent_repair,
                        'leased': leased
                    };
                    console.log('user data')
                    console.log(userdata)
                    $.ajax({
                        type: "post",
                        url: remote_url + '/api/crm/vehicle/new',
                        data: {'_token': token, form_data: form_data, user_data: userdata},
                        success: function (res1) {
                            console.log('remote db add')
                            console.log(res1)
                            if (res1.status) {
                                $.ajax({
                                    type: "post",
                                    url: '/customer/business/motor/save',
                                    data: {'_token': token, form_data: form_data, user_data: userdata,lead_id:res1.lead_id},
                                    success: function (res2) {
                                        console.log('local db ')
                                        console.log(res2)
                                        if (res2.status) {
                                            window.location = '/customer/business/' + res2.id;
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            });
        });
    </script>
@endsection