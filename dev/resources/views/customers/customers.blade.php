@extends('layouts.dashboard-layout')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="text-center visible-xs">
            Customers
        </h1>
        <h1 class="hidden-xs">
            Customers
        </h1>
        <ol class="breadcrumb hidden-xs">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Customers</a></li>
            <!-- <li class="active">Blank page</li> -->
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="box-footer box-comments">
                    @if(count($customers))
                        @foreach($customers as $customer)
                            <div class="box-comment">
                                <!-- User image -->
                                <img class="img-circle img-sm" src="{{asset('')}}images/icons/user.png"
                                     alt="User Image">

                                <div class="comment-text">
                                    <span class="username">{{ucfirst($customer->first_name).' '.ucfirst($customer->last_name)}}</span>
                                    <small class="text-muted pull-left">{{\Carbon\Carbon::parse($customer->created_at)->toFormattedDateString()}}</small>
                                    <a href="{{asset('')}}customer/business/select/{{$customer->id}}">
                                        <button class="btn btn-info btn-sm pull-right">New Business <i class="fa fa-arrow-circle-right"></i></button>
                                    </a>
                                </div>
                                <!-- /.comment-text -->
                            </div>
                        @endforeach
                    @else
                        <div class="box-comment">
                            <!-- User image -->
                            <p class="text-center">No customers added.</p>
                        </div>
                @endif
                <!-- /.box-comment -->

                    <!-- /.box-comment -->
                </div>
            </div>

        </div>
        <!-- /.box -->

    </section>
@endsection

@section('extra-css')

@endsection


@section('extra-js')

@endsection