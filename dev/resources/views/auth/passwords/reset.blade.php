@extends('layouts.register-layout')

@section('content')
    <div class="login-box-body">
        <h3 class="login-box-msg">Reset Password</h3>
        <form method="POST" action="{{ route('password.request') }}">
            {{ csrf_field() }}
            <input type="hidden" name="token" value="{{ $token }}">
            <div class="form-group has-feedback {{ $errors->has('email') ? ' has-error' : '' }}">
                <input type="email" class="form-control" placeholder="Email" value="{{ old('email') }}" name="email" required
                       autofocus>
                <span class="ion-email form-control-feedback"></span>
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group has-feedback {{ $errors->has('password') ? ' has-error' : '' }}">
                <input type="password" class="form-control" placeholder="Password" name="password" >
                <span class="ion-ios-locked form-control-feedback"></span>
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group has-feedback {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                <input type="password" class="form-control" placeholder="Confirm Password" name="password_confirmation" >
                <span class="ion-ios-locked form-control-feedback"></span>
                @if ($errors->has('password_confirmation'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                    </span>
                @endif
            </div>

            <div class="row">
                <br>
                <div class="col-xs-6 col-md-8 col-lg-8"></div>
                <!-- /.col -->
                <div class="col-xs-6 col-md-4 col-lg-4">
                    <button type="submit" class="btn btn-warning btn-block">Reset Password</button>
                </div>
                <!-- /.col -->
            </div>
        </form>
        <br>
        <p class="text-center">
            <a class="btn btn-link" href="{{ route('register') }}">
                <i class="ion-person-add"></i>&nbsp;&nbsp;&nbsp;Register Agent
            </a>
        </p>
    </div>
@endsection
