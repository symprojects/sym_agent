@extends('layouts.register-layout')

@section('content')
    <div class="login-box-body">
        <h3 class="login-box-msg">Reset Password</h3>
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <form method="POST" action="{{ route('password.email') }}">
            {{ csrf_field() }}
            <div class="form-group has-feedback {{ $errors->has('email') ? ' has-error' : '' }}">
                <input type="email" class="form-control" placeholder="Email" value="{{ old('email') }}" name="email" required
                       autofocus>
                <span class="ion-email form-control-feedback"></span>
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
            <div class="row">
                <br>
                <div class="col-xs-3 col-md-4 col-lg-4"></div>
                <!-- /.col -->
                <div class="col-xs-9 col-md-8 col-lg-8">
                    <button type="submit" class="btn btn-warning btn-block">Send Password Reset Link</button>
                </div>
                <!-- /.col -->
            </div>
        </form>
        <br>

        <p class="text-center">
            <a class="btn btn-link" href="{{ route('login') }}">
                <i class="ion-person"></i>&nbsp;&nbsp;&nbsp;Agent Login
            </a>
        </p>
    </div>
@endsection
