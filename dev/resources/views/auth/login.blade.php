@extends('layouts.register-layout')

@section('content')

    <div class="login-box-body">
        <h3 class="login-box-msg">Agent Login</h3>
        <form class="" method="POST" action="{{ route('login') }}" id="login_form">
            {{ csrf_field() }}
            <div class="form-group has-feedback">
                <span class="ion-email form-control-feedback"></span>
                <input type="email" class="form-control" placeholder="Email" name="email"
                       required
                       autofocus>
                @if ($errors->has('email'))
                    <label class="error">{{ $errors->first('email') }}</label>
                @endif

            </div>
            <div class="form-group has-feedback">
                <span class="ion-ios-locked form-control-feedback"></span>
                <input type="password" class="form-control" placeholder="Password" name="password">
                @if ($errors->has('password'))
                    <label class="error">{{ $errors->first('password') }}</label>
                @endif
            </div>
            <div class="row">
                <br>
                <div class="col-xs-6 col-md-8 col-lg-8"></div>
                <!-- /.col -->
                <div class="col-xs-6 col-md-4 col-lg-4">
                    <button type="submit" class="btn btn-warning btn-block">Login</button>
                </div>
                <!-- /.col -->
            </div>
        </form>
        <br>

        <p class="text-center">
            <a class="btn btn-link" href="{{ route('password.request') }}">
                Forgot Your Password?
            </a>
        </p>
        <p class="text-center">
            <a class="btn btn-link" href="{{ route('register') }}">
                <i class="ion-person-add"></i>&nbsp;&nbsp;&nbsp;Register Agent
            </a>
        </p>
    </div>
@endsection

@section('extra-js')
    <script>

        validator = $('#login_form').validate({ // initialize plugin
            // your rules & options,
            ignore: [],
            rules: {
                'email': {
                    required: true,
                    email: true,
                    minlength: 3,
                },
                'password': {
                    required: true,
                    minlength: 6
                }

            },
            messages: {

                password: {
                    required: "Please enter password"
                },
                email: {
                    required: "Please enter email",
                },

            },
            highlight: function (label) {
                $(label).closest('.form-group').addClass('has-error');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents(".form-group").removeClass("has-error");
            },
            success: function (label) {
                $(label).closest('.form-group').removeClass('has-error');
                label.remove();
            },
            errorPlacement: function (error, element) {
                var placement = element.closest('.form-group .form-control-feedback');
                if (!placement.get(0)) {
                    placement = element;
                }
                if (error.text() !== '') {
                    placement.after(error);
                }
            },
            submitHandler: function (form) {
                blockUi('processing..');
                setTimeout(function () {
                    form.submit();

                }, 1000);
            }
        });
    </script>
@endsection