@extends('layouts.register-layout')

@section('content')
    <div class="register-box-body">
        <h3 class="login-box-msg">Agent Signup</h3>

        <form class="" method="POST" action="{{ route('register') }}" id="register_form" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="form-group has-feedback">
                <span class="ion-android-person form-control-feedback"></span>
                <input type="text" class="form-control" placeholder="First Name" name="first_name" id="first_name"
                       required autofocus>
            </div>
            <div class="form-group has-feedback">
                <span class="ion-android-person form-control-feedback"></span>
                <input type="text" class="form-control" placeholder="Last Name"
                       name="last_name" id="last_name" required>
            </div>
            <div class="form-group has-feedback">
                <span class="ion-card form-control-feedback"></span>
                <input type="text" class="form-control" placeholder="NIC Number" name="nic" id="nic">
            </div>
            <div class="form-group has-feedback">
                <span class="ion-iphone form-control-feedback"></span>
                <input type="text" class="form-control" placeholder="Mobile Number" name="mobile" id="mobile">
            </div>
            <div class="form-group has-feedback">
                <span class="ion-email form-control-feedback"></span>
                <input type="email" class="form-control" placeholder="Email" id="email"
                       name="email" required>
                @if ($errors->has('email'))
                    <label class="error">{{ $errors->first('email') }}</label>
                @endif
            </div>
            <div class="form-group has-feedback">
                <span class="ion-navicon form-control-feedback"></span>
                <input type="text" class="form-control" placeholder="Address" name="address" id="address" required>
            </div>
            <div class="form-group has-feedback">
                <span class="ion-ios-locked form-control-feedback"></span>
                <input type="password" class="form-control" placeholder="Password" name="password" id="password"
                       required>
            </div>
            <div class="form-group has-feedback">
                <span class="ion-ios-unlocked form-control-feedback"></span>
                <input type="password" class="form-control" placeholder="Retype password" name="password_confirmation"
                       id="password_confirmation" required>
            </div>
            <div class="form-group has-feedback">
                <div class="avatar-upload">
                    <div class="avatar-edit">
                        <input type='file' id="imageUpload" name="imageUpload"/>
                        <label for="imageUpload"></label>
                    </div>
                    <label id="imageUpload-error" class="error" for="imageUpload"></label>
                    <div class="avatar-preview">
                        <div id="imagePreview" style="background-image: url(http://i.pravatar.cc/500?img=7);">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6 col-md-8 col-lg-8">

                </div>
                <!-- /.col -->
                <div class="col-xs-6 col-md-4 col-lg-4">
                    <button type="submit" class="btn btn-warning btn-block">Register</button>
                </div>
                <!-- /.col -->
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <p class="mt-2 text-center">By click register, I agree to the <br class="visible-xs"><a href="#">Tearms
                            and Conditions</a>
                    </p>
                    <p class="text-center">
                        <a class="btn btn-link" href="{{ route('login') }}">
                            <i class="ion-person"></i>&nbsp;&nbsp;&nbsp;Already has account?
                        </a>
                    </p>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('extra-css')
    <style>
        .register-box-body {
            margin-bottom: 18px;
        }


    </style>
@endsection

@section('extra-js')
    <script>
        validator = $('#register_form').validate({ // initialize plugin
            // your rules & options,
            ignore: [],
            rules: {
                'first_name': {
                    required: true,
                },
                'last_name': {
                    required: true,
                },
                'nic': {
                    required: true,
                    maxlength: 12,
                    minlength: 10
                },
                'mobile': {
                    required: true,
                    number: true,
                    maxlength: 10,
                    minlength: 10

                },
                'email': {
                    required: true,
                    email: true,
                    minlength: 3,
                },
                'address': {
                    required: true,
                    maxlength: 100,
                    minlength: 10
                },
                'password': {
                    required: true,
                    minlength: 6
                },
                'password_confirmation': {
                    required: true,
                    equalTo: "#password"
                },
                'imageUpload': {
                    required: true,
                    accept: "image/*"
                }

            },
            messages: {
                first_name: "Please enter first name",
                last_name: "Please enter last name",
                nic: {
                    required: "Please enter NIC number"
                },
                mobile: {
                    required: "Please enter mobile number"
                },
                address: {
                    required: "Please enter address"
                },
                password: {
                    required: "Please enter password"
                },
                password_confirmation: {
                    required: "Please enter password confirmation",
                    equalTo: 'Password not matched'
                },
                email: {
                    required: "Please enter email",

                },
                imageUpload: {
                    required: "Please upload profile image",
                    // filesize_max:200
                }

            },
            highlight: function (label) {
                $(label).closest('.form-group').addClass('has-error');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents(".form-group").removeClass("has-error");
            },
            success: function (label) {
                $(label).closest('.form-group').removeClass('has-error');
                label.remove();
            },
            errorPlacement: function (error, element) {
                var placement = element.closest('.form-group .form-control-feedback');
                if (!placement.get(0)) {
                    placement = element;
                }
                if (error.text() !== '') {
                    placement.after(error);
                }
            },
            submitHandler: function (form) {
                $.blockUI({message: '<h1><img src="busy.gif" /> Just a moment...</h1>'});
                form.submit();
                setTimeout(function () {



                }, 1000);
            }
        });
        //
        // jQuery.validator.addMethod("filesize_max", function(value, element, param) {
        //     var isOptional = this.optional(element),
        //         file;
        //
        //     if(isOptional) {
        //         return isOptional;
        //     }
        //
        //     if ($(element).attr("type") === "file") {
        //
        //         if (element.imageUpload && element.imageUpload.length) {
        //
        //             file = element.imageUpload[0];
        //             return ( file.size && file.size <= param );
        //         }
        //     }
        //     return false;
        // }, "File size is too large.");


        // ----------------image-------------------

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#imagePreview').css('background-image', 'url(' + e.target.result + ')');
                    $('#imagePreview').hide();
                    $('#imagePreview').fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imageUpload").change(function () {
            $('.avatar-preview').show();
            readURL(this);
        });
        // ----------------END image-------------------
    </script>
@endsection