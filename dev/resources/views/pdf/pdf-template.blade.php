<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>{{$title}}</title>

    <style type="text/css">
        @page {
            margin: 40px 15px;
        }

        body {
            margin: 0px;
        }

        * {
            font-family: Verdana, Arial, sans-serif;
        }

        a {
            color: #fff;
            text-decoration: none;
        }

        table {
            font-size: small;
        }

        tfoot tr td {
            font-weight: bold;
            font-size: small;
        }

        tfoot tr td {
            font-weight: bold;
            font-size: x-small;
        }

        .gray {
            background-color: lightgray
        }
    </style>

</head>
<body>
<div class="" style="width: 100%;">
    <div class="" style="width: 15%;margin-left: auto;margin-right: auto;">
        <img src="assets/images/logo.jpg" style="width: 100%;margin-top: 10px;">
    </div>
    <h2 style="text-align: center;">Insure.lk</h2>
    <br>
    <table width="50%;margin-left: auto;margin-right: auto;">
        <tbody>
        <tr style="border-bottom: 1px solid #ccc">
            <td colspan="2" style="border-bottom: 1px solid #ccc">
                <img src="{{$lead_details['picture']}}" style="width: 20%;margin-top: 10px;">
            </td>
        </tr>
        <tr style="border-bottom: 1px solid #ccc">
            <td style="border-bottom: 1px solid #ccc">Amount</td>
            <td style="border-bottom: 1px solid #ccc">LKR {{$lead_details->quote_value}}</td>
        </tr>
        <tr style="border-bottom: 1px solid #ccc">
            <td style="border-bottom: 1px solid #ccc">Discount</td>
            <td style="border-bottom: 1px solid #ccc">LKR {{$lead_details->quote_value - $lead_details->discounted_value}}</td>
        </tr>
        <tr style="border-bottom: 1px solid #ccc">
            <td style="border-bottom: 1px solid #ccc">Final Amount</td>
            <td style="border-bottom: 1px solid #ccc">LKR {{$lead_details->discounted_value}}</td>
        </tr>
        <tr style="border-bottom: 1px solid #ccc">
            <td style="border-bottom: 1px solid #ccc">Discount %</td>
            <td style="border-bottom: 1px solid #ccc">{{round((($lead_details->quote_value - $lead_details->discounted_value)/$lead_details->quote_value)*100,2)}} %</td>
        </tr>

        </tbody>
    </table>

</div>
<br>

<br>
<br>

</body>
</html>