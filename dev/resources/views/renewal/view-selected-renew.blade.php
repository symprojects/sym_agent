@extends('layouts.dashboard-layout')

@section('content')
    <section class="content-header">
        <!--<h1 class="text-center visible-xs">-->
        <!--Customers-->
        <!--</h1>-->
        <h1 class="hidden-xs">
            Insurance Company
        </h1>
        <ol class="breadcrumb hidden-xs">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Insurance Company</a></li>
            <!-- <li class="active">Blank page</li> -->
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="register-box">
            <div class="register-box-body">
                @if(session()->has('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                @endif
                @if(session()->has('error'))
                    <div class="alert alert-danger">
                        {{ session()->get('error') }}
                    </div>
                @endif
                <div class="row">
                    <div class="col-xs-12 text-center">

                        <div class="form-horizontal">
                            <div class="col-xs-12">
                                <img id="ins_com_logo" src="{{$leadquote->picture}}"
                                     style="width:150px;height: auto;object-fit: cover;display: inline;margin-right: auto; margin-left: auto">
                                <hr>
                                <form action="{{route('RenewBusinessQuote')}}" method="post">
                                    {{csrf_field()}}
                                    <input type="hidden" name="leadqoute_id" value="{{$leadquote->id}}">
                                    <input type="hidden" name="quote_no" value="{{$leadquote->quote_no}}">
                                    <input type="hidden" name="lead_id" value="{{$leadquote->lead_id}}">
                                    {{--<div class="form-group">--}}
                                        {{--<label for="inputEmail3"--}}
                                               {{--class="col-xs-12 control-label"--}}
                                               {{--style="text-align: left !important;">Insurance Provider</label>--}}
                                        {{--<div class="col-xs-12">--}}
                                            {{--<div class="input-group">--}}
                                                {{--<select name="ins_provider" id="ins_provider" class="form-control" required>--}}
                                                    {{--<option value="">Select Insurance Provider</option>--}}
                                                    {{--@foreach($insurance_providers as $ins)--}}
                                                        {{--<option value="{{$ins->ins_prov_name}}">{{$ins->ins_reg_name}}</option>--}}
                                                    {{--@endforeach--}}
                                                {{--</select>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    <div class="form-group">
                                        <label for="inputEmail3"
                                               class="col-xs-12 control-label"
                                               style="text-align: left !important;">Renewal Date</label>
                                        <div class="col-xs-12">
                                            <div class="input-group date">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" class="form-control pull-right" id="datepicker" name="renew_date"
                                                       value="{{\Carbon\Carbon::now()->format('Y-m-d')}}" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3"
                                               class="col-xs-12 control-label"
                                               style="text-align: left !important;">Amount</label>
                                        <div class="col-xs-12">
                                            <div class="input-group">
                                                <span class="input-group-addon">LKR</span>
                                                <input type="number" class="form-control" id="amount" name="amount"
                                                       placeholder="Amount" required value="{{$leadquote->discounted_value}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            {{--<div class="input-group">--}}
                                            <button class="btn btn-block btn-primary btn-sm" type="submit">Generate New
                                                Quote
                                            </button>
                                            {{--</div>--}}
                                        </div>
                                    </div>
                                    {{--<hr>--}}
                                    {{--<div class="form-group">--}}
                                        {{--<div class="col-xs-12">--}}
                                            {{--<div class="input-group">--}}
                                            {{--<button class="btn btn-block btn-success btn-sm">Proceed With Renewal--}}
                                            {{--</button>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.box -->

    </section>

    {{--@include('partials.payment-doc-back_singleview')--}}
@endsection

@section('extra-css')
    <style>

        .footer-tabs .navbar {
            overflow: hidden;
            background-color: rgba(255, 255, 255, 1);
            position: fixed;
            bottom: -1px;
            width: 100%;
            margin: 0px !important;
            border-radius: 6px 6px 0px 0px;
            min-height: 76px !important;

        }

        .footer-tabs .add-business {
            padding: 9px 15px !important;
            border-radius: 100% !important;
            margin-top: 7px;
            margin-right: 2px;
            margin-left: 2px;
            border: 2px solid #09708a !important;
            color: #fff !important;
            background: #09708a !important;
        }

        .footer-tabs .add-user {
            padding: 9px 14px !important;
            border-radius: 100% !important;
            margin-top: 7px;
            margin-right: 2px;
            margin-left: 2px;
            border: 2px solid #09708a !important;
            color: #09708a !important;
            background: #fff !important;
        }

        .footer-tabs .hot-leads {

        }

        .footer-tabs .navbar a {
            float: left;
            display: block;
            color: #fff;
            text-align: center;
            padding: 10px 8px;
            font-weight: 600;
            text-decoration: none;
            font-size: 17px;
            border-radius: 10px 10px 0px 0px;
            background: #09708a;
            border: 1px solid #09708a;
            margin-top: 3px;
        }

        .footer-tabs .navbar a:hover {
            background: #f1f1f1;
            color: black;
        }

        .footer-tabs .navbar a.active {
            background-color: #4CAF50;
            color: white;
        }

        .footer-tabs .navbar .customer_list {
            margin-right: 2px;
            margin-left: 1px;
        }


    </style>
@endsection


@section('extra-js')
    <script>
        $('#datepicker').datepicker({
            autoclose: true,
            format: "yyyy-mm-dd",
            useCurrent: true
        });
    </script>
@endsection