@extends('layouts.dashboard-layout')

@section('content')
    <section class="content-header">
        <!--<h1 class="text-center visible-xs">-->
        <!--Customers-->
        <!--</h1>-->
        <h1 class="hidden-xs">
            Insurance Company
        </h1>
        <ol class="breadcrumb hidden-xs">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Insurance Company</a></li>
            <!-- <li class="active">Blank page</li> -->
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="register-box">
            <div class="register-box-body">
                @if(session()->has('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                @endif
                @if(session()->has('error'))
                    <div class="alert alert-danger">
                        {{ session()->get('error') }}
                    </div>
                @endif
                <div class="row">
                    <div class=" col-xs-12 text-center">
                        <img id="ins_com_logo" src="{{$lead_qoute->picture}}"
                             style="width:150px;height: auto;object-fit: cover;display: inline;margin-right: auto; margin-left: auto">
                        <hr>
                        <div class="form-horizontal">
                            <div class=" col-xs-12">
                                <div class="form-group">
                                    <label for="inputEmail3"
                                           class="col-xs-6 control-label text-right">Amount</label>
                                    <div class="col-xs-6">
                                        <div class="input-group">
                                            <span class="input-group-addon">LKR</span>
                                            <input type="number" class="form-control" id="amount" placeholder="Amount"
                                                   readonly="readonly" value="{{$requests['amount']}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3"
                                           class="col-xs-6 control-label text-right">Renewal Date</label>
                                    <div class="col-xs-6">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control" id="discount"
                                                   placeholder="Discount" readonly="readonly" value="{{$requests['renew_date']}}">
                                        </div>
                                    </div>
                                </div>

                                {{--<hr>--}}
                                {{--<div class="form-group">--}}
                                    {{--<div class="col-xs-12">--}}
                                        {{--<button class="btn btn-block btn-success btn-sm">Proceed With Renewal--}}
                                        {{--</button>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            </div>
                            <div class="row">
                                <div class="col-xs-6 col-md-6 col-lg-6 pr-2">
                                    <form action="{{route('printInvoiceRenew')}}" method="post" target="_blank">
                                        {{csrf_field()}}
                                        <input type="hidden" value="{{$lead_no}}" name="lead_id">
                                        <input type="hidden" value="invoice" name="type">
                                        <input type="hidden" value="{{$requests['leadqoute_id']}}" name="lead_quote">
                                        <input type="hidden" name="contents" id="content">
                                        <button type="submit" class="btn btn-warning btn-block" >Print Invoice</button>
                                    </form>

                                </div>
                                <!-- /.col -->
                                <div class="col-xs-6 col-md-6 col-lg-6  pl-2">
                                    <form action="{{route('sendEmail')}}" method="post" class="form-inline">
                                        {{csrf_field()}}
                                        <input type="hidden" name="contents" id="content2">
                                        <input type="hidden" value="{{$lead_no}}" name="lead_id">
                                        <input type="hidden" name="type" value="invoice_renew">
                                        <input type="hidden" value="{{$requests['leadqoute_id']}}" name="lead_quote">
                                        <input type="hidden" name="email" value="{{$lead_details->user_email}}">
                                        <input type="hidden" name="user_name" value="{{$lead_details->user_fname}}">
                                        <button type="submit" class="btn btn-warning btn-block">Send Invoice</button>
                                    </form>

                                </div>
                                <!-- /.col -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.box -->

    </section>

   @include('partials.payment-doc-back_singleview')
@endsection

@section('extra-css')
    <style>

        .footer-tabs .navbar {
            overflow: hidden;
            background-color: rgba(255, 255, 255, 1);
            position: fixed;
            bottom: -1px;
            width: 100%;
            margin: 0px !important;
            border-radius: 6px 6px 0px 0px;
            min-height: 76px !important;

        }

        .footer-tabs .add-business {
            padding: 9px 15px !important;
            border-radius: 100% !important;
            margin-top: 7px;
            margin-right: 2px;
            margin-left: 2px;
            border: 2px solid #09708a !important;
            color: #fff !important;
            background: #09708a !important;
        }

        .footer-tabs .add-user {
            padding: 9px 14px !important;
            border-radius: 100% !important;
            margin-top: 7px;
            margin-right: 2px;
            margin-left: 2px;
            border: 2px solid #09708a !important;
            color: #09708a !important;
            background: #fff !important;
        }

        .footer-tabs .hot-leads {

        }

        .footer-tabs .navbar a {
            float: left;
            display: block;
            color: #fff;
            text-align: center;
            padding: 10px 8px;
            font-weight: 600;
            text-decoration: none;
            font-size: 17px;
            border-radius: 10px 10px 0px 0px;
            background: #09708a;
            border: 1px solid #09708a;
            margin-top: 3px;
        }

        .footer-tabs .navbar a:hover {
            background: #f1f1f1;
            color: black;
        }

        .footer-tabs .navbar a.active {
            background-color: #4CAF50;
            color: white;
        }

        .footer-tabs .navbar .customer_list {
            margin-right: 2px;
            margin-left: 1px;
        }


    </style>
@endsection


@section('extra-js')
    {{--<script>--}}
        {{--var quote_no = {{$quote_no}};--}}
        {{--var remote_url = "{{env('remote_url')}}";--}}
        {{--var token = $('meta[name="csrf-token"]').attr('content');--}}

        {{--$(document).ready(function () {--}}
            {{--$.ajax({--}}
                {{--type: "post",--}}
                {{--url: remote_url + '/api/quote/insurence/single/' + quote_no,--}}
                {{--data: {'_token': token, quote_no: quote_no},--}}
                {{--success: function (res) {--}}
                    {{--console.log(res);--}}

                    {{--$('#content').val(JSON.stringify(res[0]));--}}
                    {{--$('#content2').val(JSON.stringify(res[0]));--}}
                    {{--console.log(res[0].quote_no);--}}
                    {{--$('#amount').val(res[0].quote_value).attr('readonly', 'readonly');--}}
                    {{--$('#discount').val(calcDiscount(res[0].quote_value, res[0].discounted_value)).attr('readonly', 'readonly');--}}
                    {{--$('#final_amount').val(res[0].discounted_value).attr('readonly', 'readonly');--}}
                    {{--$('#discount_rate').val(calcDiscountRate(calcDiscount(res[0].quote_value, res[0].discounted_value), res[0].quote_value)).attr('readonly', 'readonly');--}}
                    {{--$('#ins_com_logo').attr('src', res[0].picture);--}}
                {{--}--}}
            {{--});--}}
        {{--});--}}

        {{--function calcDiscountRate(discount, amount) {--}}
            {{--return (discount / amount) * 100;--}}
        {{--}--}}

        {{--function calcDiscount(quote_value, discounted_value) {--}}
            {{--return parseFloat(quote_value) - parseFloat(discounted_value);--}}
        {{--}--}}
    {{--</script>--}}
@endsection