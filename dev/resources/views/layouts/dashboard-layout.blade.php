@php $user =\Illuminate\Support\Facades\Auth::user(); @endphp
        <!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{env("APP_NAME")}} | Blank Page</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="siteURL" content="https://insure.lk">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="{{asset('')}}assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{asset('')}}assets/bower_components/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{asset('')}}assets/bower_components/Ionicons/css/ionicons.min.css">
    <link rel="stylesheet" href="{{asset('')}}css/sym-styles.css?v=1">
    <link rel="stylesheet" href="{{asset('')}}assets/dist/css/skins/_all-skins.min.css">
    <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet"
          href="{{asset('')}}assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet"/>

    <link rel="stylesheet" href="{{asset('')}}assets/plugins/timepicker/bootstrap-timepicker.min.css">
    <link rel="stylesheet" href="{{asset('')}}assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <link rel="stylesheet" href="{{asset('')}}plugins/noty/noty.css">
    <link rel="stylesheet" href="{{asset('')}}plugins/noty/animate.css">
    <link rel="stylesheet" href="{{asset('')}}plugins/noty/themes/mint.css">
    <link rel="stylesheet" href="{{asset('')}}plugins/noty/themes/nest.css">
    <link rel="stylesheet" href="{{asset('')}}plugins/noty/themes/sunset.css">
    @yield('extra-css')
    <style>
        .datepicker-dropdown {
            top: 0;
            left: 0;
            padding: 4px;
            background-color: #2C6E8E;
            border-radius: 10px;
        }
        .datepicker table {
            margin: 0;
            -webkit-touch-callout: none;
            -webkit-user-select: none;
            -khtml-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;

        }

        .datepicker table tr td,
        .datepicker table tr th {
            text-align: center;
            width: 35px;
            height: 35px;
            border-radius: 4px;
            border: none;
            color: #FFFFFF;
            font-weight: 700;
        }

        .datepicker table tr td.day:hover,
        .datepicker table tr td.focused {
            background: #555555;
            cursor: pointer;
        }
        .datepicker table tr td.old,
        .datepicker table tr td.new {
            color: #FF8000;
        }

        .datepicker table tr td.today {
            color: #000000;
            background-color: #FFA953;
            border-color: #FFB76F;
        }
        .datepicker table tr td.today:hover {
            color: #FFFFFF;
            background-color: #884400;
            border-color: #f59e00;
        }
        .datepicker table tr td.active:active,
        .datepicker table tr td.active.highlighted:active,
        .datepicker table tr td.active.active,
        .datepicker table tr td.active.highlighted.active,
        .open > .dropdown-toggle.datepicker table tr td.active,
        .open > .dropdown-toggle.datepicker table tr td.active.highlighted {
            color: #ffffff;
            background-color: #419841;
            border-color: #285e8e;
        }


        .datepicker table tr td.active:active:hover,
        .datepicker table tr td.active.highlighted:active:hover,
        .datepicker table tr td.active.active:hover,
        .datepicker table tr td.active.highlighted.active:hover,
        .open > .dropdown-toggle.datepicker table tr td.active:hover,
        .open > .dropdown-toggle.datepicker table tr td.active.highlighted:hover,
        .datepicker table tr td.active:active:focus,
        .datepicker table tr td.active.highlighted:active:focus,
        .datepicker table tr td.active.active:focus,
        .datepicker table tr td.active.highlighted.active:focus,
        .open > .dropdown-toggle.datepicker table tr td.active:focus,
        .open > .dropdown-toggle.datepicker table tr td.active.highlighted:focus,
        .datepicker table tr td.active:active.focus,
        .datepicker table tr td.active.highlighted:active.focus,
        .datepicker table tr td.active.active.focus,
        .datepicker table tr td.active.highlighted.active.focus,
        .open > .dropdown-toggle.datepicker table tr td.active.focus,
        .open > .dropdown-toggle.datepicker table tr td.active.highlighted.focus {
            color: #ffffff;
            background-color: #285e8e;
            border-color: #193c5a;
        }
        .datepicker table tr td.active:active,
        .datepicker table tr td.active.highlighted:active,
        .datepicker table tr td.active.active,
        .datepicker table tr td.active.highlighted.active,
        .open > .dropdown-toggle.datepicker table tr td.active,
        .open > .dropdown-toggle.datepicker table tr td.active.highlighted {
            color: #ffffff;
            background-color: #3071a9;
            border-color: #285e8e;
        }
        .datepicker table tr td.active:active:hover,
        .datepicker table tr td.active.highlighted:active:hover,
        .datepicker table tr td.active.active:hover,
        .datepicker table tr td.active.highlighted.active:hover,
        .open > .dropdown-toggle.datepicker table tr td.active:hover,
        .open > .dropdown-toggle.datepicker table tr td.active.highlighted:hover,
        .datepicker table tr td.active:active:focus,
        .datepicker table tr td.active.highlighted:active:focus,
        .datepicker table tr td.active.active:focus,
        .datepicker table tr td.active.highlighted.active:focus,
        .open > .dropdown-toggle.datepicker table tr td.active:focus,
        .open > .dropdown-toggle.datepicker table tr td.active.highlighted:focus,
        .datepicker table tr td.active:active.focus,
        .datepicker table tr td.active.highlighted:active.focus,
        .datepicker table tr td.active.active.focus,
        .datepicker table tr td.active.highlighted.active.focus,
        .open > .dropdown-toggle.datepicker table tr td.active.focus,
        .open > .dropdown-toggle.datepicker table tr td.active.highlighted.focus {
            color: #ffffff;
            background-color: #285e8e;
            border-color: #193c5a;
        }
        .datepicker .datepicker-switch {
            width: 145px;

        }
        .datepicker .datepicker-switch:hover,
        .datepicker .prev:hover,
        .datepicker .next:hover,
        .datepicker tfoot tr th:hover {
            background: #50A2C9;
        }
    </style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

    <header class="main-header">
        <!-- Logo -->
        <a href="{{asset('')}}assets/index2.html" class="logo hidden-xs">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>{{env("APP_NAME")}}</b></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>{{env("APP_NAME")}}</b></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="{{asset('')}}uploads/profile_images/{{$user->profile_image}}" class="user-image"
                                 alt="User Image">
                            <span class="hidden-xs">{{$user->name}}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="{{asset('')}}uploads/profile_images/{{$user->profile_image}}"
                                     class="img-circle" alt="User Image">

                                <p>
                                    {{$user->name}} - Agent
                                    <small>Since -
                                        <b>{{\Carbon\Carbon::parse($user->created_at)->toFormattedDateString()}}</b>
                                    </small>
                                </p>
                            </li>
                            <li class="user-footer">
                                <div class="pull-left">
                                    {{--<a href="#" class="btn btn-default btn-flat">Profile</a>--}}
                                </div>
                                <div class="pull-right">
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"
                                       class="btn btn-default btn-flat">
                                        Logout
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>

    <!-- =============================================== -->

    <!-- Left side column. contains the sidebar -->
@include('layouts.partials.side-nav')

<!-- =============================================== -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        @yield('content')
    </div>
    <!-- /.content-wrapper -->

    <footer class="main-footer">
        {{--<div class="pull-right hidden-xs">--}}
        {{--<b>Version</b> 2.4.0--}}
        {{--</div>--}}
        <strong>Copyright &copy; 2019 {{env("APP_NAME")}}</strong> <br class="visible-xs"> All rights
        reserved.
    </footer>

    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    {{ csrf_field() }}
</form>
<!-- jQuery 3 -->
<script src="{{asset('')}}assets/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('')}}assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="{{asset('')}}assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="{{asset('')}}assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="{{asset('')}}assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="{{asset('')}}assets/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="{{asset('')}}assets/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('')}}assets/dist/js/demo.js"></script>

<script src="{{asset('')}}plugins/noty/noty.min.js"></script>
<script src="{{asset('')}}plugins/noty/velocity.min.js"></script>
<script src="{{asset('')}}plugins/noty/bouncejs/bounce.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script src="http://malsup.github.io/jquery.blockUI.js"></script>
<script src="{{asset('')}}assets/dist/js/general.js"></script>
<script src="{{asset('')}}assets/bower_components/moment/min/moment.min.js"></script>
<script src="{{asset('')}}assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="{{asset('')}}assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<script src="{{asset('')}}assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script>
    $(document).ready(function () {
        $('.sidebar-menu').tree()
    })
    function notyalert(type,message,timeout) {
        new Noty({
            theme: 'sunset',
            text: message,
            type: type,
            layout: 'topRight',
            timeout:timeout,
            killer:true,
            closeWith: ['click', 'button'],
            animation: {
                open: 'animated bounceInRight', // Animate.css class names
                close: 'animated bounceOutRight' // Animate.css class names
            }
        }).show();
    }
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
@yield('extra-js')
</body>
</html>
