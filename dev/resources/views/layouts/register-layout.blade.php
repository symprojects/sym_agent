<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{env("APP_NAME")}} | Registration Page</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="{{asset('')}}assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{asset('')}}assets/bower_components/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{asset('')}}assets/bower_components/Ionicons/css/ionicons.min.css">
    <link rel="stylesheet" href="{{asset('')}}css/sym-styles.css?v=1">
    <link rel="stylesheet" href="{{asset('')}}assets/plugins/iCheck/square/blue.css">
    <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    @yield('extra-css')
    <style>
        .register-logo img{
            width: 35%;
            margin: auto;
        }
        a {
            color: #091b77 !important;
        }
    </style>
</head>
<body class="hold-transition register-page">
<div class="register-box">
    <div class="register-logo">
        <img src="{{asset('')}}assets/images/logo.jpg" alt="" class="img-responsive img-circle">
        {{--<a href="{{asset('')}}assets/index2.html"><b>Admin</b>LTE</a>--}}
    </div>

@yield('content')
</div>

<script src="{{asset('')}}assets/bower_components/jquery/dist/jquery.min.js"></script>
<script src="{{asset('')}}assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="{{asset('')}}assets/plugins/iCheck/icheck.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script src="http://malsup.github.io/jquery.blockUI.js"></script>
<script src="{{asset('')}}assets/dist/js/general.js"></script>
<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' /* optional */
        });
    });
</script>
@yield('extra-js')
</body>
</html>
