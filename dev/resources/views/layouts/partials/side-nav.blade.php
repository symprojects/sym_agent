<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{asset('')}}uploads/profile_images/{{$user->profile_image}}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{$user->name}}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Agent</a>
            </div>
        </div>
        <!-- search form -->
    {{--<form action="#" method="get" class="sidebar-form">--}}
    {{--<div class="input-group">--}}
    {{--<input type="text" name="q" class="form-control" placeholder="Search...">--}}
    {{--<span class="input-group-btn">--}}
    {{--<button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>--}}
    {{--</button>--}}
    {{--</span>--}}
    {{--</div>--}}
    {{--</form>--}}
    <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">NAVIGATION</li>
            <li>
                <a href="{{asset('')}}dashboard">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                    <span class="pull-right-container">
                      <i class=" fa-angle-left ion-ios-arrow-thin-right pull-right"></i>
                    </span>
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-user-circle"></i>
                    <span>Customers</span>
                    <span class="pull-right-container">
                      <i class=" fa-angle-left ion-ios-arrow-thin-right pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('add_customer')}}"><i class="fa fa-circle-o"></i> Add New Customer</a></li>
                    <li><a href="{{route('list_customers')}}"><i class="fa fa-circle-o"></i> List Customers</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-list"></i>
                    <span>Businesses</span>
                    <span class="pull-right-container">
                      <i class=" fa-angle-left ion-ios-arrow-thin-right pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('listBusiness')}}"><i class="fa fa-circle-o"></i> Ongoing Businesses</a></li>
                    <li><a href="{{route('listComplatedBusiness')}}"><i class="fa fa-circle-o"></i> Completed Businesses</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-list"></i>
                    <span>Renewals</span>
                    <span class="pull-right-container">
                      <i class=" fa-angle-left ion-ios-arrow-thin-right pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('renewSoon')}}"><i class="fa fa-circle-o"></i> Renew Soon Business</a></li>
                    <li><a href="{{route('renewalsComplated')}}"><i class="fa fa-circle-o"></i> Completed Renewals</a></li>
                </ul>
            </li>

            {{--<li><a href="{{asset('')}}add-vehical"><i class="fa fa-book"></i> <span>Add Vehicle Details</span></a></li>--}}
            {{--<li><a href="{{asset('')}}customers"><i class="fa fa-book"></i> <span>Customers</span></a></li>--}}
            {{--<li><a href="{{asset('')}}insurance-company-1"><i class="fa fa-book"></i> <span>Insurance Company 1</span></a></li>--}}
            {{--<li><a href="{{asset('')}}insurance-company-2"><i class="fa fa-book"></i> <span>Insurance Company 2</span></a></li>--}}
            {{--<li><a href="{{asset('')}}loa"><i class="fa fa-book"></i> <span>LOA</span></a></li>--}}
            {{--<li><a href="{{asset('')}}loa-to-company"><i class="fa fa-book"></i> <span>LOA to company</span></a></li>--}}
            {{--<li><a href="{{asset('')}}new-business"><i class="fa fa-book"></i> <span>New Business</span></a></li>--}}
            {{--<li><a href="{{asset('')}}quotations"><i class="fa fa-book"></i> <span>Quotations</span></a></li>--}}
            {{--<li><a href="{{asset('')}}register-new-customer"><i class="fa fa-book"></i> <span>Register New Customer</span></a></li>--}}
            {{--<li><a href="{{asset('')}}renewal"><i class="fa fa-book"></i> <span>Renewal</span></a></li>--}}
            {{--<li><a href="{{asset('')}}selected-business"><i class="fa fa-book"></i> <span>Selected Business</span></a></li>--}}
            {{--<li class="header">LABELS</li>--}}

        </ul>
    </section>
    <!-- /.sidebar -->
</aside>