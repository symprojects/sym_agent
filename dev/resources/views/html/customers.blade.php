@extends('layouts.dashboard-layout')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="text-center visible-xs">
            Customers
        </h1>
        <h1 class="hidden-xs">
            Customers
        </h1>
        <ol class="breadcrumb hidden-xs">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Customers</a></li>
            <!-- <li class="active">Blank page</li> -->
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="box-footer box-comments">
                    <div class="box-comment">
                        <!-- User image -->
                        <img class="img-circle img-sm" src="{{asset('')}}images/icons/user.png" alt="User Image">

                        <div class="comment-text">
                      <span class="username">
                        Krish
                        <small class="text-muted pull-right">8:03 PM Today</small>
                      </span>
                            <span class="pull-right badge bg-blue">New Customer</span>
                        </div>
                        <!-- /.comment-text -->
                    </div>
                    <!-- /.box-comment -->
                    <div class="box-comment">
                        <!-- User image -->
                        <img class="img-circle img-sm" src="{{asset('')}}images/icons/user.png" alt="User Image">

                        <div class="comment-text">
                      <span class="username">
                        Piyumali
                        <small class="text-muted pull-right">8:03 PM Today</small>
                      </span>
                            <span class="pull-right badge bg-blue">New Customer</span>
                        </div>
                        <!-- /.comment-text -->
                    </div>
                    <!-- /.box-comment -->
                </div>
            </div>

        </div>
        <!-- /.box -->

    </section>
@endsection

@section('extra-css')

@endsection


@section('extra-js')

@endsection