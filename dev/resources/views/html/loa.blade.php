@extends('layouts.dashboard-layout')

@section('content')
    <section class="content-header">
        <h1 class="text-center visible-xs">
            letter of Authority
        </h1>
        <h1 class="hidden-xs">
            letter of Authority
        </h1>
        <ol class="breadcrumb hidden-xs">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">letter of Authority</a></li>
            <!-- <li class="active">Blank page</li> -->
        </ol>
    </section>

    <!-- Main content -->
    <section class="">
        <div class="register-box">
            <div class="register-box-body">
                <form action="./assets/index.html" method="post">
                    <br>
                    <div class="row">

                        <!-- /.col -->
                        <div class="col-xs-12 col-md-12 col-lg-12">
                            <button type="submit" class="btn btn-warning btn-block">Send LOA to Customer</button>
                        </div>
                        <!-- /.col -->
                    </div>
                    <br>
                    <!-- <div class="row">
                      <div class="col-xs-12">
                        <p class="mt-2 text-center">By click register, I agree to the <a href="#">Tearms and Conditions</a></p>
                      </div>
                    </div> -->
                </form>
            </div>
        </div>
    </section>
    <br>
@endsection

@section('extra-css')

@endsection


@section('extra-js')

@endsection