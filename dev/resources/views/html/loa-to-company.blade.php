@extends('layouts.dashboard-layout')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="text-center visible-xs">
            Renewal
        </h1>
        <h1 class="hidden-xs">
            Renewal
        </h1>
        <ol class="breadcrumb hidden-xs">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Renewal</a></li>
            <!-- <li class="active">Blank page</li> -->
        </ol>
    </section>

    <!-- Main content -->
    <section class="">
        <div class="register-box">
            <div class="register-box-body">
                <form action="./assets/index.html" method="post">
                    <div class="form-group">
                        <label for="" class="col-xs-4 no-padding">Customer Sent Us</label>
                        <label for="" class="col-xs-4">
                            <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked="">
                            Yes
                        </label>
                        <label for="" class="col-xs-4">
                            <input type="radio" name="optionsRadios" id="optionsRadios2" value="option1" checked="">
                            No
                        </label>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group has-feedback">
                        <div class="avatar-upload loa">
                            <div class="avatar-edit">
                                <input type='file' id="imageUpload" name="imageUpload"/>
                                <label for="imageUpload"></label>
                            </div>
                            <label id="imageUpload-error" class="error" for="imageUpload"></label>
                            <div class="avatar-preview">
                                <div id="imagePreview" style="background-image: url(http://i.pravatar.cc/500?img=7);">
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">

                        <!-- /.col -->
                        <div class="col-xs-12 col-md-12 col-lg-12">
                            <button type="submit" class="btn btn-warning btn-block">
                                Send LOA to Insurance Company
                            </button>
                        </div>
                        <!-- /.col -->
                    </div>
                    <br>

                </form>
            </div>
        </div>
    </section>
    <br>

@endsection

@section('extra-css')

@endsection


@section('extra-js')

@endsection