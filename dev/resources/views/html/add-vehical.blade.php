@extends('layouts.dashboard-layout')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="text-center visible-xs">
            Add Vehicle Details
        </h1>
        <h1 class="hidden-xs">
            Add Vehicle Details
        </h1>
        <ol class="breadcrumb hidden-xs">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Add Vehicle Details</a></li>
            <!-- <li class="active">Blank page</li> -->
        </ol>
    </section>

    <!-- Main content -->
    <section class="">
        <div class="register-box">
            <div class="register-box-body">
                <form action="./assets/index.html" method="post">
                    <div class="form-group has-feedback">
                        <select class="form-control">
                            <option value="">Make</option>
                        </select>
                    </div>
                    <div class="form-group has-feedback">
                        <select class="form-control">
                            <option value="">Model</option>
                        </select>
                    </div>
                    <div class="form-group has-feedback">
                        <select class="form-control">
                            <option value="">Model Year</option>
                        </select>
                    </div>
                    <div class="form-group has-feedback">
                        <select class="form-control">
                            <option value="">Fuel Type</option>
                        </select>
                    </div>
                    <div class="input-group form-group">
                        <span class="input-group-addon">LKR</span>
                        <input type="text" class="form-control" placeholder="Value">
                    </div>
                    <div class="form-group">
                        <label for="" class="col-xs-4 no-padding">Agent Repair</label>
                        <label for="" class="col-xs-4">
                            <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked="">
                            Yes
                        </label>
                        <label for="" class="col-xs-4">
                            <input type="radio" name="optionsRadios" id="optionsRadios2" value="option1" checked="">
                            No
                        </label>
                        <div class="clearfix"></div>
                    </div>
                    <div class="input-group form-group">
                        <input type="text" class="form-control" placeholder="No Claim Bonus %">
                        <span class="input-group-addon">%</span>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-xs-4 no-padding">Lease</label>
                        <label for="" class="col-xs-4">
                            <input type="radio" name="optionsRadios" id="" value="option1" checked="">
                            Yes
                        </label>
                        <label for="" class="col-xs-4">
                            <input type="radio" name="optionsRadios" id="" value="option1" checked="">
                            No
                        </label>
                        <div class="clearfix"></div>
                    </div>

                    <div class="row">
                        <div class="col-xs-6 col-md-6 col-lg-6">

                        </div>
                        <!-- /.col -->
                        <div class="col-xs-12 col-md-6 col-lg-6">
                            <button type="submit" class="btn btn-warning btn-block">Generate Quotes</button>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- <div class="row">
                      <div class="col-xs-12">
                        <p class="mt-2 text-center">By click register, I agree to the <a href="#">Tearms and Conditions</a></p>
                      </div>
                    </div> -->
                </form>
            </div>
        </div>
    </section>
    <br>

@endsection
