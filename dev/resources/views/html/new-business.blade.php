@extends('layouts.dashboard-layout')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="text-center visible-xs">
            New Business
        </h1>
        <h1 class="hidden-xs">
            New Business
        </h1>
        <ol class="breadcrumb hidden-xs">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">New Business</a></li>
            <!-- <li class="active">Blank page</li> -->
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box box-info">
            <br>
            <div class="box-body new-business" >
                <div class="col-md-3">
                    <div class="info-box bg-yellow">
                        <span class="info-box-icon"><i class="ion ion-model-s"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Motor</span>

                        </div>
                        <!-- /.info-box-content -->
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="info-box bg-aqua">
                        <span class="info-box-icon"><i class="ion-android-plane"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Travel</span>

                        </div>
                        <!-- /.info-box-content -->
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="info-box bg-green">
                        <span class="info-box-icon"><i class="ion ion-ios-heart-outline"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Health</span>

                        </div>
                        <!-- /.info-box-content -->
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="info-box bg-purple">
                        <span class="info-box-icon"><i class="ion-ios-home-outline"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Dwelling</span>

                        </div>
                        <!-- /.info-box-content -->
                    </div>
                </div>

            </div>

        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection

@section('extra-css')
    <style>
        .new-business .info-box-text{
            font-size: 25px;
            margin-top: 22px;
        }
    </style>
@endsection


@section('extra-js')

@endsection