@extends('layouts.dashboard-layout')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="text-center visible-xs">
            Register New Customer
        </h1>
        <h1 class="hidden-xs">
            Register New Customer
        </h1>
        <ol class="breadcrumb hidden-xs">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Register New Customer</a></li>
            <!-- <li class="active">Blank page</li> -->
        </ol>
    </section>

    <!-- Main content -->
    <section class="">
        <div class="register-box">
            <div class="register-box-body">
                <form action="./assets/index.html" method="post">
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" placeholder="First Name">
                        <span class="ion-android-person form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" placeholder="Last Name">
                        <span class="ion-android-person form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" placeholder="NIC Number">
                        <span class="ion-card form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" placeholder="Mobile Number">
                        <span class="ion-iphone form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="email" class="form-control" placeholder="Email">
                        <span class="ion-email form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" placeholder="Address">
                        <span class="ion-navicon form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" class="form-control" placeholder="Password">
                        <span class="ion-ios-locked form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" class="form-control" placeholder="Retype password">
                        <span class="ion-ios-unlocked form-control-feedback"></span>
                    </div>

                    <div class="row">
                        <div class="col-xs-6 col-md-6 col-lg-6">

                        </div>
                        <!-- /.col -->
                        <div class="col-xs-6 col-md-6 col-lg-6">
                            <button type="submit" class="btn btn-warning btn-block">Register</button>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- <div class="row">
                      <div class="col-xs-12">
                        <p class="mt-2 text-center">By click register, I agree to the <a href="#">Tearms and Conditions</a></p>
                      </div>
                    </div> -->
                </form>
            </div>
        </div>
    </section>
    <br>
    <!-- /.content -->
@endsection

@section('extra-css')

@endsection


@section('extra-js')

@endsection