@extends('layouts.dashboard-layout')

@section('content')
    <section class="content-header">
        <!--<h1 class="text-center visible-xs">-->
        <!--Customers-->
        <!--</h1>-->
        <h1 class="hidden-xs">
            Insurance Company
        </h1>
        <ol class="breadcrumb hidden-xs">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Insurance Company</a></li>
            <!-- <li class="active">Blank page</li> -->
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-4 col-md-4 col-lg-4 col-xs-12 text-center">
                        <img src="https://bckend.saveyourmonkey.com/uploads/ins/hj6Ca9Efzm6DSgbJ_16.png"
                             style="width:150px;height: auto;object-fit: cover;display: inline;margin-right: auto; margin-left: auto">
                        <form action="#" class="form-horizontal">
                            <div class="col-sm-8 col-md-8 col-lg-8 col-xs-12">
                                <div class="form-group">
                                    <label for="inputEmail3"
                                           class="col-xs-6 control-label text-right">Amount</label>
                                    <div class="col-xs-6">
                                        <input type="email" class="form-control" id="" placeholder="Amount">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3"
                                           class="col-xs-6 control-label text-right">Discount</label>
                                    <div class="col-xs-6">
                                        <input type="email" class="form-control" id=""
                                               placeholder="Discount">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-xs-6 control-label text-right">Final
                                        Amount</label>
                                    <div class="col-xs-6">
                                        <input type="email" class="form-control" id=""
                                               placeholder="Final Amount">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-xs-6 control-label text-right">Discount
                                        %</label>
                                    <div class="col-xs-6">
                                        <div class="input-group">
                                            <input type="text" class="form-control" placeholder="Discount %">
                                            <span class="input-group-addon">%</span>

                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6 col-md-6 col-lg-6 pr-2">
                                    <button type="submit" class="btn btn-warning btn-block">Print Invoice</button>

                                </div>
                                <!-- /.col -->
                                <div class="col-xs-6 col-md-6 col-lg-6  pl-2">
                                    <button type="submit" class="btn btn-warning btn-block">Send Invoice</button>
                                </div>
                                <!-- /.col -->
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.box -->

    </section>

    <div class="footer-tabs visible-xs">
        <div class="navbar">
            <div class="" style="margin-top: 5px;margin-bottom: 5px">
                <div class="col-xs-4" style="margin-top: 5px">
                    <label for=""><small>Payment Link</small></label>
                </div>
                <div class="col-xs-8">
                    <div class="input-group input-group-sm">
                        <input type="text" class="form-control" placeholder="Payment Link">
                        <span class="input-group-btn">
                      <button type="button" class="btn btn-warning">Send</button>
                    </span>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="row">
                <div class="col-xs-4 pr-2">
                    <button class="btn btn-block btn-primary">Payments</button>
                </div>
                <div class="col-xs-5 padding-2">
                    <button class="btn btn-block btn-primary">Documentation</button>
                </div>
                <div class="col-xs-3 pl-2 ">
                    <button class="btn btn-block btn-danger"><i class="ion-android-arrow-back"></i> Back</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('extra-css')
    <style>

        .footer-tabs .navbar {
            overflow: hidden;
            background-color: rgba(255, 255, 255, 1);
            position: fixed;
            bottom: -1px;
            width: 100%;
            margin: 0px !important;
            border-radius: 6px 6px 0px 0px;
            min-height: 76px !important;


        }

        .footer-tabs .add-business {
            padding: 9px 15px !important;
            border-radius: 100% !important;
            margin-top: 7px;
            margin-right: 2px;
            margin-left: 2px;
            border: 2px solid #09708a !important;
            color: #fff !important;
            background: #09708a !important;
        }

        .footer-tabs .add-user {
            padding: 9px 14px !important;
            border-radius: 100% !important;
            margin-top: 7px;
            margin-right: 2px;
            margin-left: 2px;
            border: 2px solid #09708a !important;
            color: #09708a !important;
            background: #fff !important;
        }

        .footer-tabs .hot-leads {

        }

        .footer-tabs .navbar a {
            float: left;
            display: block;
            color: #fff;
            text-align: center;
            padding: 10px 8px;
            font-weight: 600;
            text-decoration: none;
            font-size: 17px;
            border-radius: 10px 10px 0px 0px;
            background: #09708a;
            border: 1px solid #09708a;
            margin-top: 3px;
        }

        .footer-tabs .navbar a:hover {
            background: #f1f1f1;
            color: black;
        }

        .footer-tabs .navbar a.active {
            background-color: #4CAF50;
            color: white;
        }

        .footer-tabs .navbar .customer_list {
            margin-right: 2px;
            margin-left: 1px;
        }


    </style>
@endsection


@section('extra-js')

@endsection