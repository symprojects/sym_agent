@extends('layouts.dashboard-layout')

@section('content')
    <section class="content-header">
        <h1 class="text-center visible-xs">
            Renewal
        </h1>
        <h1 class="hidden-xs">
            Renewal
        </h1>
        <ol class="breadcrumb hidden-xs">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Renewal</a></li>
            <!-- <li class="active">Blank page</li> -->
        </ol>
    </section>

    <!-- Main content -->
    <section class="">
        <div class="register-box">
            <div class="register-box-body">
                <form action="./assets/index.html" method="post">
                    <div class="form-group has-feedback">
                        <select class="form-control">
                            <option value="">Insurance Company</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <!--<label>Date of renewal</label>-->
                        <div class="input-group date">
                            <input type="text" class="form-control pull-right" id="datepicker" placeholder="Date of renewal">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                        </div>
                        <!-- /.input group -->
                    </div>
                    <div class="input-group form-group">
                        <span class="input-group-addon">LKR</span>
                        <input type="text" class="form-control" placeholder="Value">
                    </div>

                    <div class="row">
                        <div class="col-xs-6 col-md-4 col-lg-4">

                        </div>
                        <!-- /.col -->
                        <div class="col-xs-12 col-md-8 col-lg-8">
                            <button type="submit" class="btn btn-warning btn-block">Get A New Quote</button>
                        </div>
                        <!-- /.col -->
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xs-6 col-md-4 col-lg-4">

                        </div>
                        <!-- /.col -->
                        <div class="col-xs-12 col-md-8 col-lg-8">
                            <button type="submit" class="btn btn-warning btn-block">Proceed With Renewal</button>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- <div class="row">
                      <div class="col-xs-12">
                        <p class="mt-2 text-center">By click register, I agree to the <a href="#">Tearms and Conditions</a></p>
                      </div>
                    </div> -->
                </form>
            </div>
        </div>
    </section>
    <br>

@endsection

@section('extra-css')

@endsection


@section('extra-js')

@endsection