@extends('layouts.dashboard-layout')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="text-center visible-xs">
            New Business
        </h1>
        <h1 class="hidden-xs">
            New Business
        </h1>
        <ol class="breadcrumb hidden-xs">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">New Business</a></li>
            <!-- <li class="active">Blank page</li> -->
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box box-info">
            <br>
            <div class="box-body new-business-2" >
                <div class="col-md-3">
                    <div class="info-box bg-darkblue">
                        <!--<span class="info-box-icon"><i class="ion ion-model-s"></i></span>-->
                        <p>Motor</p>
                        <!-- /.info-box-content -->
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="info-box bg-darkblue">
                        <p>Renewal</p>
                        <!-- /.info-box-content -->
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="info-box bg-darkblue">
                        <p>Renewal Under Other Broker Code</p>
                        <!-- /.info-box-content -->
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="info-box bg-darkblue">
                        <p>Insurance Company Change</p>
                        <!-- /.info-box-content -->
                    </div>
                </div>

            </div>

        </div>
        <!-- /.box -->

    </section>
    <br>
@endsection

@section('extra-css')
    <style>
        .new-business .info-box-text{
            font-size: 25px;
            margin-top: 22px;
        }
        .new-business-2 p{
            font-size: 16px;
            padding-top: 22px;
            text-align: center;
            color: #fff;
            font-weight: 600;
        }
        .new-business-2 .info-box{
            min-height: 70px !important;
        }
    </style>
@endsection


@section('extra-js')

@endsection