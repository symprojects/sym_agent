function blockUi(message) {
    $.blockUI({
        message: '<img src="/images/gif/loading.gif" style=" width: 40px;"/> <br><h1 style="margin: 0px; font-size: 28px;    padding-top: 10px;">'+message+'</h1>',
        css: {
            backgroundColor: '#fff',
            // 'width': "320px",
            // 'left': '0px !important',
            'color': '#000000',
            'border':0,
            'padding': '20px 0px 25px'

        }
    });
}