<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Lead;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Client;

class CustomerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function addCustomer()
    {
        return view('customers.add-customer');
    }

    public function saveCustomer(Request $request)
    {
        $customer = new Customer($request->all());
        $customer->created_by = Auth::user()->id;
        $res = $customer->save();
        if ($res) {
            return redirect()->to('/customer/business/select/' . $customer->id)->with('success', 'Customer added successfully');

        } else {
            return redirect()->back()->with('error', 'Customer not added');

        }
    }

    public function selectBusiness($id)
    {
        return view('customers.new-business')
            ->with('id', $id);
    }

    public function listCustomer()
    {
        $customers = Customer::Where('created_by',Auth::user()->id)->get();
//        dd($customers);
        return view('customers.customers')
            ->with('customers',$customers);

    }

    public function listBusiness(){
        $customers = Customer::Where('created_by',Auth::user()->id)->get();
        $customers_id = array();
        foreach ($customers as $c){
            array_push($customers_id, $c->id);

        }
        $businesses = Lead::whereIn('user_id',$customers_id)->where('status',1)->with('document')->get();
//        dd($businesses);
        return view('customers.list-businesses')
            ->with('customers',$businesses);
    }

    public function listComplatedBusiness(){
        $customers = Customer::Where('created_by',Auth::user()->id)->get();
        $customers_id = array();
        foreach ($customers as $c){
            array_push($customers_id, $c->id);

        }
        $businesses = Lead::whereIn('user_id',$customers_id)->with('document')->get();
//        dd($businesses);
        return view('customers.list-completed-businesses')
            ->with('customers',$businesses);
    }
}
