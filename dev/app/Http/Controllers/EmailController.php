<?php

namespace App\Http\Controllers;

use App\Mail\SendEmail;
use App\Mail\SendEmailWithAttachment;
use App\Models\LeadQuote;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Barryvdh\DomPDF\Facade as PDF;

class EmailController extends Controller
{
    public function __construct()
    {
//        $this->middleware('auth');
    }
    public function sendEmail(Request $request)
    {
//        dd($request->all());
        $to = '';
        if ($request->type == 'quotes') {

            $to = $request->email;
            $res = Mail::to($request->email)->send(new SendEmail($request->all()));

        } elseif ($request->type == 'quotes_to') {

            $to = $request->email;
            $res = Mail::to($request->email);
            if ($request->cc_email) {
                $to = $to . ',' . $request->cc_email;
                $res->cc(explode(',', $request->cc_email));
            }
            $res->send(new SendEmail($request->all()));

        } elseif ($request->type == 'payment_link') {

            $to = $request->email;
            $res = Mail::to($request->email)->send(new SendEmail($request->all()));

        } elseif ($request->type == 'invoice') {

            $lead_details = json_decode($request->contents);
            $data=['lead_details'=>$lead_details, 'lead_id'=>$request->lead_id,'title'=>$request->lead_id];
            $path = public_path('/invoice/'.$request->lead_id.'.pdf');
            $filepath = asset('').'invoice/'.$request->lead_id.'.pdf';
            $pdf = PDF::loadView('pdf.pdf-template',$data)->save($path);
            $request->merge(['filepath' => $filepath]);
            $request->merge(['path' => $path]);
            $to = $request->email;
            $res = Mail::to($request->email)->send(new SendEmailWithAttachment($request->all()));
            return redirect()->back()->with('success', 'Email Sent to ' . $to);

        }elseif ($request->type == 'invoice_renew') {

            $lead_details = $lead_details = LeadQuote::join('insurance_providers', 'insurance_providers.ins_prov_no', '=', 'lead_quotes.quote_ins_no')
                ->where('id',$request['lead_quote'])
                ->first();
            $data=['lead_details'=>$lead_details, 'lead_id'=>$request->lead_id,'title'=>$request->lead_id];
//            dd($data);
            $path = public_path('/invoice/'.$request->lead_id.'.pdf');
            $filepath = asset('').'invoice/'.$request->lead_id.'.pdf';
            $pdf = PDF::loadView('pdf.pdf-template',$data)->save($path);
            $request->merge(['filepath' => $filepath]);
            $request->merge(['path' => $path]);
            $to = $request->email;
            $res = Mail::to($request->email)->send(new SendEmailWithAttachment($request->all()));
            return redirect()->route('renewSoon')->with('success', 'Email Sent to ' . $to);

        }
        return redirect()->back()->with('email_sent', 'Email Sent to ' . $to);
    }
}
