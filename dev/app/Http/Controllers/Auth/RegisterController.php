<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
//            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
//            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
//        dd($data);
        $img = $this->upload($data);
        return User::create([
            'name' => $data['first_name'].' '. $data['last_name'],
            'nic'=>$data['nic'],
            'mobile'=>$data['mobile'],
            'address'=>$data['address'],
            'profile_image'=>$img,
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    public function upload($request){
        $dir = './uploads/profile_images/';

        $file = $request['imageUpload']; //file field
        $ext = $file->guessExtension(); //get file extenstion
        $name = uniqid() . '.' . $ext; //create file name
        $res = Input::file('imageUpload')->move($dir , $name); //path to save file
        return $name;
//        $request->merge(['file_path' => $dir . '/' . $request->lesson_type . '/' . $name]);
//
    }
}
