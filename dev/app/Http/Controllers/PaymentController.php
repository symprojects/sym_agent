<?php

namespace App\Http\Controllers;

use App\Models\LeadQuote;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function savePaymentType(Request $request){
//        dd($request->all());
        $front_url =$request->front_url;
        if ($request->payment == 'SAMPATH'){
            $payment_link = $front_url .'/sampath?quote_id='.$request->quote_no.'&ins_token='.$request->lead_token.'&pmt_id=0';
        }else if ($request->payment == 'COMBANK'){
            $payment_link = $front_url .'/combank?quote_id='.$request->quote_no.'&ins_token='.$request->lead_token.'&pmt_id=0';
        }else{
            $payment_link = $front_url .'/amex?quote_id='.$request->quote_no.'&ins_token='.$request->lead_token.'&pmt_id=0';
        }
        $res = LeadQuote::where('quote_no',$request->quote_no)->update(['tr_type'=>$request->payment, 'payment_link'=>$payment_link]);
//        dd($res);
        return response()->json(['status'=>$res,'payment_link'=>$payment_link])
            ->header('Access-Control-Allow-Origin', '*')
            ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
    }
}
