<?php

namespace App\Http\Controllers;

use App\Models\Lead;
use App\Models\LeadQuote;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;

class PrintController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function printInvoice(Request $request){
//        dd($request->all());
        $lead_details = json_decode($request->contents);
//        dd($lead_details);

        $data=['lead_details'=>$lead_details, 'lead_id'=>$request->lead_id,'title'=>$request->lead_id];
        $pdf = PDF::loadView('pdf.pdf-template',$data);
        return $pdf->stream($request->lead_id.'.pdf');
    }

    public function printInvoiceRenew(Request $request){
        $lead_details = LeadQuote::join('insurance_providers', 'insurance_providers.ins_prov_no', '=', 'lead_quotes.quote_ins_no')
            ->where('id',$request['lead_quote'])
            ->first();
//        dd($lead_details);
        $data=['lead_details'=>$lead_details, 'lead_id'=>$request->lead_id,'title'=>$request->lead_id];
        $pdf = PDF::loadView('pdf.pdf-template',$data);
        return $pdf->stream($request->lead_id.'.pdf');

    }
}
