<?php

namespace App\Http\Controllers;

use App\Models\Document;
use App\Models\Lead;
use App\Models\LeadQuote;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class DocumentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function customerDocumentsSave(Request $request)
    {
        //dd($request->all());
        $lead_details = Lead::where('main_lead_id',$request->lead_no)->first();
        $res = Document::where('lead_id',$request->lead_no)->first();

        if ($res == null){
            $res = new Document();
            $res->lead_id = $request->lead_no;
        }
        $res->user_id = $lead_details->user_id;
        if (isset($request->vehicle_reg_no)){
            $res->vehicle_reg_no = $request->vehicle_reg_no;
        }
        if (isset($request->chassis_no)){
            $res->chassis_no = $request->chassis_no;
        }
        if (isset($request->engine_no)){
            $res->engine_no = $request->engine_no;
        }
        $res->created_by = Auth::user()->id;

        return response()->json([$res->save()])
            ->header('Access-Control-Allow-Origin', '*')
            ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');

    }

    public function customerDocumentsUpload(Request $request){
//        dd($request->all());

        $res = Document::where('lead_id',$request->lead_id)->first();
        if ($res == null){
            $res = new Document();
            $res->lead_id = $request->lead_id;
        }

        if ($request->field == 'nic'){
            $res->nic = $this->uploadDocs($request,'nic',$request->lead_id);
        }
        if ($request->field == 'vrc'){
            $res->vrc = $this->uploadDocs($request,'vrc',$request->lead_id);
        }
        if ($request->field == 'proposal'){
            $res->proposal = $this->uploadDocs($request,'proposal',$request->lead_id);
        }
        if ($request->field == 'inspection'){
            $res->inspection = $this->uploadDocs($request,'inspection',$request->lead_id);
        }
        if ($request->field == 'loa'){
            $res->loa = $this->uploadDocs($request,'loa',$request->lead_id);
        }
        if ($request->field == 'temp_cover_note'){
            $res->temp_cover_note = $this->uploadDocs($request,'temp_cover_note',$request->lead_id);
        }
        if ($request->field == 'debit_note'){
            $res = LeadQuote::where('quote_no',$request->quote_no)->first();
            $res->debit_note = $this->uploadDocs($request,'debit_note',$request->lead_id);
            $res->save();
        }
        return response()->json([$res->save()])
            ->header('Access-Control-Allow-Origin', '*')
            ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
    }

    private function uploadDocs($request,$field,$lead_id){
        $dir = './uploads/documents/'.$lead_id.'/';
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }
        $file = $request[$field]; //file field
        $ext = $file->guessExtension(); //get file extenstion
        $name = $field.'-'.uniqid() . '.' . $ext; //create file name
        $res = Input::file($field)->move($dir , $name); //path to save file
        return $dir.'/'.$name;
//        $request->merge(['file_path' => $dir . '/' . $request->lesson_type . '/' . $name]);
//
    }
}
