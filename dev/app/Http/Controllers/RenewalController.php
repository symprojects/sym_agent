<?php

namespace App\Http\Controllers;

use App\InsuranceProvider;
use App\Models\Customer;
use App\Models\Lead;
use App\Models\LeadQuote;
use App\Models\LeadQuoteHistory;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RenewalController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function renewSoon()
    {
        $customers = Customer::Where('created_by', Auth::user()->id)->get();
        $customers_id = array();
        foreach ($customers as $c) {
            array_push($customers_id, $c->id);

        }
        $businesses = Lead::whereIn('user_id', $customers_id)
            ->whereBetween('policy_end_date', [Carbon::now()->format('Y-m-d'), Carbon::now()->addDays(30)->format('Y-m-d')])
            ->where('status', 2)
            ->where('leadrenewtype',1)
            ->with('document')->get();
//        dd($businesses);
        return view('renewal.list-renewal-businesses')
            ->with('customers', $businesses);
    }
    public function renewalsComplated()
    {
        $customers = Customer::Where('created_by', Auth::user()->id)->get();
        $customers_id = array();
        foreach ($customers as $c) {
            array_push($customers_id, $c->id);

        }
        $businesses = Lead::whereIn('user_id', $customers_id)
            ->whereBetween('policy_end_date', [Carbon::now()->format('Y-m-d'), Carbon::now()->addDays(30)->format('Y-m-d')])
            ->where('status', 2)
            ->where('leadrenewtype',2)
            ->with('document')->get();
//        dd($businesses);
        return view('renewal.list-renewal-businesses-done')
            ->with('customers', $businesses);
    }
    public function renewSoonByUser($userID)
    {

        $businesses = Lead::where('user_id', $userID)
            ->whereBetween('policy_end_date', [Carbon::now()->format('Y-m-d'), Carbon::now()->addDays(30)->format('Y-m-d')])
            ->where('status', 2)
            ->where('leadrenewtype',1)
            ->with('document')->get();
//        dd($businesses);
        return view('renewal.list-renewal-businesses')
            ->with('customers', $businesses);
    }

    public function RenewBusiness($lead_id)
    {
        $insurance_providers = InsuranceProvider::where('class_published', 1)->get();
//        $leadquote = LeadQuote::where('lead_id',$lead_id)->where('selected',1)->first();
//        dd($leadquote);
        $requests = LeadQuote::join('insurance_providers', 'insurance_providers.ins_prov_no', '=', 'lead_quotes.quote_ins_no')
            ->where('lead_id',$lead_id)->where('selected',1)
            ->where('leadrenewtype',1)
            ->first();
        return view('renewal.view-selected-renew')
            ->with('insurance_providers',$insurance_providers)
            ->with('leadquote',$requests);

    }
    public function RenewBusinessQuote(Request $request)
    {
//        dump($request->all());
        $leadquote = LeadQuote::where('id',$request->leadqoute_id)->first();
        $leadquote->transaction_amount = $request->amount;
        $leadquote->discounted_value = $request->amount;
        $leadquote->quote_value = $request->amount;
        $leadquote->advance_payment = 0;
        $leadquote->balance_payment = 0;
        $leadquote->save();

        $leadquote = LeadQuote::where('id',$request->leadqoute_id)->first();
        $leadquote->reason = 'renewal on '.Carbon::now()->format('Y-m-d H:i:s');
        $count = LeadQuoteHistory::where('id',$request->leadqoute_id)->first();
        if ($count){
            LeadQuoteHistory::where('id',$request->leadqoute_id)->delete();
        }
        $res =LeadQuoteHistory::insert($leadquote->toArray());

        Lead::where('main_lead_id',$request->lead_id)->update(['leadrenewtype'=>2, 'renew_date'=>Carbon::now()->format('Y-m-d')]);
        $insurance_providers = InsuranceProvider::where('class_published', 1)->get();
        $lead_details = Lead::where('main_lead_id', $request->lead_id)->first();
//        $res = LeadQuote::where('quote_no', $request->quote_no)->first();
        $requests = LeadQuote::join('insurance_providers', 'insurance_providers.ins_prov_no', '=', 'lead_quotes.quote_ins_no')
            ->where('id',$request->leadqoute_id)
            ->first();
//
//        dd($requests);
        return view('renewal.view-selected-renew-quote')
            ->with('insurance_providers', $insurance_providers)
            ->with('lead_no', $request->lead_id)
            ->with('lead_qoute', $requests)
            ->with('quote_no', $request->quote_no)
            ->with('requests', $request->all())
            ->with('lead_details', $lead_details);


    }
    public function RenewBusinessQuoteProceed(Request $request)
    {
//        dd($request->all());
        $leadquote = LeadQuote::where('id',$request->leadqoute_id)->first();
        $leadquote->reason = 'renewal on '.Carbon::now()->format('Y-m-d H:i:s');
        $res =LeadQuoteHistory::insert($leadquote->toArray());
        dd($res);
//        $insurance_providers = InsuranceProvider::where('class_published', 1)->get();
//
////        dd($leadquote);
//        return view('renewal.view-selected-renew-quote')
//            ->with('insurance_providers',$insurance_providers)
//            ->with('leadquote',$leadquote);

    }
    public function saveRenewalQuote(Request $request)
    {
        dd($request->all());

    }

    public function RenewBusinessQuoteGet(Request $request){

    }

//$insurance_providers = InsuranceProvider::where('class_published', 1)->get();
//$lead_details = Lead::where('main_lead_id', $request->lead_id)->first();
////        $res = LeadQuote::where('quote_no', $request->quote_no)->first();
//$requests = LeadQuote::join('insurance_providers', 'insurance_providers.ins_prov_no', '=', 'lead_quotes.quote_ins_no')
//->where('lead_quotes.quote_no',$request->quote_no)
//->first();
////
////        dd($requests);
//return view('renewal.view-selected-renew-quote')
//->with('insurance_providers',$insurance_providers)
//->with('lead_no',$request->lead_id)
//->with('lead_qoute', $requests)
//->with('quote_no', $request->quote_no)
//->with('requests', $request->all())
//->with('lead_details',$lead_details);
}
