<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Document;
use App\Models\Lead;
use App\Models\LeadQuote;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class BusinessController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function motorBusiness($id)
    {
        return view('customers.motor-business')
            ->with('id', $id);
    }

    public function addNewVehicle($id)
    {
        return view('customers.add-new-vehical')
            ->with('id', $id);
    }

    public function getCustomerInfo(Request $request)
    {
        $user = Customer::where('id', $request->user_id)->first();
//        dd($request->user_id);

        return response()->json($user, 200);
    }

    public function saveMotorBusinessData(Request $request)
    {
        $form_data = $request->form_data;
        $user_data = $request->user_data;

        $lead = new Lead();

        $lead->user_fname = $user_data['first_name'];
        $lead->user_lname = $user_data['last_name'];
        $lead->user_nic = $user_data['nic'];
        $lead->user_mobile = $user_data['mobile'];
        $lead->user_email = $user_data['email'];
        $lead->purpose = $form_data['purpose'];
        $lead->lead_make_id = $form_data['lead_make'];
        $lead->lead_model_id = $form_data['lead_model'];
        $lead->lead_year = $form_data['lead_year'];
        $lead->lead_fuel_id = $form_data['lead_fueltype'];
        $lead->lead_value = $form_data['vl_value'];
        $lead->lead_no_claim_bonus = $form_data['lead_no_claim_bonus'];
        $lead->agent_repair = $form_data['agent_repair'];
        $lead->leased = $form_data['leased'];
        $lead->user_id = $user_data['id'];
        $lead->main_lead_id = $request->lead_id;

        $lead->purpose_text = $form_data['purpose_text'];
        $lead->lead_make_text = $form_data['lead_make_text'];
        $lead->lead_model_text = $form_data['lead_model_text'];
        $lead->lead_fuel_text = $form_data['lead_fueltype_text'];

        $res = $lead->save();

        return response()->json(['status' => $res, 'id' => $lead->id])
            ->header('Access-Control-Allow-Origin', '*')
            ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');

    }

    public function viewCustomerBusiness($lead_id)
    {
        $business = Lead::where('id', $lead_id)->first();
        $step = $business->step;
        if ($step == 1) {
            return redirect()->route('select_business', ['id' => $business->user_id]);
        } elseif ($step == 2) {
            return redirect()->route('motor-business', ['id' => $business->user_id]);
        } elseif ($step == 3) {
            return redirect()->route('addNewVehicle', ['id' => $business->user_id]);
        } elseif ($step == 5) {
            return redirect()->route('quotationGenerated', ['id' => $business->main_lead_id]);
        } else {
            return view('customers.customer-business-view')
                ->with('business', $business);
        }
//        dd($business);
    }

    public function saveVehicleNumber(Request $request, $save)
    {
        if ($save == 'save') {
            $res = Lead::where('main_lead_id', $request->lead_id)->update(['vehicle_reg_no' => $request->vehi_no]);

            return response()->json(['status' => $res])
                ->header('Access-Control-Allow-Origin', '*')
                ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
        } elseif ($save == 'delete') {
            $res = Lead::where('main_lead_id', $request->lead_id)->update(['vehicle_reg_no' => null]);

            return response()->json(['status' => $res])
                ->header('Access-Control-Allow-Origin', '*')
                ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
        }
    }

    public function quotationGenerate($id)
    {
        return view('customers.quotations');
    }

    public function updateMotorBusinessData(Request $request)
    {
        $form_data = $request->form_data;

        $lead = Lead::where('main_lead_id', $request->lead_id)->first();
        if ($lead) {

            $lead->purpose = $form_data['purpose'];
            $lead->lead_make_id = $form_data['lead_make'];
            $lead->lead_model_id = $form_data['lead_model'];
            $lead->lead_year = $form_data['lead_year'];
            $lead->lead_fuel_id = $form_data['lead_fueltype'];
            $lead->lead_value = $form_data['vl_value'];
            $lead->lead_no_claim_bonus = $form_data['lead_no_claim_bonus'];
            $lead->agent_repair = $form_data['agent_repair'];
            $lead->leased = $form_data['leased'];
            $lead->main_lead_id = $request->lead_id;

            $lead->purpose_text = $form_data['purpose_text'];
            $lead->lead_make_text = $form_data['lead_make_text'];
            $lead->lead_model_text = $form_data['lead_model_text'];
            $lead->lead_fuel_text = $form_data['lead_fueltype_text'];
//            $lead->step = 5;

            $res = $lead->save();

            return response()->json(['status' => $res, 'id' => $lead->id])
                ->header('Access-Control-Allow-Origin', '*')
                ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
        }
    }

    public function quotationGeneratedSave(Request $request)
    {
//        dd($request->all());
        foreach ($request->qoutes as $qoute) {
            $q = new LeadQuote($qoute);
            $q->selected_datetime = Carbon::now()->format('Y-m-d H:i:s');
            $q->save();
        }
    }

    public function quotationGenerated($id)
    {
        if (!Lead::where('main_lead_id', $id)->count()) {
            return redirect()->route('home');
        }
        $lead_details = Lead::where('main_lead_id', $id)->first();
        $lead_details->step = 5;
        $lead_details->save();
        return view('quotes.quotations')
            ->with('lead_details', $lead_details)
            ->with('lead_id', $id);
    }

    public function quotationGeneratedUpdate(Request $request)
    {
//        dd($request->all());
        $q = LeadQuote::where('quote_no', $request->quote_id)->update([
            'quote_value' => $request->amount,
            'transaction_amount' => $request->amount,
            'discounted_value' => $request->discount_amount
        ]);
        return response()->json([$q])
            ->header('Access-Control-Allow-Origin', '*')
            ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
    }

    public function quotationInsurenceSingle($id)
    {
        $res = LeadQuote::where('quote_no', $id)->first();
        $lead_details = Lead::where('main_lead_id', $res->lead_id)->first();
        LeadQuote::where('lead_id', $res->lead_id)->update(['selected' => 0]);
        LeadQuote::where('quote_no', $id)->update(['selected' => 1]);
        return view('quotes.view-selected-insurence')
            ->with('quote_no', $id)
            ->with('lead_details', $lead_details)
            ->with('lead_qoute', $res)
            ->with('lead_no', $res->lead_id);
    }

    public function customerPayments($quote_no, $lead_id)
    {
        $lead_details = Lead::where('main_lead_id', $lead_id)->first();
        $lead_qoute = LeadQuote::where('quote_no', $quote_no)->first();
//        $lead_details->step =
//        dd($lead_qoute);
        $docment_details = Document::where('lead_id', $lead_id)->first();
        return view('quotes.payments')->with('lead_no', $lead_id)
            ->with('lead_no', $lead_id)
            ->with('lead_details', $lead_details)
            ->with('docment_details', $docment_details)
            ->with('lead_qoute', $lead_qoute)
            ->with('quote_no', $quote_no);
    }

    public function customerDocuments($quote_no, $lead_id)
    {
        $lead_details = Lead::where('main_lead_id', $lead_id)->first();
//        $lead_details->step =
        $lead_qoute = LeadQuote::where('quote_no', $quote_no)->first();
        $docment_details = Document::where('lead_id', $lead_id)->first();
//        dd($lead_details);
        return view('quotes.documnets')
            ->with('lead_no', $lead_id)
            ->with('lead_qoute', $lead_qoute)
            ->with('lead_details', $lead_details)
            ->with('docment_details', $docment_details)
            ->with('quote_no', $quote_no);
    }

    public function savePaymentDetails(Request $request)
    {
        $q = LeadQuote::where('quote_no', $request->quote_no)->first();
        $diff = 0;
        if ($request->type == 'policy_start_date') {
//           dd(Carbon::createFromFormat('Y-m-d',$request->value)->format('Y-m-d'));
            $q->policy_start_date = Carbon::parse($request->value, 'Asia/Jayapura')->format('Y-m-d');
            $remainingDays = env('remainingDays', 30);
            $paymentEndDate = Carbon::parse($request->value, 'Asia/Jayapura')->addDays($remainingDays);
            $date = Carbon::parse($paymentEndDate);
            $now = Carbon::now();
            $diff = $date->diffInDays($now);

            $q->days_remaining = $diff;
            $q->payment_end_date = $paymentEndDate;
            $lead = Lead::where('main_lead_id', $q->lead_id)->first();
//            dd($q->lead_id);
//            dd(Carbon::parse($request->value, 'Asia/Jayapura')->addYear(1)->format('Y-m-d'));
            $lead->policy_end_date = Carbon::parse($request->value, 'Asia/Jayapura')->addYear(1)->format('Y-m-d');
            $lead->save();
        }
        if ($request->type == 'advance_paid') {
            $q->advance_payment = $request->value;
        }
        if ($request->type == 'balance_payment') {
            $q->balance_payment = $request->value;
        }
        if ($request->type == 'credit_period') {
            $credit_period = explode('-', $request->value);
            $q->credit_period_start_date = Carbon::parse(trim($credit_period[0]))->format('Y-m-d');
            $q->credit_period_end_date = Carbon::parse(trim($credit_period[1]))->format('Y-m-d');
        }
        if ($request->type == 'days_remaining') {
            $q->days_remaining = $request->value;

        }
        $res = $q->save();
        return response()->json(['status' => $res, 'type' => $request->type, 'days' => $diff])
            ->header('Access-Control-Allow-Origin', '*')
            ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');

    }

    public function calcRemainDate(Request $request)
    {
        $res = LeadQuote::where('quote_no', $request->quote_no)->first();
        $date = Carbon::parse($res->payment_end_date);
        $now = Carbon::now();
        $diff = $date->diffInDays($now);
        return response()->json([$diff])
            ->header('Access-Control-Allow-Origin', '*')
            ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
    }

    public function customerStatusUpdate(Request $request)
    {
        $res = Lead::where('id', $request->id)->first();
        if ($request->element == 'is_potential') {
            if ($res->is_potential == 0) {
                $res->is_potential = 1;
            } else {
                $res->is_potential = 0;
            }
        } elseif ($request->element == 'documents') {
            if ($res->documents == 0) {
                $res->documents = 1;
            } else {
                $res->documents = 0;
            }
        } elseif ($request->element == 'is_paid') {
            if ($res->is_paid == 0) {
                $res->is_paid = 1;
            } else {
                $res->is_paid = 0;
            }
        } elseif ($request->element == 'status') {
            if ($res->status == 2) {
                $res->status = 1;
            } else {
                $res->status = 2;
            }
        }

        return response()->json([$res->save()])
            ->header('Access-Control-Allow-Origin', '*')
            ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
    }

}
