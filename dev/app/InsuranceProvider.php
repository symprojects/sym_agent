<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InsuranceProvider extends Model
{
    protected $table = 'insurance_providers';
}
