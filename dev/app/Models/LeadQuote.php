<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LeadQuote extends Model
{
    protected $table ='lead_quotes';
    protected $fillable = ['lead_id','quote_no','quote_value','quote_ins_no','sms_token','quote_token','merchant_ref_id',
        'bank_reference_id','ipg_transaction_id','transaction_name','transaction_amount','transaction_status',
        'transaction_reason','transaction_failed_reason','sms_verified','tr_type','transaction_type_code','payment',
        'payment_datetime','address_line1','address_line2','address_city','selected','count_down_datetime',
        'discounted_value','selected_datetime','coupon_added','coupon_code','discount_enabled','bank_pmt_id',
        'bank_added','selected_pmt_id','custom_amount','travel_scheme_id','quote_other_enabled','quote_other_id',
        'rpt_file_path','rpt_image_token','rpt_uploaded','rpt_manual_uploaded','quote_cn_file_path','travel_policy_no',
        'card_type','payment_link','advance_payment','debit_note','policy_start_date','balance_payment','credit_period_start_date',
        'credit_period_end_date','days_remaining','payment_end_date','leadrenewtype'];
}
