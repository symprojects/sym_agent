<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $fillable=[
        'lead_id',
        'user_id',
        'vehicle_reg_no',
        'chassis_no',
        'engine_no',
        'vrc',
        'nic',
        'proposal',
        'inspection',
        'loa',
        'temp_cover_note',
        'status',
        'created_by'
    ];

    public function lead()
    {
        return $this->hasOne('App\Models\Lead','lead_id','main_lead_id');
    }
}
