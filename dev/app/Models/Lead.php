<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lead extends Model
{
    protected $fillable = ['user_fname', 'user_lname', 'user_email', 'user_mobile', 'user_nic', 'lead_make_id',
        'lead_model_id', 'lead_fuel_id', 'lead_year', 'lead_value', 'status', 'purpose', 'leased', 'agent_repair',
        'lead_no_claim_bonus', 'user_id', 'purpose_text', 'lead_make_text', 'lead_model_text', 'lead_fuel_text',
        'main_lead_id', 'vehicle_reg_no', 'step', 'status', 'is_potential', 'documents', 'is_paid', 'policy_end_date',
        'leadrenewtype','renew_date'
    ];

    public function document()
    {
        return $this->hasOne('App\Models\Document', 'lead_id', 'main_lead_id');
    }
}
