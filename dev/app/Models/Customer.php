<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable=['first_name','last_name','nic','mobile','email','status','created_by','created_at','updated_at'];
}
