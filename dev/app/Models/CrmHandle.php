<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CrmHandle extends Model
{
    protected $table = 'crm_lead_handles';

    protected $fillable = ['crm_id',
        'crm_lead_id',
        'crm_user_id',
        'crm_call_status',
        'crm_lead_completed',
        'crm_job_close',
        'crm_financial_approval',
        'crm_ins_quote',
        'crm_ins_sub_type',
        'crm_renewal_notice',
        'crm_reminder_code',
        'crm_price_negotiation',
        'crm_vehicle_book',
        'crm_owner_nic',
        'crm_letter_of_authority',
        'crm_co_renewal_notice',
        'crm_cover_notice',
        'crm_collect_form_deliver',
        'crm_email_to_customer',
        'crm_sym_handover_customer',
        'crm_handover_date',
        'crm_customer_signed_form',
        'crm_handedover_date',
        'crm_vehicle_inspection',
        'crm_insurance_confirm_date',
        'crm_customer_confirm_date',
        'crm_credit_period_start_date',
        'crm_credit_period_end_date',
        'crm_payment_customer_promise_date',
        'crm_payment_collected',
        'crm_collected_date',
        'crm_collected_location',
        'crm_collected_remark',
        'crm_paid_insurance',
        'crm_paid_date',
        'crm_paid_branch',
        'crm_collection_insurance',
        'crm_collected_insurance_date',
        'crm_all_payment_docs',
        'crm_photo_copy_to_sym',
        'crm_insurance_card',
        'crm_method',
        'crm_dispatched_date',
        'crm_recieved_date',
        'crm_location',
        'crm_policy_number',
        'crm_af_completed',
        'crm_completed',
        'crm_user',
        'is_active',
        'crm_is_despatch_email'];
}
