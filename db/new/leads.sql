-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 09, 2019 at 01:42 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sym`
--

-- --------------------------------------------------------

--
-- Table structure for table `leads`
--

CREATE TABLE `leads` (
  `id` int(11) NOT NULL,
  `main_lead_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_fname` varchar(100) DEFAULT NULL,
  `user_lname` varchar(100) DEFAULT NULL,
  `user_nic` varchar(100) DEFAULT NULL,
  `user_mobile` varchar(20) DEFAULT NULL,
  `user_email` varchar(100) DEFAULT NULL,
  `purpose` int(11) DEFAULT NULL,
  `purpose_text` varchar(100) DEFAULT NULL,
  `lead_make_id` int(11) DEFAULT NULL,
  `lead_make_text` varchar(100) DEFAULT NULL,
  `lead_model_id` int(11) DEFAULT NULL,
  `lead_model_text` varchar(100) DEFAULT NULL,
  `lead_fuel_id` int(11) DEFAULT NULL,
  `lead_fuel_text` varchar(100) DEFAULT NULL,
  `lead_year` int(11) DEFAULT NULL,
  `lead_value` double DEFAULT NULL,
  `lead_no_claim_bonus` float DEFAULT NULL,
  `agent_repair` int(11) DEFAULT '0',
  `leased` int(11) DEFAULT '0',
  `vehicle_reg_no` varchar(10) DEFAULT NULL,
  `step` int(11) NOT NULL DEFAULT '1' COMMENT '1- select business, 2 - business type, 3 - add details, 4 - view saved business , 5- quote generated',
  `status` int(1) DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `leads`
--

INSERT INTO `leads` (`id`, `main_lead_id`, `user_id`, `user_fname`, `user_lname`, `user_nic`, `user_mobile`, `user_email`, `purpose`, `purpose_text`, `lead_make_id`, `lead_make_text`, `lead_model_id`, `lead_model_text`, `lead_fuel_id`, `lead_fuel_text`, `lead_year`, `lead_value`, `lead_no_claim_bonus`, `agent_repair`, `leased`, `vehicle_reg_no`, `step`, `status`, `created_at`, `updated_at`) VALUES
(7, 4589, 5, 'salinda', 'jayawardana', '911480743v', '0716186394', 'jayawardanasalinda@gmail.com', 3, 'Hiring Dual Purposes', 49, 'HAODA', 2999, 'MO140', 4, 'Electric', 2019, 100000, 10, 1, 1, NULL, 5, 1, '2019-06-03 05:53:22', '2019-06-05 05:18:35');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `leads`
--
ALTER TABLE `leads`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `leads`
--
ALTER TABLE `leads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
