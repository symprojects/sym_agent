-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 11, 2019 at 01:56 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sym`
--

-- --------------------------------------------------------

--
-- Table structure for table `crm_lead_handles`
--

CREATE TABLE `crm_lead_handles` (
  `id` int(11) NOT NULL,
  `crm_lead_id` int(11) NOT NULL,
  `crm_user_id` int(11) DEFAULT NULL,
  `crm_call_status` bit(1) DEFAULT NULL,
  `crm_lead_completed` int(11) DEFAULT NULL,
  `crm_job_close` int(2) DEFAULT NULL,
  `crm_financial_approval` int(11) DEFAULT NULL,
  `crm_ins_quote` int(11) DEFAULT NULL,
  `crm_ins_sub_type` int(11) DEFAULT NULL,
  `crm_renewal_notice` int(11) DEFAULT NULL,
  `crm_reminder_code` varchar(20) DEFAULT NULL,
  `crm_price_negotiation` int(11) DEFAULT NULL,
  `crm_vehicle_book` int(11) DEFAULT NULL,
  `crm_owner_nic` int(11) DEFAULT NULL,
  `crm_letter_of_authority` int(11) DEFAULT NULL,
  `crm_co_renewal_notice` int(11) DEFAULT NULL,
  `crm_other_att` int(11) DEFAULT NULL,
  `crm_cover_notice` int(11) DEFAULT NULL,
  `crm_collect_form_deliver` int(11) DEFAULT NULL,
  `crm_email_to_customer` int(11) DEFAULT NULL,
  `crm_sym_handover_customer` int(11) DEFAULT NULL,
  `crm_handover_date` datetime DEFAULT NULL,
  `crm_customer_signed_form` int(11) DEFAULT NULL,
  `crm_handedover_date` datetime DEFAULT NULL,
  `crm_vehicle_inspection` int(11) DEFAULT NULL,
  `crm_insurance_confirm_date` datetime DEFAULT NULL,
  `crm_customer_confirm_date` datetime DEFAULT NULL,
  `crm_credit_period_start_date` datetime DEFAULT NULL,
  `crm_credit_period_end_date` datetime DEFAULT NULL,
  `crm_payment_customer_promise_date` datetime DEFAULT NULL,
  `crm_payment_collected` int(11) DEFAULT NULL,
  `crm_collected_date` datetime DEFAULT NULL,
  `crm_collected_location` text,
  `crm_paid_insurance` int(11) DEFAULT NULL,
  `crm_paid_date` datetime DEFAULT NULL,
  `crm_paid_branch` text,
  `crm_collection_insurance` int(11) DEFAULT NULL,
  `crm_collected_insurance_date` datetime DEFAULT NULL,
  `crm_all_payment_docs` int(11) DEFAULT NULL,
  `crm_photo_copy_to_sym` int(11) DEFAULT NULL,
  `crm_insurance_card` int(11) DEFAULT NULL,
  `crm_method` int(11) DEFAULT NULL,
  `crm_dispatched_date` datetime DEFAULT NULL,
  `crm_recieved_date` datetime DEFAULT NULL,
  `crm_location` varchar(200) DEFAULT NULL,
  `crm_policy_number` int(11) DEFAULT NULL,
  `crm_af_completed` int(11) DEFAULT NULL,
  `crm_completed` int(11) DEFAULT NULL,
  `crm_user` varchar(50) DEFAULT NULL,
  `is_active` bit(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `crm_is_despatch_email` int(11) DEFAULT NULL,
  `crm_debit_note` int(11) DEFAULT '0',
  `crm_debit_note_date` datetime DEFAULT NULL,
  `crm_financial_approval_date` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(11) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `nic` varchar(15) DEFAULT NULL,
  `mobile` varchar(15) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `first_name`, `last_name`, `nic`, `mobile`, `email`, `status`, `created_by`, `created_at`, `updated_at`) VALUES
(2, 'salinda', 'jayawardana', '123456789v', '716186394', 'jayawardanasalinda@gmail.com', 1, 1, '2019-04-04 12:20:51', '2019-04-04 12:20:51'),
(3, 'Krish', 'nanayakkara', '123456789v', '713882815', 'krish@gmail.com', 1, 1, '2019-04-04 12:40:45', '2019-04-04 12:40:45'),
(4, 'salinda', 'jayawardana', '4521574786', '7161863940', 'jayawardanasalinda@gmail.com', 1, 1, '2019-04-27 09:48:57', '2019-04-27 09:48:57'),
(5, 'salinda', 'jayawardana', '911480743v', '0716186394', 'jayawardanasalinda@gmail.com', 1, 2, '2019-05-23 10:21:31', '2019-05-23 10:21:31'),
(6, 'dhanushka', 'salinda', '123123123v', '0713882515', 'salinda@gmail.com', 1, 2, '2019-06-05 11:57:36', '2019-06-05 11:57:36'),
(7, 'salinda', 'jayawardana', '911480743v', '0713882815', 'salinda.sym@gmail.com', 1, 2, '2019-06-29 11:24:14', '2019-06-29 11:24:14');

-- --------------------------------------------------------

--
-- Table structure for table `documents`
--

CREATE TABLE `documents` (
  `id` int(11) NOT NULL,
  `lead_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `vehicle_reg_no` varchar(100) DEFAULT NULL,
  `chassis_no` varchar(255) DEFAULT NULL,
  `engine_no` varchar(255) DEFAULT NULL,
  `vrc` varchar(255) DEFAULT NULL,
  `nic` varchar(255) DEFAULT NULL,
  `proposal` varchar(255) DEFAULT NULL,
  `inspection` varchar(255) DEFAULT NULL,
  `loa` varchar(255) DEFAULT NULL,
  `temp_cover_note` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `is_potential` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `documents`
--

INSERT INTO `documents` (`id`, `lead_id`, `user_id`, `vehicle_reg_no`, `chassis_no`, `engine_no`, `vrc`, `nic`, `proposal`, `inspection`, `loa`, `temp_cover_note`, `status`, `is_potential`, `created_by`, `created_at`, `updated_at`) VALUES
(3, 4598, 6, 'WPCAO7878', '154254852', '251445220', './uploads/documents/4598//vrc-5d25ccdc8d0f8.png', './uploads/documents/4598//nic-5d25ccfb6f70a.png', './uploads/documents/4598//proposal-5d25cf2ed67ea.png', './uploads/documents/4598//loa-5d26bcc7617bb.jpeg', './uploads/documents/4598//loa-5d26bce3d258f.png', './uploads/documents/4598//temp_cover_note-5d26be48c7862.jpeg', 1, 0, 2, '2019-07-10 01:46:26', '2019-07-10 23:12:48');

-- --------------------------------------------------------

--
-- Table structure for table `leads`
--

CREATE TABLE `leads` (
  `id` int(11) NOT NULL,
  `main_lead_id` int(11) DEFAULT NULL,
  `lead_type` varchar(10) NOT NULL DEFAULT 'v',
  `user_id` int(11) DEFAULT NULL,
  `user_fname` varchar(100) DEFAULT NULL,
  `user_lname` varchar(100) DEFAULT NULL,
  `user_nic` varchar(100) DEFAULT NULL,
  `user_mobile` varchar(20) DEFAULT NULL,
  `user_email` varchar(100) DEFAULT NULL,
  `purpose` int(11) DEFAULT NULL,
  `purpose_text` varchar(100) DEFAULT NULL,
  `lead_make_id` int(11) DEFAULT NULL,
  `lead_make_text` varchar(100) DEFAULT NULL,
  `lead_model_id` int(11) DEFAULT NULL,
  `lead_model_text` varchar(100) DEFAULT NULL,
  `lead_fuel_id` int(11) DEFAULT NULL,
  `lead_fuel_text` varchar(100) DEFAULT NULL,
  `lead_year` int(11) DEFAULT NULL,
  `lead_value` double DEFAULT NULL,
  `lead_no_claim_bonus` float DEFAULT NULL,
  `agent_repair` int(11) DEFAULT '0',
  `leased` int(11) DEFAULT '0',
  `vehicle_reg_no` varchar(10) DEFAULT NULL,
  `step` int(11) NOT NULL DEFAULT '4' COMMENT '1- select business, 2 - business type, 3 - add details, 4 - view saved business , 5- quote generated',
  `status` int(1) DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `leads`
--

INSERT INTO `leads` (`id`, `main_lead_id`, `lead_type`, `user_id`, `user_fname`, `user_lname`, `user_nic`, `user_mobile`, `user_email`, `purpose`, `purpose_text`, `lead_make_id`, `lead_make_text`, `lead_model_id`, `lead_model_text`, `lead_fuel_id`, `lead_fuel_text`, `lead_year`, `lead_value`, `lead_no_claim_bonus`, `agent_repair`, `leased`, `vehicle_reg_no`, `step`, `status`, `created_at`, `updated_at`) VALUES
(4, NULL, 'v', 5, 'salinda', 'jayawardana', '911480743v', '0716186394', 'jayawardanasalinda@gmail.com', 3, NULL, 33, NULL, 462, NULL, 4, NULL, 2017, 45555, 5, 1, 0, NULL, 1, 1, '2019-06-02 10:57:43', '2019-06-02 10:57:43'),
(5, NULL, 'v', 5, 'salinda', 'jayawardana', '911480743v', '0716186394', 'jayawardanasalinda@gmail.com', 1, 'Private Dual Purposes', 35, 'DONG FENG', 465, '499 121Y', 4, 'Electric', 2016, 1000, 40, 1, 0, NULL, 1, 1, '2019-06-02 12:47:30', '2019-06-02 12:47:30'),
(6, NULL, 'v', 5, 'salinda', 'jayawardana', '911480743v', '0716186394', 'jayawardanasalinda@gmail.com', 3, 'Hiring Dual Purposes', 6, 'ASTON MARTIN', 16, 'DB9 COUPE', 3, 'Hybrid', 2016, 100000, NULL, 1, 0, NULL, 1, 1, '2019-06-03 05:22:45', '2019-06-03 05:22:45'),
(7, 4589, 'v', 5, 'salinda', 'jayawardana', '911480743v', '0716186394', 'jayawardanasalinda@gmail.com', 4, 'Hiring Vehicle', 126, 'TOYOTA', 2060, 'AXIO', 2, 'Diesel', 2017, 37000000, 25, 1, 1, NULL, 1, 1, '2019-06-03 05:53:22', '2019-06-05 11:02:50'),
(8, 4590, 'v', 5, 'salinda', 'jayawardana', '911480743v', '0716186394', 'jayawardanasalinda@gmail.com', 6, 'Private Vehicle', 126, 'TOYOTA', 2356, 'HARRIER', 5, 'PHEV', 2019, 14500000, 5, 1, 1, NULL, 1, 1, '2019-06-05 11:33:39', '2019-06-05 11:33:39'),
(9, 4591, 'v', 5, 'salinda', 'jayawardana', '911480743v', '0716186394', 'jayawardanasalinda@gmail.com', 4, 'Hiring Vehicle', 6, 'ASTON MARTIN', 16, 'DB9 COUPE', 4, 'Electric', 2016, 1000, 6, 1, 0, NULL, 1, 1, '2019-06-05 11:50:41', '2019-06-05 11:50:41'),
(10, 4592, 'v', 6, 'dhanushka', 'salinda', '123123123v', '0713882515', 'salinda@gmail.com', 4, 'Hiring Vehicle', 68, 'KAMA', 989, 'KMC 1023S3  CREW CAB', 3, 'Hybrid', 2018, 1500000, 10, 1, 0, NULL, 1, 1, '2019-06-05 11:58:22', '2019-06-05 11:58:22'),
(11, 4593, 'v', 7, 'salinda', 'jayawardana', '911480743v', '0713882815', 'salinda.sym@gmail.com', 6, 'Private Vehicle', 3, 'ALFA ROMEO', 3, '156 SPARK VELOCE', 5, 'PHEV', 2019, 200000, 0, 0, 0, NULL, 1, 1, '2019-06-29 11:25:40', '2019-06-29 11:25:40'),
(12, 4594, 'v', 7, 'salinda', 'jayawardana', '911480743v', '0713882815', 'salinda.sym@gmail.com', 1, 'Private Dual Purposes', 5, 'ASHOK LEYLAND', 15, 'VIKING', 2, 'Diesel', 2015, 150000, 0, 0, 0, NULL, 1, 1, '2019-06-29 12:01:40', '2019-06-29 12:03:23'),
(13, 4595, 'v', 5, 'salinda', 'jayawardana', '911480743v', '0716186394', 'jayawardanasalinda@gmail.com', 3, 'Hiring Dual Purposes', 30, 'DAEWOO', 374, 'NUBIRASXA', 5, 'PHEV', 2018, 150000, 0, 0, 0, NULL, 1, 1, '2019-06-29 12:05:27', '2019-06-29 12:32:22'),
(14, 4596, 'v', 5, 'salinda', 'jayawardana', '911480743v', '0716186394', 'jayawardanasalinda@gmail.com', 3, 'Hiring Dual Purposes', 3, 'ALFA ROMEO', 7, 'GIULIETTA VELOCE SERIES 2', 4, 'Electric', 2019, 150000, 0, 0, 0, 'WPCAO2233', 1, 1, '2019-06-29 20:56:13', '2019-06-29 21:09:59'),
(15, 4597, 'v', 7, 'salinda', 'jayawardana', '911480743v', '0713882815', 'salinda.sym@gmail.com', 1, 'Private Dual Purposes', 4, 'ALTA', 10, 'TYPJ DUMP TRUCK', NULL, 'Fuel Type', 2018, 1458000, 0, 0, 0, NULL, 1, 1, '2019-07-02 09:48:39', '2019-07-02 09:48:39'),
(16, 4598, 'v', 6, 'dhanushka', 'salinda', '123123123v', '0713882515', 'salinda@gmail.com', 4, 'Hiring Vehicle', 5, 'ASHOK LEYLAND', 13, 'DOST LX', 4, 'Electric', 2016, 1542000, 0, 0, 0, 'WPCAO4545', 5, 1, '2019-07-02 12:33:53', '2019-07-02 12:33:53');

-- --------------------------------------------------------

--
-- Table structure for table `lead_quotes`
--

CREATE TABLE `lead_quotes` (
  `id` int(11) NOT NULL,
  `quote_no` int(11) NOT NULL,
  `lead_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `quote_value` double DEFAULT NULL,
  `quote_ins_no` int(11) DEFAULT NULL,
  `sms_token` varchar(10) DEFAULT NULL,
  `quote_token` text NOT NULL,
  `merchant_ref_id` text,
  `bank_reference_id` text,
  `ipg_transaction_id` text,
  `transaction_name` text,
  `transaction_amount` double DEFAULT NULL,
  `transaction_status` varchar(200) DEFAULT NULL,
  `transaction_reason` text,
  `transaction_failed_reason` text,
  `sms_verified` int(11) NOT NULL DEFAULT '0',
  `tr_type` varchar(25) NOT NULL DEFAULT 'AMEX',
  `transaction_type_code` varchar(255) DEFAULT NULL,
  `payment` int(11) NOT NULL DEFAULT '0',
  `payment_datetime` datetime DEFAULT NULL,
  `address_line1` text,
  `address_line2` text,
  `address_city` text,
  `selected` int(11) NOT NULL DEFAULT '0',
  `count_down_datetime` datetime DEFAULT NULL,
  `discounted_value` double NOT NULL DEFAULT '0',
  `selected_datetime` datetime NOT NULL,
  `coupon_added` int(11) NOT NULL DEFAULT '0',
  `coupon_code` varchar(200) DEFAULT NULL,
  `discount_enabled` int(11) NOT NULL DEFAULT '0',
  `bank_pmt_id` int(11) NOT NULL DEFAULT '0',
  `bank_added` int(11) NOT NULL DEFAULT '0',
  `selected_pmt_id` int(11) NOT NULL DEFAULT '0',
  `custom_amount` int(11) NOT NULL DEFAULT '0',
  `travel_scheme_id` int(11) NOT NULL DEFAULT '0',
  `quote_other_enabled` int(11) NOT NULL DEFAULT '0',
  `quote_other_id` int(11) NOT NULL DEFAULT '0',
  `rpt_file_path` text,
  `rpt_image_token` text,
  `rpt_uploaded` int(11) NOT NULL DEFAULT '0',
  `rpt_manual_uploaded` int(11) NOT NULL DEFAULT '0',
  `quote_cn_file_path` text,
  `travel_policy_no` text,
  `card_type` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `lead_quotes`
--

INSERT INTO `lead_quotes` (`id`, `quote_no`, `lead_id`, `created_at`, `updated_at`, `quote_value`, `quote_ins_no`, `sms_token`, `quote_token`, `merchant_ref_id`, `bank_reference_id`, `ipg_transaction_id`, `transaction_name`, `transaction_amount`, `transaction_status`, `transaction_reason`, `transaction_failed_reason`, `sms_verified`, `tr_type`, `transaction_type_code`, `payment`, `payment_datetime`, `address_line1`, `address_line2`, `address_city`, `selected`, `count_down_datetime`, `discounted_value`, `selected_datetime`, `coupon_added`, `coupon_code`, `discount_enabled`, `bank_pmt_id`, `bank_added`, `selected_pmt_id`, `custom_amount`, `travel_scheme_id`, `quote_other_enabled`, `quote_other_id`, `rpt_file_path`, `rpt_image_token`, `rpt_uploaded`, `rpt_manual_uploaded`, `quote_cn_file_path`, `travel_policy_no`, `card_type`) VALUES
(19, 12645, 4598, '2019-07-02 18:04:00', '2019-07-11 05:56:38', 0, 7, NULL, 'wK4IqAOOVknqz5Qhyt56QspBuPFLEAqL', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-03 00:45:22', 0, '2019-07-02 18:04:00', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL),
(20, 12646, 4598, '2019-07-02 18:04:00', '2019-07-11 05:56:38', 0, 1, NULL, 'y8kEzq1HluhSydr2qkUyf69UPMyH0WYG', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-03 01:35:00', 0, '2019-07-02 18:04:00', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL),
(21, 12647, 4598, '2019-07-02 18:04:00', '2019-07-11 05:56:38', 0, 5, NULL, 'WhN2nyVwNcbOwyxkW0pomUMohNCt9QFn', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-03 02:05:00', 0, '2019-07-02 18:04:00', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL),
(22, 12648, 4598, '2019-07-02 18:04:01', '2019-07-11 05:56:38', 0, 14, NULL, 'xKU6d2SgBcAgKWnMkabDvdHcT7wdEx6O', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-03 02:45:22', 0, '2019-07-02 18:04:01', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL),
(23, 12649, 4598, '2019-07-02 18:04:01', '2019-07-11 05:56:38', 0, 9, NULL, 'l1Ze4QldXZvHwJBVLrQC7lAdAJ5mwf6n', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-03 01:35:00', 0, '2019-07-02 18:04:01', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL),
(24, 12650, 4598, '2019-07-02 18:04:01', '2019-07-11 05:56:38', 0, 3, NULL, 'Wi91QYRDVFWqt678zxwjFkM3sdQrNQgd', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-03 02:05:00', 0, '2019-07-02 18:04:01', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL),
(25, 12651, 4598, '2019-07-02 18:04:01', '2019-07-11 05:56:38', 0, 10, NULL, 'o3R4EbCfdcbfMv0Ww0Lu20dJibN96AUF', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-03 01:35:00', 0, '2019-07-02 18:04:01', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL),
(26, 12652, 4598, '2019-07-02 18:04:01', '2019-07-11 05:56:38', 0, 16, NULL, 'AttVQwNSRq9kZIdO4yf8h8gFwS5fUMrl', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 1, '2019-07-03 02:45:22', 0, '2019-07-02 18:04:01', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL),
(27, 12653, 4598, '2019-07-02 18:04:01', '2019-07-11 05:56:38', 500, 2, NULL, 'ywQeRIR1YbIKBq4W4tiOy2SMK5FIx9al', NULL, NULL, NULL, NULL, 500, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-03 03:05:00', 400, '2019-07-02 18:04:01', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nic` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profile_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `nic`, `mobile`, `address`, `password`, `profile_image`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'aaa sasasa', 'salinda@gmail.com', '1111111111', '0713881815', 'asasasasasasasas', '$2y$10$w9ZIHGp6tKKd3uDuFXFaneD43ALiYKS/3ZMBdBte9LyXQLt.xGIgu', '5c8b8f06bb95e.png', 'pBmJyzzSAc2PoyS1DeA6cgNq8GUVqQJOWF5SIkyrd0I57it78kA768jKaeTJ', '2019-03-15 06:09:50', '2019-03-15 06:09:50'),
(2, 'salinda jayawardana', 'jayawardanasalinda@gmail.com', '1231231233', '7161863942', 'NO88, helweeshiyawaththa, narammala', '$2y$10$i1gqojqz4sSU8DyGqDnsn.OQvy4YKyl.tMf92ojk9daPKr/Kdz/mq', '5c9d0cd4c29c9.png', 'v63rLESCgtnBW6vMdxKMlXPHDYuWsKvSAdDT9sbKlgEN6GnAYTos5rGbd5PT', '2019-03-28 12:35:08', '2019-03-28 12:35:08');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `crm_lead_handles`
--
ALTER TABLE `crm_lead_handles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `documents`
--
ALTER TABLE `documents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leads`
--
ALTER TABLE `leads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lead_quotes`
--
ALTER TABLE `lead_quotes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `crm_lead_handles`
--
ALTER TABLE `crm_lead_handles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `documents`
--
ALTER TABLE `documents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `leads`
--
ALTER TABLE `leads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `lead_quotes`
--
ALTER TABLE `lead_quotes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
