-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 03, 2019 at 02:01 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sym`
--

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(11) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `nic` varchar(15) DEFAULT NULL,
  `mobile` varchar(15) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `first_name`, `last_name`, `nic`, `mobile`, `email`, `status`, `created_by`, `created_at`, `updated_at`) VALUES
(2, 'salinda', 'jayawardana', '123456789v', '716186394', 'jayawardanasalinda@gmail.com', 1, 1, '2019-04-04 12:20:51', '2019-04-04 12:20:51'),
(3, 'Krish', 'nanayakkara', '123456789v', '713882815', 'krish@gmail.com', 1, 1, '2019-04-04 12:40:45', '2019-04-04 12:40:45'),
(4, 'salinda', 'jayawardana', '4521574786', '7161863940', 'jayawardanasalinda@gmail.com', 1, 1, '2019-04-27 09:48:57', '2019-04-27 09:48:57'),
(5, 'salinda', 'jayawardana', '911480743v', '0716186394', 'jayawardanasalinda@gmail.com', 1, 2, '2019-05-23 10:21:31', '2019-05-23 10:21:31');

-- --------------------------------------------------------

--
-- Table structure for table `leads`
--

CREATE TABLE `leads` (
  `id` int(11) NOT NULL,
  `main_lead_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_fname` varchar(100) DEFAULT NULL,
  `user_lname` varchar(100) DEFAULT NULL,
  `user_nic` varchar(100) DEFAULT NULL,
  `user_mobile` varchar(20) DEFAULT NULL,
  `user_email` varchar(100) DEFAULT NULL,
  `purpose` int(11) DEFAULT NULL,
  `purpose_text` varchar(100) DEFAULT NULL,
  `lead_make_id` int(11) DEFAULT NULL,
  `lead_make_text` varchar(100) DEFAULT NULL,
  `lead_model_id` int(11) DEFAULT NULL,
  `lead_model_text` varchar(100) DEFAULT NULL,
  `lead_fuel_id` int(11) DEFAULT NULL,
  `lead_fuel_text` varchar(100) DEFAULT NULL,
  `lead_year` int(11) DEFAULT NULL,
  `lead_value` double DEFAULT NULL,
  `lead_no_claim_bonus` float DEFAULT NULL,
  `agent_repair` int(11) DEFAULT '0',
  `leased` int(11) DEFAULT '0',
  `vehicle_reg_no` varchar(10) DEFAULT NULL,
  `status` int(1) DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `leads`
--

INSERT INTO `leads` (`id`, `main_lead_id`, `user_id`, `user_fname`, `user_lname`, `user_nic`, `user_mobile`, `user_email`, `purpose`, `purpose_text`, `lead_make_id`, `lead_make_text`, `lead_model_id`, `lead_model_text`, `lead_fuel_id`, `lead_fuel_text`, `lead_year`, `lead_value`, `lead_no_claim_bonus`, `agent_repair`, `leased`, `vehicle_reg_no`, `status`, `created_at`, `updated_at`) VALUES
(4, NULL, 5, 'salinda', 'jayawardana', '911480743v', '0716186394', 'jayawardanasalinda@gmail.com', 3, NULL, 33, NULL, 462, NULL, 4, NULL, 2017, 45555, 5, 1, 0, NULL, 1, '2019-06-02 10:57:43', '2019-06-02 10:57:43'),
(5, NULL, 5, 'salinda', 'jayawardana', '911480743v', '0716186394', 'jayawardanasalinda@gmail.com', 1, 'Private Dual Purposes', 35, 'DONG FENG', 465, '499 121Y', 4, 'Electric', 2016, 1000, 40, 1, 0, NULL, 1, '2019-06-02 12:47:30', '2019-06-02 12:47:30'),
(6, NULL, 5, 'salinda', 'jayawardana', '911480743v', '0716186394', 'jayawardanasalinda@gmail.com', 3, 'Hiring Dual Purposes', 6, 'ASTON MARTIN', 16, 'DB9 COUPE', 3, 'Hybrid', 2016, 100000, NULL, 1, 0, NULL, 1, '2019-06-03 05:22:45', '2019-06-03 05:22:45'),
(7, 4589, 5, 'salinda', 'jayawardana', '911480743v', '0716186394', 'jayawardanasalinda@gmail.com', 3, 'Hiring Dual Purposes', 49, 'HAODA', 2999, 'MO140', 4, 'Electric', 2019, 100000, 10, 1, 1, NULL, 1, '2019-06-03 05:53:22', '2019-06-03 06:30:51');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nic` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profile_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `nic`, `mobile`, `address`, `password`, `profile_image`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'aaa sasasa', 'salinda@gmail.com', '1111111111', '0713881815', 'asasasasasasasas', '$2y$10$w9ZIHGp6tKKd3uDuFXFaneD43ALiYKS/3ZMBdBte9LyXQLt.xGIgu', '5c8b8f06bb95e.png', 'pBmJyzzSAc2PoyS1DeA6cgNq8GUVqQJOWF5SIkyrd0I57it78kA768jKaeTJ', '2019-03-15 06:09:50', '2019-03-15 06:09:50'),
(2, 'salinda jayawardana', 'jayawardanasalinda@gmail.com', '1231231233', '7161863942', 'NO88, helweeshiyawaththa, narammala', '$2y$10$i1gqojqz4sSU8DyGqDnsn.OQvy4YKyl.tMf92ojk9daPKr/Kdz/mq', '5c9d0cd4c29c9.png', 'v63rLESCgtnBW6vMdxKMlXPHDYuWsKvSAdDT9sbKlgEN6GnAYTos5rGbd5PT', '2019-03-28 12:35:08', '2019-03-28 12:35:08');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leads`
--
ALTER TABLE `leads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `leads`
--
ALTER TABLE `leads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
