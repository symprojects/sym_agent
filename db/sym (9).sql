-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 06, 2019 at 01:36 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sym`
--

-- --------------------------------------------------------

--
-- Table structure for table `crm_lead_handles`
--

CREATE TABLE `crm_lead_handles` (
  `id` int(11) NOT NULL,
  `crm_lead_id` int(11) NOT NULL,
  `crm_user_id` int(11) DEFAULT NULL,
  `crm_call_status` bit(1) DEFAULT NULL,
  `crm_lead_completed` int(11) DEFAULT NULL,
  `crm_job_close` int(2) DEFAULT NULL,
  `crm_financial_approval` int(11) DEFAULT NULL,
  `crm_ins_quote` int(11) DEFAULT NULL,
  `crm_ins_sub_type` int(11) DEFAULT NULL,
  `crm_renewal_notice` int(11) DEFAULT NULL,
  `crm_reminder_code` varchar(20) DEFAULT NULL,
  `crm_price_negotiation` int(11) DEFAULT NULL,
  `crm_vehicle_book` int(11) DEFAULT NULL,
  `crm_owner_nic` int(11) DEFAULT NULL,
  `crm_letter_of_authority` int(11) DEFAULT NULL,
  `crm_co_renewal_notice` int(11) DEFAULT NULL,
  `crm_other_att` int(11) DEFAULT NULL,
  `crm_cover_notice` int(11) DEFAULT NULL,
  `crm_collect_form_deliver` int(11) DEFAULT NULL,
  `crm_email_to_customer` int(11) DEFAULT NULL,
  `crm_sym_handover_customer` int(11) DEFAULT NULL,
  `crm_handover_date` datetime DEFAULT NULL,
  `crm_customer_signed_form` int(11) DEFAULT NULL,
  `crm_handedover_date` datetime DEFAULT NULL,
  `crm_vehicle_inspection` int(11) DEFAULT NULL,
  `crm_insurance_confirm_date` datetime DEFAULT NULL,
  `crm_customer_confirm_date` datetime DEFAULT NULL,
  `crm_credit_period_start_date` datetime DEFAULT NULL,
  `crm_credit_period_end_date` datetime DEFAULT NULL,
  `crm_payment_customer_promise_date` datetime DEFAULT NULL,
  `crm_payment_collected` int(11) DEFAULT NULL,
  `crm_collected_date` datetime DEFAULT NULL,
  `crm_collected_location` text,
  `crm_paid_insurance` int(11) DEFAULT NULL,
  `crm_paid_date` datetime DEFAULT NULL,
  `crm_paid_branch` text,
  `crm_collection_insurance` int(11) DEFAULT NULL,
  `crm_collected_insurance_date` datetime DEFAULT NULL,
  `crm_all_payment_docs` int(11) DEFAULT NULL,
  `crm_photo_copy_to_sym` int(11) DEFAULT NULL,
  `crm_insurance_card` int(11) DEFAULT NULL,
  `crm_method` int(11) DEFAULT NULL,
  `crm_dispatched_date` datetime DEFAULT NULL,
  `crm_recieved_date` datetime DEFAULT NULL,
  `crm_location` varchar(200) DEFAULT NULL,
  `crm_policy_number` int(11) DEFAULT NULL,
  `crm_af_completed` int(11) DEFAULT NULL,
  `crm_completed` int(11) DEFAULT NULL,
  `crm_user` varchar(50) DEFAULT NULL,
  `is_active` bit(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `crm_is_despatch_email` int(11) DEFAULT NULL,
  `crm_debit_note` int(11) DEFAULT '0',
  `crm_debit_note_date` datetime DEFAULT NULL,
  `crm_financial_approval_date` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(11) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `nic` varchar(15) DEFAULT NULL,
  `mobile` varchar(15) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `first_name`, `last_name`, `nic`, `mobile`, `email`, `status`, `created_by`, `created_at`, `updated_at`) VALUES
(2, 'salinda', 'jayawardana', '123456789v', '716186394', 'jayawardanasalinda@gmail.com', 1, 1, '2019-04-04 12:20:51', '2019-04-04 12:20:51'),
(3, 'Krish', 'nanayakkara', '123456789v', '713882815', 'krish@gmail.com', 1, 1, '2019-04-04 12:40:45', '2019-04-04 12:40:45'),
(4, 'salinda', 'jayawardana', '4521574786', '7161863940', 'jayawardanasalinda@gmail.com', 1, 1, '2019-04-27 09:48:57', '2019-04-27 09:48:57'),
(5, 'salinda', 'jayawardana', '911480743v', '0716186394', 'jayawardanasalinda@gmail.com', 1, 2, '2019-05-23 10:21:31', '2019-05-23 10:21:31'),
(6, 'dhanushka', 'salinda', '123123123v', '0713882515', 'salinda@gmail.com', 1, 2, '2019-06-05 11:57:36', '2019-06-05 11:57:36'),
(7, 'salinda', 'jayawardana', '911480743v', '0713882815', 'salinda.sym@gmail.com', 1, 2, '2019-06-29 11:24:14', '2019-06-29 11:24:14'),
(8, 'sachi', 'Perera', '916700675V', '0776564323', 'krishna.satrait@gmail.com', 1, 2, '2019-07-21 08:10:19', '2019-07-21 08:10:19'),
(9, 'happy', 'lakhi', '8ee8476363u', '0773253545', 'ffsr@gmail.com', 1, 2, '2019-07-21 08:54:24', '2019-07-21 08:54:24'),
(10, 'Krish', 'nanayakkara', '916100515V', '0716186231', 'krish.sym@gmail.com', 1, 2, '2019-07-21 10:50:22', '2019-07-21 10:50:22');

-- --------------------------------------------------------

--
-- Table structure for table `documents`
--

CREATE TABLE `documents` (
  `id` int(11) NOT NULL,
  `lead_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `vehicle_reg_no` varchar(100) DEFAULT NULL,
  `chassis_no` varchar(255) DEFAULT NULL,
  `engine_no` varchar(255) DEFAULT NULL,
  `vrc` varchar(255) DEFAULT NULL,
  `nic` varchar(255) DEFAULT NULL,
  `proposal` varchar(255) DEFAULT NULL,
  `inspection` varchar(255) DEFAULT NULL,
  `loa` varchar(255) DEFAULT NULL,
  `temp_cover_note` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `documents`
--

INSERT INTO `documents` (`id`, `lead_id`, `user_id`, `vehicle_reg_no`, `chassis_no`, `engine_no`, `vrc`, `nic`, `proposal`, `inspection`, `loa`, `temp_cover_note`, `status`, `created_by`, `created_at`, `updated_at`) VALUES
(3, 4598, 6, 'WPCAO7878', '154254852554', '251445220', './uploads/documents/4598//vrc-5d25ccdc8d0f8.png', './uploads/documents/4598//nic-5d260143ef521.jpeg', './uploads/documents/4598//proposal-5d25cf2ed67ea.png', './uploads/documents/4598//inspection-5d26015e9f860.jpeg', './uploads/documents/4598//loa-5d340ce7146d0.jpeg', NULL, 1, 2, '2019-07-10 01:46:26', '2019-07-21 01:27:43'),
(5, 4607, 7, 'WPCAO7878', '676767', 'weeeeee', './uploads/documents/4607//vrc-5d341ce41517a.jpeg', './uploads/documents/4607//nic-5d341cedc55de.jpeg', './uploads/documents/4607//proposal-5d341cf752420.jpeg', './uploads/documents/4607//inspection-5d341d4774dc8.jpeg', './uploads/documents/4607//loa-5d341d54498fe.jpeg', './uploads/documents/4607//temp_cover_note-5d341d5c92135.jpeg', 1, 2, '2019-07-21 02:35:56', '2019-07-21 02:59:25'),
(6, 4611, 10, 'CAR7687', '675435', '56r66', './uploads/documents/4611//vrc-5d34918a457bc.jpeg', './uploads/documents/4611//nic-5d34915feaf21.jpeg', './uploads/documents/4611//proposal-5d349165b7dbd.jpeg', './uploads/documents/4611//inspection-5d34916b75ded.jpeg', './uploads/documents/4611//loa-5d34917184187.jpeg', './uploads/documents/4611//temp_cover_note-5d3491797c1ea.jpeg', 1, 2, '2019-07-21 10:52:37', '2019-07-21 10:53:38');

-- --------------------------------------------------------

--
-- Table structure for table `insurance_providers`
--

CREATE TABLE `insurance_providers` (
  `ins_prov_no` int(11) NOT NULL,
  `ins_prov_name` varchar(255) DEFAULT NULL,
  `ins_email` varchar(255) DEFAULT NULL,
  `ins_active` int(11) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `published` int(11) NOT NULL DEFAULT '0',
  `class_name` varchar(50) NOT NULL DEFAULT '',
  `picture` text NOT NULL,
  `class_published` int(11) NOT NULL DEFAULT '0',
  `letter_head` text NOT NULL,
  `rating` double NOT NULL DEFAULT '0',
  `discount_rate` double NOT NULL DEFAULT '0',
  `discount_time_limit` varchar(20) NOT NULL DEFAULT '',
  `ins_reg_name` text NOT NULL,
  `travel_class_published` int(11) NOT NULL DEFAULT '0',
  `inv_pre_code` varchar(50) NOT NULL,
  `ins_add_line1` text NOT NULL,
  `ins_add_line2` text NOT NULL,
  `ins_add_city` text NOT NULL,
  `block_price_match_discount` int(11) NOT NULL DEFAULT '0',
  `is_api_with_pre_calc_travel` int(11) NOT NULL DEFAULT '0',
  `extra_terms_enabled` int(11) NOT NULL DEFAULT '0',
  `ins_marker` text NOT NULL,
  `proposal_form_motor` int(11) NOT NULL DEFAULT '0',
  `proposal_form_travel` int(11) NOT NULL DEFAULT '0',
  `rate_tables` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `insurance_providers`
--

INSERT INTO `insurance_providers` (`ins_prov_no`, `ins_prov_name`, `ins_email`, `ins_active`, `created_at`, `updated_at`, `published`, `class_name`, `picture`, `class_published`, `letter_head`, `rating`, `discount_rate`, `discount_time_limit`, `ins_reg_name`, `travel_class_published`, `inv_pre_code`, `ins_add_line1`, `ins_add_line2`, `ins_add_city`, `block_price_match_discount`, `is_api_with_pre_calc_travel`, `extra_terms_enabled`, `ins_marker`, `proposal_form_motor`, `proposal_form_travel`, `rate_tables`) VALUES
(1, 'Ceylinco', 'apwajith@gmail.com', 1, NULL, '2018-10-09 12:01:31', 1, 'Ceylinco', 'http://bckend.saveyourmonkey.in/uploads/ins/bEPjqFkNV3IzESBj_1.png', 1, 'http://bckend.saveyourmonkey.in/uploads/ins/wt4PBeHTt4ZNsl5Y_lh_1.png', 2.8904944293, 10, ' 02:00:00', 'Ceylinco Insurance PLC Sri Lanka', 0, 'CY', 'Ceylinco House', '69 Janadhipathi Mawatha', 'Colombo 00100', 1, 0, 0, 'http://bckend.saveyourmonkey.in/uploads/ins/7Geq0ueImckTlujL_1.png', 1, 0, ''),
(4, 'Allianz ', 'test@sym.com', 1, '2017-02-24 18:32:45', '2018-01-26 15:29:51', 1, '', 'http://bckend.saveyourmonkey.in/uploads/ins/W9H1c11bSDNZCQRF_4.png', 0, '', 2.78645833334, 0, '03:00:00', 'Allianz Insurance Lanka Limited', 0, '', 'No. 46/10', 'Navam Mawatha', 'Colombo', 0, 0, 0, '', 0, 0, NULL),
(2, 'Sri Lanka Insurance', 'apwajith@gmail.com', 1, NULL, '2018-10-29 09:11:57', 1, 'Srilanka', 'https://bckend.saveyourmonkey.in/uploads/ins/mIDKgBrZ6ktyNXob_2.png', 1, 'https://bckend.saveyourmonkey.in/uploads/ins/q32Vjegkk0FzQcuE_lh_2.png', 3.90643706498, 20, '03:30:00', 'Sri Lanka Insurance', 0, 'SLI', 'Rakshana Mandiraya', '21 Vauxhall St', 'Colombo', 0, 0, 0, 'http://bckend.saveyourmonkey.in/uploads/ins/xJT0GULnxsWUgbYS_2.png', 1, 1, 'slic_rates'),
(3, 'Janashakthi', 'apwajith@gmail.com', 1, '2016-11-17 11:15:37', '2018-03-09 13:53:03', 1, 'Janashakthi', 'http://bckend.saveyourmonkey.in/uploads/ins/rcA5UAGqrKBSikL1_3.png', 0, 'http://bckend.saveyourmonkey.in/uploads/ins/TNSgAVFqkFEw4s2w_lh_3.png', 3, 20, '02:30:00', 'Janashakthi Insurance PLC', 1, 'JN', '55/72 ', 'Vauxhall Ln', 'Colombo 00200', 0, 0, 1, 'http://bckend.saveyourmonkey.in/uploads/ins/MFYnOuxD6WzBz3Pb_3.png', 1, 0, ''),
(5, 'Continental Insurance', 'test1@sym.com', 1, '2017-02-24 18:33:08', '2018-01-26 15:28:09', 1, 'Continental', 'http://bckend.saveyourmonkey.in/uploads/ins/ew6EfpflOYtcPLpR_5.png', 1, 'http://bckend.saveyourmonkey.in/uploads/ins/2aKoYhLPIkQPFhwj_lh_5.png', 1.5, 20, '02:30:00', 'Continental Insurance Lanka Limited', 0, 'CIL', '79', 'Dr. C. W. W. Kannangara Mawatha', 'Colombo', 0, 0, 0, 'http://bckend.saveyourmonkey.in/uploads/ins/2XxndroDvburKvjL_5.png', 0, 0, NULL),
(6, 'AIG', 'test2@sym.com', 1, '2017-02-24 18:33:18', '2017-10-23 16:53:48', 1, '', '', 0, '', 0.5, 0, '', '', 0, '', '', '', '', 0, 0, 0, '', 0, 0, NULL),
(7, 'Amana', 'test4@sym.com', 1, '2017-02-24 18:33:32', '2018-01-29 17:31:02', 1, 'Amana', 'http://bckend.saveyourmonkey.in/uploads/ins/qBJxRS2lsKP0a3WU_7.png', 1, 'http://bckend.saveyourmonkey.in/uploads/ins/Qbs1RFGE10Ch7GAD_lh_7.png', 1.34895833334, 20, '01:10:22', 'Amana Takaful PLC', 1, 'AMN', '660 1/1', 'Galle Road', 'Colombo 00300', 0, 0, 0, 'http://bckend.saveyourmonkey.in/uploads/ins/JDbGYyQPhUvDeoKZ_7.png', 0, 0, 'amana_travel_rates'),
(8, 'Co-Operative', 'test5@sym.com', 1, '2017-02-24 18:33:42', '2018-03-09 11:49:30', 1, 'Coop', 'http://bckend.saveyourmonkey.in/uploads/ins/RwlTABMeaOknrkLi_8.png', 1, 'http://bckend.saveyourmonkey.in/uploads/ins/tT7WWyRkFKXemu8f_lh_8.png', 1.375, 0, ' ', 'Co-Operative Insurance', 1, 'COP', '74/5', 'Grandpass Rd', 'Colombo', 0, 0, 0, 'http://bckend.saveyourmonkey.in/uploads/ins/9a9ygWfBhc8MHzcD_8.png', 1, 1, ''),
(9, 'HNB', 'test6@sym.com', 1, '2017-02-24 18:33:52', '2018-03-09 13:51:44', 1, 'HNB', 'http://bckend.saveyourmonkey.in/uploads/ins/sD3TnVku8o7NhLec_9.png', 1, 'http://bckend.saveyourmonkey.in/uploads/ins/w6JZN5QFtEaqCg8u_lh_9.png', 2.5, 2, '02:00:00', 'HNB Assurance PLC', 0, 'HNB', 'No 10 ', 'Sri Uttarananda Mawatha', 'Colombo', 1, 0, 0, 'http://bckend.saveyourmonkey.in/uploads/ins/VjJzoMZtIEU2Sd5J_9.png', 1, 1, ''),
(10, 'LOLC', 'test7@sym.com', 1, '2017-02-24 18:34:01', '2018-10-18 09:53:12', 1, 'LOLC', 'http://bckend.saveyourmonkey.in/uploads/ins/DEiiSH3UvVbztdZG_10.png', 1, 'http://bckend.saveyourmonkey.in/uploads/ins/S78X5Hkb8JeD5eaR_lh_10.png', 3.125, 20, '02:00:00', 'LOLC General Insurance Ltd', 1, 'LOLC', 'No.481', 'T B Jayah Mawatha', 'Colombo 10', 0, 0, 0, 'http://bckend.saveyourmonkey.in/uploads/ins/iRQ5zDrzMQamQakO_10.png', 0, 0, 'lolc_disc_rates,lolc_rates'),
(11, 'MBSL', 'test8@sym.com', 1, '2017-02-24 18:34:14', '2018-01-26 15:05:12', 1, '', '', 0, '', 2, 0, '03:00:00', 'MBSL Insurance (Pvt) Ltd', 0, '', 'Kew Road', 'Colombo', 'Colombo', 0, 0, 0, '', 0, 0, NULL),
(12, 'NITF', 'test9@sym.com', 1, '2017-02-24 18:34:25', '2017-02-24 18:34:25', 1, '', '', 0, '', 2, 0, '', '', 0, '', '', '', '', 0, 0, 0, '', 0, 0, NULL),
(13, 'People\'s Insurance', 'test10@sym.com', 1, '2017-02-24 18:34:59', '2018-03-09 14:01:04', 1, 'peoples', 'http://bckend.saveyourmonkey.in/uploads/ins/GQwoOYToC2SEyVSk_13.png', 1, '', 2.5, 0, '00:00:00', 'People\'s Insurance PLC', 0, '', 'Sri Sambuddhathva Jayanthi Mawatha', 'Colombo 00500', 'Colombo 00500', 0, 0, 0, '', 1, 0, ''),
(14, 'Fairfirst Insurance', 'test11@sym.com', 1, '2017-02-24 18:35:14', '2018-07-09 15:45:03', 1, 'Union', 'http://bckend.saveyourmonkey.in/uploads/ins/bTK24njZdpMvrseD_14.png', 1, 'http://bckend.saveyourmonkey.in/uploads/ins/QKwpJChs65dOS6f9_lh_14.png', 2, 20, '03:10:22', 'Fairfirst Insurance Limited', 1, 'FFI', 'No. 33', 'St Michael\'s Road', 'Colombo 03', 1, 1, 0, 'http://bckend.saveyourmonkey.in/uploads/ins/D72Ngpry3JDiqHY1_14.png', 1, 1, 'union_purpose_value_range,union_risk_rate'),
(15, 'Asian Alliance', 'test12@sym.com', 1, '2017-02-24 18:35:27', '2018-01-26 15:01:50', 1, ' ', '', 0, '', 2.5, 0, ' ', 'Asian Alliance Insurance PLC', 0, 'AAI', '283', 'R.A De Mel Mawatha', 'Colombo 3.', 0, 0, 0, '', 0, 0, NULL),
(16, 'Orient', 'test14@sym.com', 1, '2017-02-24 18:35:44', '2018-01-29 17:33:18', 1, 'Orient', 'http://bckend.saveyourmonkey.in/uploads/ins/hj6Ca9Efzm6DSgbJ_16.png', 0, 'http://bckend.saveyourmonkey.in/uploads/ins/u8vbLiZCYqI2D4Pn_lh_16.png', 3.30555925926, 4, '03:10:22', 'Orient Insurance', 1, 'ORN', '133 Bauddhaloka Mawatha', 'Colombo 00400', 'Colombo 00400', 0, 0, 0, 'http://bckend.saveyourmonkey.in/uploads/ins/v0VipkhxfxjsE48g_16.png', 0, 0, 'orient_travel_rates'),
(17, 'Test', 'test@saveyourmonkey.com', 0, '2017-04-06 16:25:54', '2017-04-06 18:06:06', 0, '', 'http://bckend.saveyourmonkey.in/uploads/ins/Dp57RPrCFawBBwiz_0.png', 0, 'http://bckend.saveyourmonkey.in/uploads/ins/4DI7B5iiIHtmbErS_lh_17.png', 0, 0, '', '', 0, '', '', '', '', 0, 0, 0, '', 0, 0, NULL),
(18, 'test 1', 'test@test.com', 0, '2017-09-15 14:42:23', '2017-09-15 14:52:59', 1, 'test', '', 0, '', 0, 1, '0', 'test', 0, '', '', '', '', 0, 0, 0, '', 0, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `leads`
--

CREATE TABLE `leads` (
  `id` int(11) NOT NULL,
  `main_lead_id` int(11) DEFAULT NULL,
  `lead_type` varchar(10) NOT NULL DEFAULT 'v',
  `user_id` int(11) DEFAULT NULL,
  `user_fname` varchar(100) DEFAULT NULL,
  `user_lname` varchar(100) DEFAULT NULL,
  `user_nic` varchar(100) DEFAULT NULL,
  `user_mobile` varchar(20) DEFAULT NULL,
  `user_email` varchar(100) DEFAULT NULL,
  `purpose` int(11) DEFAULT NULL,
  `purpose_text` varchar(100) DEFAULT NULL,
  `lead_make_id` int(11) DEFAULT NULL,
  `lead_make_text` varchar(100) DEFAULT NULL,
  `lead_model_id` int(11) DEFAULT NULL,
  `lead_model_text` varchar(100) DEFAULT NULL,
  `lead_fuel_id` int(11) DEFAULT NULL,
  `lead_fuel_text` varchar(100) DEFAULT NULL,
  `lead_year` int(11) DEFAULT NULL,
  `lead_value` double DEFAULT NULL,
  `lead_no_claim_bonus` float DEFAULT NULL,
  `agent_repair` int(11) DEFAULT '0',
  `leased` int(11) DEFAULT '0',
  `vehicle_reg_no` varchar(10) DEFAULT NULL,
  `step` int(11) NOT NULL DEFAULT '4' COMMENT '1- select business, 2 - business type, 3 - add details, 4 - view saved business , 5- quote generated',
  `status` int(1) DEFAULT '1' COMMENT '1 - pending , 2 - Completed',
  `is_potential` int(11) NOT NULL DEFAULT '0',
  `documents` int(11) NOT NULL DEFAULT '0',
  `is_paid` int(11) NOT NULL DEFAULT '0',
  `policy_end_date` date DEFAULT NULL,
  `leadrenewtype` int(11) NOT NULL DEFAULT '1' COMMENT '1 new 2 renew',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `leads`
--

INSERT INTO `leads` (`id`, `main_lead_id`, `lead_type`, `user_id`, `user_fname`, `user_lname`, `user_nic`, `user_mobile`, `user_email`, `purpose`, `purpose_text`, `lead_make_id`, `lead_make_text`, `lead_model_id`, `lead_model_text`, `lead_fuel_id`, `lead_fuel_text`, `lead_year`, `lead_value`, `lead_no_claim_bonus`, `agent_repair`, `leased`, `vehicle_reg_no`, `step`, `status`, `is_potential`, `documents`, `is_paid`, `policy_end_date`, `leadrenewtype`, `created_at`, `updated_at`) VALUES
(4, NULL, 'v', 5, 'salinda', 'jayawardana', '911480743v', '0716186394', 'jayawardanasalinda@gmail.com', 3, NULL, 33, NULL, 462, NULL, 4, NULL, 2017, 45555, 5, 1, 0, NULL, 1, 2, 1, 1, 1, NULL, 1, '2019-06-02 10:57:43', '2019-07-28 08:09:16'),
(5, NULL, 'v', 5, 'salinda', 'jayawardana', '911480743v', '0716186394', 'jayawardanasalinda@gmail.com', 1, 'Private Dual Purposes', 35, 'DONG FENG', 465, '499 121Y', 4, 'Electric', 2016, 1000, 40, 1, 0, NULL, 1, 2, 1, 1, 1, NULL, 1, '2019-06-02 12:47:30', '2019-07-28 08:09:52'),
(6, NULL, 'v', 5, 'salinda', 'jayawardana', '911480743v', '0716186394', 'jayawardanasalinda@gmail.com', 3, 'Hiring Dual Purposes', 6, 'ASTON MARTIN', 16, 'DB9 COUPE', 3, 'Hybrid', 2016, 100000, NULL, 1, 0, NULL, 1, 2, 1, 1, 1, NULL, 1, '2019-06-03 05:22:45', '2019-07-28 08:11:10'),
(7, 4589, 'v', 5, 'salinda', 'jayawardana', '911480743v', '0716186394', 'jayawardanasalinda@gmail.com', 4, 'Hiring Vehicle', 126, 'TOYOTA', 2060, 'AXIO', 2, 'Diesel', 2017, 37000000, 25, 1, 1, NULL, 1, 2, 0, 0, 0, NULL, 1, '2019-06-03 05:53:22', '2019-07-28 08:11:19'),
(8, 4590, 'v', 5, 'salinda', 'jayawardana', '911480743v', '0716186394', 'jayawardanasalinda@gmail.com', 6, 'Private Vehicle', 126, 'TOYOTA', 2356, 'HARRIER', 5, 'PHEV', 2019, 14500000, 5, 1, 1, NULL, 1, 1, 0, 1, 0, NULL, 1, '2019-06-05 11:33:39', '2019-07-28 08:11:29'),
(9, 4591, 'v', 5, 'salinda', 'jayawardana', '911480743v', '0716186394', 'jayawardanasalinda@gmail.com', 4, 'Hiring Vehicle', 6, 'ASTON MARTIN', 16, 'DB9 COUPE', 4, 'Electric', 2016, 1000, 6, 1, 0, NULL, 1, 1, 1, 0, 1, NULL, 1, '2019-06-05 11:50:41', '2019-07-28 08:09:05'),
(10, 4592, 'v', 6, 'dhanushka', 'salinda', '123123123v', '0713882515', 'salinda@gmail.com', 4, 'Hiring Vehicle', 68, 'KAMA', 989, 'KMC 1023S3  CREW CAB', 3, 'Hybrid', 2018, 1500000, 10, 1, 0, NULL, 1, 1, 0, 0, 0, NULL, 1, '2019-06-05 11:58:22', '2019-06-05 11:58:22'),
(11, 4593, 'v', 7, 'salinda', 'jayawardana', '911480743v', '0713882815', 'salinda.sym@gmail.com', 6, 'Private Vehicle', 3, 'ALFA ROMEO', 3, '156 SPARK VELOCE', 5, 'PHEV', 2019, 200000, 0, 0, 0, NULL, 1, 1, 0, 0, 0, NULL, 1, '2019-06-29 11:25:40', '2019-06-29 11:25:40'),
(12, 4594, 'v', 7, 'salinda', 'jayawardana', '911480743v', '0713882815', 'salinda.sym@gmail.com', 1, 'Private Dual Purposes', 5, 'ASHOK LEYLAND', 15, 'VIKING', 2, 'Diesel', 2015, 150000, 0, 0, 0, NULL, 1, 1, 0, 0, 0, NULL, 1, '2019-06-29 12:01:40', '2019-06-29 12:03:23'),
(13, 4595, 'v', 5, 'salinda', 'jayawardana', '911480743v', '0716186394', 'jayawardanasalinda@gmail.com', 3, 'Hiring Dual Purposes', 30, 'DAEWOO', 374, 'NUBIRASXA', 5, 'PHEV', 2018, 150000, 0, 0, 0, NULL, 1, 1, 0, 0, 0, NULL, 1, '2019-06-29 12:05:27', '2019-06-29 12:32:22'),
(14, 4596, 'v', 5, 'salinda', 'jayawardana', '911480743v', '0716186394', 'jayawardanasalinda@gmail.com', 3, 'Hiring Dual Purposes', 3, 'ALFA ROMEO', 7, 'GIULIETTA VELOCE SERIES 2', 4, 'Electric', 2019, 150000, 0, 0, 0, 'WPCAO2233', 1, 1, 0, 0, 0, NULL, 1, '2019-06-29 20:56:13', '2019-06-29 21:09:59'),
(15, 4597, 'v', 7, 'salinda', 'jayawardana', '911480743v', '0713882815', 'salinda.sym@gmail.com', 1, 'Private Dual Purposes', 4, 'ALTA', 10, 'TYPJ DUMP TRUCK', NULL, 'Fuel Type', 2018, 1458000, 0, 0, 0, NULL, 1, 1, 0, 0, 0, NULL, 1, '2019-07-02 09:48:39', '2019-07-02 09:48:39'),
(16, 4598, 'v', 6, 'dhanushka', 'salinda', '123123123v', '0713882515', 'salinda@gmail.com', 4, 'Hiring Vehicle', 5, 'ASHOK LEYLAND', 13, 'DOST LX', 4, 'Electric', 2016, 1542000, 0, 0, 0, 'WPCAO4545', 5, 1, 0, 0, 0, '2020-08-30', 1, '2019-07-02 12:33:53', '2019-07-28 09:31:33'),
(17, 4607, 'v', 7, 'salinda', 'jayawardana', '911480743v', '0713882815', 'salinda.sym@gmail.com', 3, 'Hiring Dual Purposes', 5, 'ASHOK LEYLAND', 13, 'DOST LX', 1, 'Petrol', 2017, 8000000, NULL, 0, 0, NULL, 4, 1, 0, 0, 0, NULL, 1, '2019-07-21 02:04:00', '2019-07-21 02:04:00'),
(18, 4608, 'v', 8, 'sachi', 'Perera', '916700675V', '0776564323', 'krishna.satrait@gmail.com', 4, 'Hiring Vehicle', 122, 'SUZUKI', 1873, 'WAGON R', 3, 'Hybrid', 2016, 400000, 0, 0, 0, NULL, 4, 1, 0, 0, 0, NULL, 1, '2019-07-21 08:11:05', '2019-07-21 08:51:53'),
(19, 4609, 'v', 8, 'sachi', 'Perera', '916700675V', '0776564323', 'krishna.satrait@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2016, NULL, NULL, 0, 0, NULL, 3, 1, 0, 0, 0, NULL, 1, '2019-07-21 08:46:13', '2019-07-21 08:46:13'),
(20, 4610, 'v', 9, 'happy', 'lakhi', '8ee8476363u', '0773253545', 'ffsr@gmail.com', 6, 'Private Vehicle', 126, 'TOYOTA', 2062, 'AQUA', 3, 'Hybrid', 2017, 3500000, NULL, 0, 0, NULL, 4, 1, 0, 0, 0, NULL, 1, '2019-07-21 08:55:08', '2019-07-21 08:55:08'),
(21, 4611, 'v', 10, 'Krish', 'nanayakkara', '916100515V', '0716186231', 'krish.sym@gmail.com', 1, 'Private Dual Purposes', 126, 'TOYOTA', 2062, 'AQUA', 4, 'Electric', 2016, 3100000, NULL, 1, 0, NULL, 5, 2, 0, 0, 0, '2019-08-14', 2, '2019-07-21 10:51:13', '2019-08-06 06:04:33'),
(22, 4613, 'v', 5, 'salinda', 'jayawardana', '911480743v', '0716186394', 'jayawardanasalinda@gmail.com', 6, 'Private Vehicle', 126, 'TOYOTA', 2060, 'AXIO', NULL, 'Fuel Type', 2017, 4500000, NULL, 1, 0, NULL, 5, 2, 0, 0, 0, '2019-08-28', 2, '2019-07-21 11:11:51', '2019-08-06 06:05:23');

-- --------------------------------------------------------

--
-- Table structure for table `lead_quotes`
--

CREATE TABLE `lead_quotes` (
  `id` int(11) NOT NULL,
  `quote_no` int(11) NOT NULL,
  `lead_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `quote_value` double DEFAULT NULL,
  `quote_ins_no` int(11) DEFAULT NULL,
  `sms_token` varchar(10) DEFAULT NULL,
  `quote_token` text NOT NULL,
  `merchant_ref_id` text,
  `bank_reference_id` text,
  `ipg_transaction_id` text,
  `transaction_name` text,
  `transaction_amount` double DEFAULT NULL,
  `transaction_status` varchar(200) DEFAULT NULL,
  `transaction_reason` text,
  `transaction_failed_reason` text,
  `sms_verified` int(11) NOT NULL DEFAULT '0',
  `tr_type` varchar(25) NOT NULL DEFAULT 'AMEX',
  `transaction_type_code` varchar(255) DEFAULT NULL,
  `payment` int(11) NOT NULL DEFAULT '0',
  `payment_datetime` datetime DEFAULT NULL,
  `address_line1` text,
  `address_line2` text,
  `address_city` text,
  `selected` int(11) NOT NULL DEFAULT '0',
  `count_down_datetime` datetime DEFAULT NULL,
  `discounted_value` double NOT NULL DEFAULT '0',
  `selected_datetime` datetime NOT NULL,
  `coupon_added` int(11) NOT NULL DEFAULT '0',
  `coupon_code` varchar(200) DEFAULT NULL,
  `discount_enabled` int(11) NOT NULL DEFAULT '0',
  `bank_pmt_id` int(11) NOT NULL DEFAULT '0',
  `bank_added` int(11) NOT NULL DEFAULT '0',
  `selected_pmt_id` int(11) NOT NULL DEFAULT '0',
  `custom_amount` int(11) NOT NULL DEFAULT '0',
  `travel_scheme_id` int(11) NOT NULL DEFAULT '0',
  `quote_other_enabled` int(11) NOT NULL DEFAULT '0',
  `quote_other_id` int(11) NOT NULL DEFAULT '0',
  `rpt_file_path` text,
  `rpt_image_token` text,
  `rpt_uploaded` int(11) NOT NULL DEFAULT '0',
  `rpt_manual_uploaded` int(11) NOT NULL DEFAULT '0',
  `quote_cn_file_path` text,
  `travel_policy_no` text,
  `card_type` varchar(50) DEFAULT NULL,
  `payment_link` varchar(255) DEFAULT NULL,
  `advance_payment` double DEFAULT '0',
  `debit_note` varchar(255) DEFAULT NULL,
  `policy_start_date` date DEFAULT NULL,
  `balance_payment` double DEFAULT '0',
  `credit_period_start_date` datetime DEFAULT NULL,
  `credit_period_end_date` datetime DEFAULT NULL,
  `days_remaining` int(11) DEFAULT '0',
  `payment_end_date` date DEFAULT NULL,
  `leadrenewtype` int(11) NOT NULL DEFAULT '1' COMMENT '1 new, 2 renew'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `lead_quotes`
--

INSERT INTO `lead_quotes` (`id`, `quote_no`, `lead_id`, `created_at`, `updated_at`, `quote_value`, `quote_ins_no`, `sms_token`, `quote_token`, `merchant_ref_id`, `bank_reference_id`, `ipg_transaction_id`, `transaction_name`, `transaction_amount`, `transaction_status`, `transaction_reason`, `transaction_failed_reason`, `sms_verified`, `tr_type`, `transaction_type_code`, `payment`, `payment_datetime`, `address_line1`, `address_line2`, `address_city`, `selected`, `count_down_datetime`, `discounted_value`, `selected_datetime`, `coupon_added`, `coupon_code`, `discount_enabled`, `bank_pmt_id`, `bank_added`, `selected_pmt_id`, `custom_amount`, `travel_scheme_id`, `quote_other_enabled`, `quote_other_id`, `rpt_file_path`, `rpt_image_token`, `rpt_uploaded`, `rpt_manual_uploaded`, `quote_cn_file_path`, `travel_policy_no`, `card_type`, `payment_link`, `advance_payment`, `debit_note`, `policy_start_date`, `balance_payment`, `credit_period_start_date`, `credit_period_end_date`, `days_remaining`, `payment_end_date`, `leadrenewtype`) VALUES
(19, 12645, 4598, '2019-07-02 18:04:00', '2019-08-06 10:33:06', 0, 7, NULL, 'wK4IqAOOVknqz5Qhyt56QspBuPFLEAqL', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-03 00:45:22', 0, '2019-07-02 18:04:00', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(20, 12646, 4598, '2019-07-02 18:04:00', '2019-08-06 10:33:06', 0, 1, NULL, 'y8kEzq1HluhSydr2qkUyf69UPMyH0WYG', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-03 01:35:00', 0, '2019-07-02 18:04:00', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(21, 12647, 4598, '2019-07-02 18:04:00', '2019-08-06 10:33:06', 0, 5, NULL, 'WhN2nyVwNcbOwyxkW0pomUMohNCt9QFn', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-03 02:05:00', 0, '2019-07-02 18:04:00', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(22, 12648, 4598, '2019-07-02 18:04:01', '2019-08-06 10:33:06', 0, 14, NULL, 'xKU6d2SgBcAgKWnMkabDvdHcT7wdEx6O', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-03 02:45:22', 0, '2019-07-02 18:04:01', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(23, 12649, 4598, '2019-07-02 18:04:01', '2019-08-06 10:33:06', 0, 9, NULL, 'l1Ze4QldXZvHwJBVLrQC7lAdAJ5mwf6n', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-03 01:35:00', 0, '2019-07-02 18:04:01', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(24, 12650, 4598, '2019-07-02 18:04:01', '2019-08-06 10:33:06', 0, 3, NULL, 'Wi91QYRDVFWqt678zxwjFkM3sdQrNQgd', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-03 02:05:00', 0, '2019-07-02 18:04:01', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(25, 12651, 4598, '2019-07-02 18:04:01', '2019-08-06 10:33:06', 0, 10, NULL, 'o3R4EbCfdcbfMv0Ww0Lu20dJibN96AUF', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-03 01:35:00', 0, '2019-07-02 18:04:01', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(26, 12652, 4598, '2019-07-02 18:04:01', '2019-08-06 10:33:06', 0, 16, NULL, 'AttVQwNSRq9kZIdO4yf8h8gFwS5fUMrl', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-03 02:45:22', 0, '2019-07-02 18:04:01', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(27, 12653, 4598, '2019-07-02 18:04:01', '2019-08-06 10:33:06', 25, 2, NULL, 'ywQeRIR1YbIKBq4W4tiOy2SMK5FIx9al', NULL, NULL, NULL, NULL, 25, NULL, NULL, NULL, 0, 'SAMPATH', NULL, 0, NULL, NULL, NULL, NULL, 1, '2019-07-03 03:05:00', 20, '2019-07-02 18:04:01', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, 'https://insure.lk/sampath?quote_id=12653&ins_token=PxAxwUvMPeYtUT98fVsUwxdY6L39S9UI&pmt_id=0', 0, './uploads/documents/4598//debit_note-5d33ff7a4ede9.jpeg', '2019-08-30', 1200, '2019-07-18 00:00:00', '2019-07-31 00:00:00', 62, '2019-09-29', 1),
(28, 12726, 4607, '2019-07-21 07:34:05', '2019-07-21 13:38:41', 0, 7, NULL, '5b2s9XwlFYgCWGFLBP9yiAAQOfNOLMAI', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-21 14:15:28', 0, '2019-07-21 07:34:05', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 1),
(29, 12727, 4607, '2019-07-21 07:34:05', '2019-07-21 13:38:41', 0, 1, NULL, 'U3uPCSHKh8yWWBoEAE2YwhahmGQ1V3el', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-21 15:05:06', 0, '2019-07-21 07:34:05', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 1),
(30, 12728, 4607, '2019-07-21 07:34:05', '2019-07-21 13:38:41', 0, 5, NULL, 'rGX1yh8NGLrFs8RtGblQiE1qlDUTRtr5', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-21 15:35:06', 0, '2019-07-21 07:34:05', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 1),
(31, 12729, 4607, '2019-07-21 07:34:05', '2019-07-21 13:38:41', 0, 14, NULL, 'ClKVoTvXij0zTTSJYOjrUZ6cgFzzii24', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-21 16:15:28', 0, '2019-07-21 07:34:05', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 1),
(32, 12730, 4607, '2019-07-21 07:34:05', '2019-07-21 13:38:41', 0, 9, NULL, 'TTmYD1OzvpFgiKQqUZeh2pQyX62lEDow', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-21 15:05:06', 0, '2019-07-21 07:34:05', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 1),
(33, 12731, 4607, '2019-07-21 07:34:05', '2019-07-21 13:38:41', 0, 3, NULL, 'LtGZgxGg3BHm0nc64YYuHP0cgz5f71jB', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-21 15:35:06', 0, '2019-07-21 07:34:05', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 1),
(34, 12732, 4607, '2019-07-21 07:34:05', '2019-07-21 13:38:41', 0, 10, NULL, 'RkwOxv0uDyeRtYB7m2Z12UULSqf48Yr1', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-21 15:05:06', 0, '2019-07-21 07:34:05', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 1),
(35, 12733, 4607, '2019-07-21 07:34:05', '2019-07-21 13:38:41', 50000, 16, NULL, 'shdpakVBdyyPAVmwhAPcImDcitu24jOo', NULL, NULL, NULL, NULL, 50000, NULL, NULL, NULL, 0, 'SAMPATH', NULL, 0, NULL, NULL, NULL, NULL, 1, '2019-07-21 16:15:28', 50000, '2019-07-21 07:34:05', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, 'https://insure.lk/sampath?quote_id=12733&ins_token=O3VvONnW79CnwFHCw9R0Elic1OfWtkrN&pmt_id=0', 0, './uploads/documents/4607//debit_note-5d341f19e91d2.jpeg', '2019-02-10', 0, NULL, NULL, 0, NULL, 1),
(36, 12734, 4607, '2019-07-21 07:34:05', '2019-07-21 13:38:41', 0, 2, NULL, 'WoaBqnh3AKWsDP6g7aMqjVvburoQL9cW', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-21 16:35:06', 0, '2019-07-21 07:34:05', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 1),
(37, 12735, 4608, '2019-07-21 14:22:03', '2019-07-21 14:22:03', 0, 7, NULL, '16fsWZB4bsDhE4YrV7PjtkPq5IHHor9W', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-21 21:03:27', 0, '2019-07-21 14:22:03', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 1),
(38, 12736, 4608, '2019-07-21 14:22:03', '2019-07-21 14:22:03', 0, 1, NULL, 'wna4QgSaKWyiVlBPGrjO2QrjMPEBP1pt', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-21 21:53:05', 0, '2019-07-21 14:22:03', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 1),
(39, 12737, 4608, '2019-07-21 14:22:03', '2019-07-21 14:22:03', 0, 5, NULL, '8W54tubrN1XpBlSP36foUw4rhDxN7nJ7', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-21 22:23:05', 0, '2019-07-21 14:22:03', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 1),
(40, 12738, 4608, '2019-07-21 14:22:04', '2019-07-21 14:22:04', 0, 14, NULL, 'kYYLNqPVsOgOvVw1vAxhRnnbLhxEOq2l', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-21 23:03:27', 0, '2019-07-21 14:22:04', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 1),
(41, 12739, 4608, '2019-07-21 14:22:04', '2019-07-21 14:22:04', 0, 9, NULL, 'uTL18dEOjaNGeCHsHdaWJXaumt3GXyKW', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-21 21:53:05', 0, '2019-07-21 14:22:04', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 1),
(42, 12740, 4608, '2019-07-21 14:22:04', '2019-07-21 14:22:04', 0, 3, NULL, 'AWH5fkvqWLkeIcTYgMfnJqcg6KUj22ni', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-21 22:23:05', 0, '2019-07-21 14:22:04', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 1),
(43, 12741, 4608, '2019-07-21 14:22:04', '2019-07-21 14:22:04', 0, 10, NULL, 'W1flcKdvRt5BqWuayZn2TGLu5UYpq33K', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-21 21:53:05', 0, '2019-07-21 14:22:04', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 1),
(44, 12742, 4608, '2019-07-21 14:22:04', '2019-07-21 14:22:04', 0, 16, NULL, 'HMQS5rzUomk5CwctsicrzsBD5I9eoADi', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-21 23:03:27', 0, '2019-07-21 14:22:04', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 1),
(45, 12743, 4608, '2019-07-21 14:22:04', '2019-07-21 14:22:04', 0, 2, NULL, 'XDyAdET6JkOfSpimkFB0Svd3jgtgklmz', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-21 23:23:05', 0, '2019-07-21 14:22:04', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 1),
(46, 12744, 4610, '2019-07-21 14:25:13', '2019-07-21 14:26:37', 0, 7, NULL, '9OVD2NA3ralbpaQZa0gERNPoTb98hEZd', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-21 21:06:36', 0, '2019-07-21 14:25:13', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 1),
(47, 12745, 4610, '2019-07-21 14:25:13', '2019-07-21 14:26:37', 0, 1, NULL, '99H5s4t8XylpzwkI0itGG5IAWhTogkKK', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-21 21:56:14', 0, '2019-07-21 14:25:13', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 1),
(48, 12746, 4610, '2019-07-21 14:25:13', '2019-07-21 14:26:37', 0, 5, NULL, 'D4FfhdYcawQAb5fyWXDsQCNArWyz8lJg', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-21 22:26:14', 0, '2019-07-21 14:25:13', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 1),
(49, 12747, 4610, '2019-07-21 14:25:13', '2019-07-21 16:19:31', 24000, 14, NULL, 'RgYuaJOp6ZiCuRutDVN11J19RcgXEuqo', NULL, NULL, NULL, NULL, 24000, NULL, NULL, NULL, 0, 'SAMPATH', NULL, 0, NULL, NULL, NULL, NULL, 1, '2019-07-21 23:06:36', 24000, '2019-07-21 14:25:13', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, 'https://insure.lk/sampath?quote_id=12747&ins_token=IYBq9f4ULienv5GBCEma6YgbPufjcuYn&pmt_id=0', 150, NULL, '2019-08-01', 23850, '2019-07-10 00:00:00', '2019-07-12 00:00:00', 40, '2019-08-31', 1),
(50, 12748, 4610, '2019-07-21 14:25:13', '2019-07-21 14:26:37', 0, 9, NULL, 'po8CIJ4te4jH8siAaXhjWdo3Zmugwu8p', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-21 21:56:14', 0, '2019-07-21 14:25:13', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 1),
(51, 12749, 4610, '2019-07-21 14:25:13', '2019-07-21 14:26:37', 0, 3, NULL, 'ca0gMjGESTWJCXC3l4mgrVwGTneerItk', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-21 22:26:14', 0, '2019-07-21 14:25:13', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 1),
(52, 12750, 4610, '2019-07-21 14:25:13', '2019-07-21 14:26:37', 0, 10, NULL, 'UziRIkJ97muTprTUk5hp8AdGwwNjNR88', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-21 21:56:14', 0, '2019-07-21 14:25:13', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 1),
(53, 12751, 4610, '2019-07-21 14:25:13', '2019-07-21 14:26:37', 0, 16, NULL, 'XYydpLO8849EjOSpdcxMWCYXdIUx4kCk', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-21 23:06:36', 0, '2019-07-21 14:25:13', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 1),
(54, 12752, 4610, '2019-07-21 14:25:13', '2019-07-21 14:26:37', 0, 2, NULL, 'ue2Ans93BoDN7QJHDohMFQeXAR4tgE9p', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-21 23:26:14', 0, '2019-07-21 14:25:13', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 1),
(55, 12753, 4611, '2019-07-21 16:21:18', '2019-07-21 16:22:16', 0, 7, NULL, 'yczLWMbksqtI4fWVtgR0EyYePJnpP6iF', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-21 23:02:42', 0, '2019-07-21 16:21:18', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 1),
(56, 12754, 4611, '2019-07-21 16:21:18', '2019-07-21 16:22:16', 0, 1, NULL, 'CzFql2iCwn2ERzGRnkhzcRATqPzLxO6z', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-21 23:52:20', 0, '2019-07-21 16:21:18', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 1),
(57, 12755, 4611, '2019-07-21 16:21:18', '2019-07-21 16:22:16', 0, 5, NULL, 'xoHtmCaGxjiUOGZKaDXt6qCfg6ZF7nlD', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-22 00:22:20', 0, '2019-07-21 16:21:18', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 1),
(58, 12756, 4611, '2019-07-21 16:21:18', '2019-07-21 16:22:16', 0, 14, NULL, 'cwo4De8zQGdogIU0twzbIjLlAwmNFeEt', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-22 01:02:42', 0, '2019-07-21 16:21:18', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 1),
(59, 12757, 4611, '2019-07-21 16:21:18', '2019-07-21 16:22:16', 0, 9, NULL, 'ovDRhmKZI9d4upeOoMoP3293SduXCTr2', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-21 23:52:20', 0, '2019-07-21 16:21:18', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 1),
(60, 12758, 4611, '2019-07-21 16:21:18', '2019-07-21 16:22:16', 0, 3, NULL, 'uPGusAPuvXfeKrPCZkhndTZ5PE71CPZn', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-22 00:22:20', 0, '2019-07-21 16:21:18', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 1),
(61, 12759, 4611, '2019-07-21 16:21:19', '2019-07-21 16:22:16', 0, 10, NULL, 'QKlBepgOSMj0scoLvCGsmOH5c0qsZRm1', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-21 23:52:20', 0, '2019-07-21 16:21:19', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 1),
(62, 12760, 4611, '2019-07-21 16:21:19', '2019-07-21 16:22:16', 0, 16, NULL, 'Xr16td36JtzSLEhO1SmLbUafSfI2AaEw', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-22 01:02:42', 0, '2019-07-21 16:21:19', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 1),
(63, 12761, 4611, '2019-07-21 16:21:19', '2019-08-06 10:38:18', 15000, 2, NULL, 'PLZvQNphhS6PGZGdQidDPqUnj0QSpW2y', NULL, NULL, NULL, NULL, 15000, NULL, NULL, NULL, 0, 'SAMPATH', NULL, 0, NULL, NULL, NULL, NULL, 1, '2019-07-22 01:22:20', 15000, '2019-07-21 16:21:19', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, 'https://insure.lk/sampath?quote_id=12761&ins_token=aef1oSOv8AJL7bjMOh0GMte31bU2Hgyw&pmt_id=0', 0, './uploads/documents/4611//debit_note-5d3491b167ea0.jpeg', '2019-07-25', 0, NULL, NULL, 33, '2019-08-24', 1),
(64, 12753, 4611, '2019-07-21 16:27:43', '2019-07-21 16:27:43', 0, 7, NULL, 'yczLWMbksqtI4fWVtgR0EyYePJnpP6iF', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-21 23:02:42', 0, '2019-07-21 16:27:43', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 1),
(65, 12754, 4611, '2019-07-21 16:27:44', '2019-07-21 16:27:44', 0, 1, NULL, 'CzFql2iCwn2ERzGRnkhzcRATqPzLxO6z', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-21 23:52:20', 0, '2019-07-21 16:27:44', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 1),
(66, 12755, 4611, '2019-07-21 16:27:44', '2019-07-21 16:27:44', 0, 5, NULL, 'xoHtmCaGxjiUOGZKaDXt6qCfg6ZF7nlD', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-22 00:22:20', 0, '2019-07-21 16:27:44', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 1),
(67, 12756, 4611, '2019-07-21 16:27:44', '2019-07-21 16:27:44', 0, 14, NULL, 'cwo4De8zQGdogIU0twzbIjLlAwmNFeEt', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-22 01:02:42', 0, '2019-07-21 16:27:44', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 1),
(68, 12757, 4611, '2019-07-21 16:27:44', '2019-07-21 16:27:44', 0, 9, NULL, 'ovDRhmKZI9d4upeOoMoP3293SduXCTr2', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-21 23:52:20', 0, '2019-07-21 16:27:44', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 1),
(69, 12758, 4611, '2019-07-21 16:27:44', '2019-07-21 16:27:44', 0, 3, NULL, 'uPGusAPuvXfeKrPCZkhndTZ5PE71CPZn', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-22 00:22:20', 0, '2019-07-21 16:27:44', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 1),
(70, 12759, 4611, '2019-07-21 16:27:44', '2019-07-21 16:27:44', 0, 10, NULL, 'QKlBepgOSMj0scoLvCGsmOH5c0qsZRm1', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-21 23:52:20', 0, '2019-07-21 16:27:44', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 1),
(71, 12760, 4611, '2019-07-21 16:27:44', '2019-07-21 16:27:44', 0, 16, NULL, 'Xr16td36JtzSLEhO1SmLbUafSfI2AaEw', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-22 01:02:42', 0, '2019-07-21 16:27:44', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 1),
(72, 12761, 4611, '2019-07-21 16:27:44', '2019-07-21 16:27:44', 16000, 2, NULL, 'PLZvQNphhS6PGZGdQidDPqUnj0QSpW2y', NULL, NULL, NULL, NULL, 16000, NULL, NULL, NULL, 0, 'SAMPATH', NULL, 0, NULL, NULL, NULL, NULL, 1, '2019-07-22 01:22:20', 15000, '2019-07-21 16:27:44', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 1),
(73, 12762, 4613, '2019-07-21 16:42:30', '2019-07-21 16:43:05', 0, 7, NULL, 'ogTs2sxoe04M8Dy806LlRur0pRQZWmkC', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-21 23:23:53', 0, '2019-07-21 16:42:30', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 1),
(74, 12763, 4613, '2019-07-21 16:42:30', '2019-07-21 16:43:05', 0, 1, NULL, '8hEH5r7cmti59oziEFlmrphwtmLYd7CY', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-22 00:13:31', 0, '2019-07-21 16:42:30', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 1),
(75, 12764, 4613, '2019-07-21 16:42:30', '2019-07-21 16:43:05', 0, 5, NULL, 'ErOyAqKkf3RCOcQcgnF1barTEf1t4S3u', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-22 00:43:31', 0, '2019-07-21 16:42:30', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 1),
(76, 12765, 4613, '2019-07-21 16:42:30', '2019-07-21 16:43:05', 0, 14, NULL, 'Mm2Oru5blqOeyT5Syy4hQQvHgXk2EoZE', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-22 01:23:53', 0, '2019-07-21 16:42:30', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 1),
(77, 12766, 4613, '2019-07-21 16:42:30', '2019-07-21 16:43:05', 0, 9, NULL, 'SPei1XYtWGXRpveVBEdh4gyi7dYWeOzS', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-22 00:13:31', 0, '2019-07-21 16:42:30', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 1),
(78, 12767, 4613, '2019-07-21 16:42:30', '2019-07-21 16:43:05', 0, 3, NULL, 'at2drW03R9Y9g87YF5AC0y5SRBR3xWwi', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-22 00:43:31', 0, '2019-07-21 16:42:30', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 1),
(79, 12768, 4613, '2019-07-21 16:42:30', '2019-07-21 16:43:05', 0, 10, NULL, 'oJQx64lOCzFV90410WnhpmVXRxxfLKO8', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-22 00:13:31', 0, '2019-07-21 16:42:30', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 1),
(80, 12769, 4613, '2019-07-21 16:42:30', '2019-07-21 16:43:05', 0, 16, NULL, 'dPq9ahcjzyKGKl2Wzf9uQjXeCBVbPanS', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-22 01:23:53', 0, '2019-07-21 16:42:30', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 1),
(81, 12770, 4613, '2019-07-21 16:42:30', '2019-08-06 11:35:22', 30000, 2, NULL, 'BcmHWhs5dTsM3jzyTMauAi2SHKSuMgTt', NULL, NULL, NULL, NULL, 30000, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 1, '2019-07-22 01:43:31', 30000, '2019-07-21 16:42:30', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `lead_quotes_1`
--

CREATE TABLE `lead_quotes_1` (
  `id` int(11) NOT NULL,
  `quote_no` int(11) NOT NULL,
  `lead_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `quote_value` double DEFAULT NULL,
  `quote_ins_no` int(11) DEFAULT NULL,
  `sms_token` varchar(10) DEFAULT NULL,
  `quote_token` text NOT NULL,
  `merchant_ref_id` text,
  `bank_reference_id` text,
  `ipg_transaction_id` text,
  `transaction_name` text,
  `transaction_amount` double DEFAULT NULL,
  `transaction_status` varchar(200) DEFAULT NULL,
  `transaction_reason` text,
  `transaction_failed_reason` text,
  `sms_verified` int(11) NOT NULL DEFAULT '0',
  `tr_type` varchar(25) NOT NULL DEFAULT 'AMEX',
  `transaction_type_code` varchar(255) DEFAULT NULL,
  `payment` int(11) NOT NULL DEFAULT '0',
  `payment_datetime` datetime DEFAULT NULL,
  `address_line1` text,
  `address_line2` text,
  `address_city` text,
  `selected` int(11) NOT NULL DEFAULT '0',
  `count_down_datetime` datetime DEFAULT NULL,
  `discounted_value` double NOT NULL DEFAULT '0',
  `selected_datetime` datetime NOT NULL,
  `coupon_added` int(11) NOT NULL DEFAULT '0',
  `coupon_code` varchar(200) DEFAULT NULL,
  `discount_enabled` int(11) NOT NULL DEFAULT '0',
  `bank_pmt_id` int(11) NOT NULL DEFAULT '0',
  `bank_added` int(11) NOT NULL DEFAULT '0',
  `selected_pmt_id` int(11) NOT NULL DEFAULT '0',
  `custom_amount` int(11) NOT NULL DEFAULT '0',
  `travel_scheme_id` int(11) NOT NULL DEFAULT '0',
  `quote_other_enabled` int(11) NOT NULL DEFAULT '0',
  `quote_other_id` int(11) NOT NULL DEFAULT '0',
  `rpt_file_path` text,
  `rpt_image_token` text,
  `rpt_uploaded` int(11) NOT NULL DEFAULT '0',
  `rpt_manual_uploaded` int(11) NOT NULL DEFAULT '0',
  `quote_cn_file_path` text,
  `travel_policy_no` text,
  `card_type` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `lead_quotes_1`
--

INSERT INTO `lead_quotes_1` (`id`, `quote_no`, `lead_id`, `created_at`, `updated_at`, `quote_value`, `quote_ins_no`, `sms_token`, `quote_token`, `merchant_ref_id`, `bank_reference_id`, `ipg_transaction_id`, `transaction_name`, `transaction_amount`, `transaction_status`, `transaction_reason`, `transaction_failed_reason`, `sms_verified`, `tr_type`, `transaction_type_code`, `payment`, `payment_datetime`, `address_line1`, `address_line2`, `address_city`, `selected`, `count_down_datetime`, `discounted_value`, `selected_datetime`, `coupon_added`, `coupon_code`, `discount_enabled`, `bank_pmt_id`, `bank_added`, `selected_pmt_id`, `custom_amount`, `travel_scheme_id`, `quote_other_enabled`, `quote_other_id`, `rpt_file_path`, `rpt_image_token`, `rpt_uploaded`, `rpt_manual_uploaded`, `quote_cn_file_path`, `travel_policy_no`, `card_type`) VALUES
(19, 12645, 4598, '2019-07-02 18:04:00', '2019-07-10 15:57:52', 0, 7, NULL, 'wK4IqAOOVknqz5Qhyt56QspBuPFLEAqL', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-03 00:45:22', 0, '2019-07-02 18:04:00', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL),
(20, 12646, 4598, '2019-07-02 18:04:00', '2019-07-10 15:57:52', 0, 1, NULL, 'y8kEzq1HluhSydr2qkUyf69UPMyH0WYG', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-03 01:35:00', 0, '2019-07-02 18:04:00', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL),
(21, 12647, 4598, '2019-07-02 18:04:00', '2019-07-10 15:57:52', 0, 5, NULL, 'WhN2nyVwNcbOwyxkW0pomUMohNCt9QFn', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-03 02:05:00', 0, '2019-07-02 18:04:00', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL),
(22, 12648, 4598, '2019-07-02 18:04:01', '2019-07-10 15:57:52', 0, 14, NULL, 'xKU6d2SgBcAgKWnMkabDvdHcT7wdEx6O', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-03 02:45:22', 0, '2019-07-02 18:04:01', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL),
(23, 12649, 4598, '2019-07-02 18:04:01', '2019-07-10 15:57:52', 0, 9, NULL, 'l1Ze4QldXZvHwJBVLrQC7lAdAJ5mwf6n', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-03 01:35:00', 0, '2019-07-02 18:04:01', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL),
(24, 12650, 4598, '2019-07-02 18:04:01', '2019-07-10 15:57:52', 0, 3, NULL, 'Wi91QYRDVFWqt678zxwjFkM3sdQrNQgd', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-03 02:05:00', 0, '2019-07-02 18:04:01', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL),
(25, 12651, 4598, '2019-07-02 18:04:01', '2019-07-10 15:57:52', 0, 10, NULL, 'o3R4EbCfdcbfMv0Ww0Lu20dJibN96AUF', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 1, '2019-07-03 01:35:00', 0, '2019-07-02 18:04:01', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL),
(26, 12652, 4598, '2019-07-02 18:04:01', '2019-07-10 15:57:52', 0, 16, NULL, 'AttVQwNSRq9kZIdO4yf8h8gFwS5fUMrl', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-03 02:45:22', 0, '2019-07-02 18:04:01', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL),
(27, 12653, 4598, '2019-07-02 18:04:01', '2019-07-10 15:57:52', 500, 2, NULL, 'ywQeRIR1YbIKBq4W4tiOy2SMK5FIx9al', NULL, NULL, NULL, NULL, 500, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-03 03:05:00', 400, '2019-07-02 18:04:01', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `lead_quotes_history`
--

CREATE TABLE `lead_quotes_history` (
  `id` int(11) NOT NULL,
  `quote_no` int(11) NOT NULL,
  `lead_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `quote_value` double DEFAULT NULL,
  `quote_ins_no` int(11) DEFAULT NULL,
  `sms_token` varchar(10) DEFAULT NULL,
  `quote_token` text NOT NULL,
  `merchant_ref_id` text,
  `bank_reference_id` text,
  `ipg_transaction_id` text,
  `transaction_name` text,
  `transaction_amount` double DEFAULT NULL,
  `transaction_status` varchar(200) DEFAULT NULL,
  `transaction_reason` text,
  `transaction_failed_reason` text,
  `sms_verified` int(11) NOT NULL DEFAULT '0',
  `tr_type` varchar(25) NOT NULL DEFAULT 'AMEX',
  `transaction_type_code` varchar(255) DEFAULT NULL,
  `payment` int(11) NOT NULL DEFAULT '0',
  `payment_datetime` datetime DEFAULT NULL,
  `address_line1` text,
  `address_line2` text,
  `address_city` text,
  `selected` int(11) NOT NULL DEFAULT '0',
  `count_down_datetime` datetime DEFAULT NULL,
  `discounted_value` double NOT NULL DEFAULT '0',
  `selected_datetime` datetime NOT NULL,
  `coupon_added` int(11) NOT NULL DEFAULT '0',
  `coupon_code` varchar(200) DEFAULT NULL,
  `discount_enabled` int(11) NOT NULL DEFAULT '0',
  `bank_pmt_id` int(11) NOT NULL DEFAULT '0',
  `bank_added` int(11) NOT NULL DEFAULT '0',
  `selected_pmt_id` int(11) NOT NULL DEFAULT '0',
  `custom_amount` int(11) NOT NULL DEFAULT '0',
  `travel_scheme_id` int(11) NOT NULL DEFAULT '0',
  `quote_other_enabled` int(11) NOT NULL DEFAULT '0',
  `quote_other_id` int(11) NOT NULL DEFAULT '0',
  `rpt_file_path` text,
  `rpt_image_token` text,
  `rpt_uploaded` int(11) NOT NULL DEFAULT '0',
  `rpt_manual_uploaded` int(11) NOT NULL DEFAULT '0',
  `quote_cn_file_path` text,
  `travel_policy_no` text,
  `card_type` varchar(50) DEFAULT NULL,
  `payment_link` varchar(255) DEFAULT NULL,
  `advance_payment` double DEFAULT '0',
  `debit_note` varchar(255) DEFAULT NULL,
  `policy_start_date` date DEFAULT NULL,
  `balance_payment` double DEFAULT '0',
  `credit_period_start_date` datetime DEFAULT NULL,
  `credit_period_end_date` datetime DEFAULT NULL,
  `days_remaining` int(11) DEFAULT '0',
  `payment_end_date` date DEFAULT NULL,
  `reason` varchar(255) DEFAULT NULL,
  `leadrenewtype` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `lead_quotes_history`
--

INSERT INTO `lead_quotes_history` (`id`, `quote_no`, `lead_id`, `created_at`, `updated_at`, `quote_value`, `quote_ins_no`, `sms_token`, `quote_token`, `merchant_ref_id`, `bank_reference_id`, `ipg_transaction_id`, `transaction_name`, `transaction_amount`, `transaction_status`, `transaction_reason`, `transaction_failed_reason`, `sms_verified`, `tr_type`, `transaction_type_code`, `payment`, `payment_datetime`, `address_line1`, `address_line2`, `address_city`, `selected`, `count_down_datetime`, `discounted_value`, `selected_datetime`, `coupon_added`, `coupon_code`, `discount_enabled`, `bank_pmt_id`, `bank_added`, `selected_pmt_id`, `custom_amount`, `travel_scheme_id`, `quote_other_enabled`, `quote_other_id`, `rpt_file_path`, `rpt_image_token`, `rpt_uploaded`, `rpt_manual_uploaded`, `quote_cn_file_path`, `travel_policy_no`, `card_type`, `payment_link`, `advance_payment`, `debit_note`, `policy_start_date`, `balance_payment`, `credit_period_start_date`, `credit_period_end_date`, `days_remaining`, `payment_end_date`, `reason`, `leadrenewtype`) VALUES
(63, 12761, 4611, '2019-07-21 16:21:19', '2019-08-06 10:38:18', 15000, 2, NULL, 'PLZvQNphhS6PGZGdQidDPqUnj0QSpW2y', NULL, NULL, NULL, NULL, 15000, NULL, NULL, NULL, 0, 'SAMPATH', NULL, 0, NULL, NULL, NULL, NULL, 1, '2019-07-22 01:22:20', 15000, '2019-07-21 16:21:19', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, 'https://insure.lk/sampath?quote_id=12761&ins_token=aef1oSOv8AJL7bjMOh0GMte31bU2Hgyw&pmt_id=0', 0, './uploads/documents/4611//debit_note-5d3491b167ea0.jpeg', '2019-07-25', 0, NULL, NULL, 33, '2019-08-24', 'renewal on 2019-08-06 11:34:33', 1),
(81, 12770, 4613, '2019-07-21 16:42:30', '2019-08-06 11:35:22', 30000, 2, NULL, 'BcmHWhs5dTsM3jzyTMauAi2SHKSuMgTt', NULL, NULL, NULL, NULL, 30000, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 1, '2019-07-22 01:43:31', 30000, '2019-07-21 16:42:30', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 'renewal on 2019-08-06 11:35:23', 1);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nic` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profile_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `nic`, `mobile`, `address`, `password`, `profile_image`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'aaa sasasa', 'salinda@gmail.com', '1111111111', '0713881815', 'asasasasasasasas', '$2y$10$w9ZIHGp6tKKd3uDuFXFaneD43ALiYKS/3ZMBdBte9LyXQLt.xGIgu', '5c8b8f06bb95e.png', 'pBmJyzzSAc2PoyS1DeA6cgNq8GUVqQJOWF5SIkyrd0I57it78kA768jKaeTJ', '2019-03-15 06:09:50', '2019-03-15 06:09:50'),
(2, 'salinda jayawardana', 'jayawardanasalinda@gmail.com', '1231231233', '7161863942', 'NO88, helweeshiyawaththa, narammala', '$2y$10$i1gqojqz4sSU8DyGqDnsn.OQvy4YKyl.tMf92ojk9daPKr/Kdz/mq', '5c9d0cd4c29c9.png', 'ZKCn0p2mTcwBwHokXgAj5o6xcbR18uJu9Izx2uWKFCz5Csgwxjc6pXy9Vm9c', '2019-03-28 12:35:08', '2019-03-28 12:35:08'),
(3, '111111111111111 11111111111', 'jayasalinda@gmail.com', '1111111111', '7161863941', 'NO88, helweeshiyawaththa, narammala', '$2y$10$BpYJb0N2BTZk3SwmG7LnOOuZByWaItSc.jrB4fpNHccdVB7GpdPva', '5d33f8612b1a2.jpeg', 'urvoiEzwCFtpFr5SV7ZNHzc5TlKKs6U6nXIkQ5KD0akOJdvXroTqhLyQ0260', '2019-07-21 00:00:09', '2019-07-21 00:00:09');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `crm_lead_handles`
--
ALTER TABLE `crm_lead_handles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `documents`
--
ALTER TABLE `documents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `insurance_providers`
--
ALTER TABLE `insurance_providers`
  ADD PRIMARY KEY (`ins_prov_no`);

--
-- Indexes for table `leads`
--
ALTER TABLE `leads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lead_quotes`
--
ALTER TABLE `lead_quotes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lead_quotes_1`
--
ALTER TABLE `lead_quotes_1`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lead_quotes_history`
--
ALTER TABLE `lead_quotes_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `crm_lead_handles`
--
ALTER TABLE `crm_lead_handles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `documents`
--
ALTER TABLE `documents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `insurance_providers`
--
ALTER TABLE `insurance_providers`
  MODIFY `ins_prov_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `leads`
--
ALTER TABLE `leads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `lead_quotes`
--
ALTER TABLE `lead_quotes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT for table `lead_quotes_1`
--
ALTER TABLE `lead_quotes_1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `lead_quotes_history`
--
ALTER TABLE `lead_quotes_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
