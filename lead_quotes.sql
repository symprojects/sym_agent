-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 17, 2019 at 01:50 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sym`
--

-- --------------------------------------------------------

--
-- Table structure for table `lead_quotes`
--

CREATE TABLE `lead_quotes` (
  `id` int(11) NOT NULL,
  `quote_no` int(11) NOT NULL,
  `lead_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `quote_value` double DEFAULT NULL,
  `quote_ins_no` int(11) DEFAULT NULL,
  `sms_token` varchar(10) DEFAULT NULL,
  `quote_token` text NOT NULL,
  `merchant_ref_id` text,
  `bank_reference_id` text,
  `ipg_transaction_id` text,
  `transaction_name` text,
  `transaction_amount` double DEFAULT NULL,
  `transaction_status` varchar(200) DEFAULT NULL,
  `transaction_reason` text,
  `transaction_failed_reason` text,
  `sms_verified` int(11) NOT NULL DEFAULT '0',
  `tr_type` varchar(25) NOT NULL DEFAULT 'AMEX',
  `transaction_type_code` varchar(255) DEFAULT NULL,
  `payment` int(11) NOT NULL DEFAULT '0',
  `payment_datetime` datetime DEFAULT NULL,
  `address_line1` text,
  `address_line2` text,
  `address_city` text,
  `selected` int(11) NOT NULL DEFAULT '0',
  `count_down_datetime` datetime DEFAULT NULL,
  `discounted_value` double NOT NULL DEFAULT '0',
  `selected_datetime` datetime NOT NULL,
  `coupon_added` int(11) NOT NULL DEFAULT '0',
  `coupon_code` varchar(200) DEFAULT NULL,
  `discount_enabled` int(11) NOT NULL DEFAULT '0',
  `bank_pmt_id` int(11) NOT NULL DEFAULT '0',
  `bank_added` int(11) NOT NULL DEFAULT '0',
  `selected_pmt_id` int(11) NOT NULL DEFAULT '0',
  `custom_amount` int(11) NOT NULL DEFAULT '0',
  `travel_scheme_id` int(11) NOT NULL DEFAULT '0',
  `quote_other_enabled` int(11) NOT NULL DEFAULT '0',
  `quote_other_id` int(11) NOT NULL DEFAULT '0',
  `rpt_file_path` text,
  `rpt_image_token` text,
  `rpt_uploaded` int(11) NOT NULL DEFAULT '0',
  `rpt_manual_uploaded` int(11) NOT NULL DEFAULT '0',
  `quote_cn_file_path` text,
  `travel_policy_no` text,
  `card_type` varchar(50) DEFAULT NULL,
  `payment_link` varchar(255) DEFAULT NULL,
  `advance_payment` double DEFAULT '0',
  `debit_note` varchar(255) DEFAULT NULL,
  `policy_start_date` date DEFAULT NULL,
  `balance_payment` double DEFAULT '0',
  `credit_period_start_date` date DEFAULT NULL,
  `credit_period_end_date` date DEFAULT NULL,
  `days_remaining` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `lead_quotes`
--

INSERT INTO `lead_quotes` (`id`, `quote_no`, `lead_id`, `created_at`, `updated_at`, `quote_value`, `quote_ins_no`, `sms_token`, `quote_token`, `merchant_ref_id`, `bank_reference_id`, `ipg_transaction_id`, `transaction_name`, `transaction_amount`, `transaction_status`, `transaction_reason`, `transaction_failed_reason`, `sms_verified`, `tr_type`, `transaction_type_code`, `payment`, `payment_datetime`, `address_line1`, `address_line2`, `address_city`, `selected`, `count_down_datetime`, `discounted_value`, `selected_datetime`, `coupon_added`, `coupon_code`, `discount_enabled`, `bank_pmt_id`, `bank_added`, `selected_pmt_id`, `custom_amount`, `travel_scheme_id`, `quote_other_enabled`, `quote_other_id`, `rpt_file_path`, `rpt_image_token`, `rpt_uploaded`, `rpt_manual_uploaded`, `quote_cn_file_path`, `travel_policy_no`, `card_type`, `payment_link`, `advance_payment`, `debit_note`, `policy_start_date`, `balance_payment`, `credit_period_start_date`, `credit_period_end_date`, `days_remaining`) VALUES
(19, 12645, 4598, '2019-07-02 18:04:00', '2019-07-17 09:27:54', 0, 7, NULL, 'wK4IqAOOVknqz5Qhyt56QspBuPFLEAqL', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-03 00:45:22', 0, '2019-07-02 18:04:00', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(20, 12646, 4598, '2019-07-02 18:04:00', '2019-07-17 09:27:54', 0, 1, NULL, 'y8kEzq1HluhSydr2qkUyf69UPMyH0WYG', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-03 01:35:00', 0, '2019-07-02 18:04:00', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(21, 12647, 4598, '2019-07-02 18:04:00', '2019-07-17 09:27:54', 0, 5, NULL, 'WhN2nyVwNcbOwyxkW0pomUMohNCt9QFn', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-03 02:05:00', 0, '2019-07-02 18:04:00', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(22, 12648, 4598, '2019-07-02 18:04:01', '2019-07-17 09:27:54', 0, 14, NULL, 'xKU6d2SgBcAgKWnMkabDvdHcT7wdEx6O', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-03 02:45:22', 0, '2019-07-02 18:04:01', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(23, 12649, 4598, '2019-07-02 18:04:01', '2019-07-17 09:27:54', 0, 9, NULL, 'l1Ze4QldXZvHwJBVLrQC7lAdAJ5mwf6n', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-03 01:35:00', 0, '2019-07-02 18:04:01', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(24, 12650, 4598, '2019-07-02 18:04:01', '2019-07-17 09:27:54', 0, 3, NULL, 'Wi91QYRDVFWqt678zxwjFkM3sdQrNQgd', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-03 02:05:00', 0, '2019-07-02 18:04:01', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(25, 12651, 4598, '2019-07-02 18:04:01', '2019-07-17 09:27:54', 0, 10, NULL, 'o3R4EbCfdcbfMv0Ww0Lu20dJibN96AUF', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-03 01:35:00', 0, '2019-07-02 18:04:01', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(26, 12652, 4598, '2019-07-02 18:04:01', '2019-07-17 09:27:54', 0, 16, NULL, 'AttVQwNSRq9kZIdO4yf8h8gFwS5fUMrl', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 0, '2019-07-03 02:45:22', 0, '2019-07-02 18:04:01', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(27, 12653, 4598, '2019-07-02 18:04:01', '2019-07-17 11:41:44', 500, 2, NULL, 'ywQeRIR1YbIKBq4W4tiOy2SMK5FIx9al', NULL, NULL, NULL, NULL, 500, NULL, NULL, NULL, 0, 'AMEX', NULL, 0, NULL, NULL, NULL, NULL, 1, '2019-07-03 03:05:00', 400, '2019-07-02 18:04:01', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, 'https://insure.lk/amex?quote_id=12653&ins_token=PxAxwUvMPeYtUT98fVsUwxdY6L39S9UI&pmt_id=0', 0, './uploads/documents/4598//debit_note-5d2f0978b62c6.jpeg', '2019-07-18', 0, '2019-07-18', '2019-07-31', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `lead_quotes`
--
ALTER TABLE `lead_quotes`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `lead_quotes`
--
ALTER TABLE `lead_quotes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
